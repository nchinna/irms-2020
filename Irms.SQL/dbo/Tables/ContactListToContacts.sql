﻿CREATE TABLE [dbo].[ContactListToContacts] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [TenantId]      UNIQUEIDENTIFIER NOT NULL,
    [ContactListId] UNIQUEIDENTIFIER NOT NULL,
    [ContactId]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedById]   UNIQUEIDENTIFIER NULL,
    [CreatedOn]     DATETIME         NULL,
    [ModifiedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]    DATETIME         NULL,
    CONSTRAINT [PK_ContactListToContacts] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ContactListToContacts_ContactList] FOREIGN KEY ([ContactListId], [TenantId]) REFERENCES [dbo].[ContactList] ([Id], [TenantId]),
    CONSTRAINT [FK_ContactListToContacts_Contacts] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [Tenantid])
);



