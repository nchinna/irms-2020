﻿CREATE TABLE [dbo].[RfiFormQuestion] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [RfiFormId]    UNIQUEIDENTIFIER NOT NULL,
    [Question]     NVARCHAR (MAX)   NOT NULL,
    [Mapped]       BIT              NOT NULL,
    [MappedField]  NVARCHAR (100)   NOT NULL,
    [SortOrder]    INT              NOT NULL,
    [CreatedOn]    DATETIME         NOT NULL,
    [CreatedById]  UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]   DATETIME         NULL,
    [ModifiedById] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CampaignFormQuestion] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignFormQuestion_CampaignForm] FOREIGN KEY ([RfiFormId], [TenantId]) REFERENCES [dbo].[RfiForm] ([Id], [TenantId])
);

