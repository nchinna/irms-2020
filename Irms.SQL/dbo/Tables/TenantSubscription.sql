﻿CREATE TABLE [dbo].[TenantSubscription] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [ProductId]      UNIQUEIDENTIFIER NOT NULL,
    [StartDateTime]  DATETIME         NOT NULL,
    [ExpiryDateTime] DATETIME         NOT NULL,
    [IsActive]       BIT              CONSTRAINT [DF_TenantSubscription_IsActive] DEFAULT ((0)) NOT NULL,
    [CreatedById]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_TenantSubscription_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_TenantSubscription_ModifiedOn] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TenantSubscription] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TenantSubscription_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]),
    CONSTRAINT [FK_TenantSubscription_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

