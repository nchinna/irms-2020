﻿CREATE TABLE [dbo].[ServiceCatalogFeature] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ServiceCatalogId] UNIQUEIDENTIFIER NOT NULL,
    [Title]            NVARCHAR (200)   NOT NULL,
    [Description]      NVARCHAR (1000)  NULL,
    [IsCustom]         BIT              NULL,
    [CustomText]       NVARCHAR (200)   NULL,
    [IsActive]         BIT              NOT NULL,
    [CreatedById]      UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF__ServiceCa__Creat__59E54FE7] DEFAULT (getdate()) NULL,
    [ModifiedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF__ServiceCa__Modif__5AD97420] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_ServiceCatalogFeature] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ServiceCatalogFeature_ServiceCatalog] FOREIGN KEY ([ServiceCatalogId]) REFERENCES [dbo].[ServiceCatalog] ([Id])
);




GO

CREATE TRIGGER ServiceCatalogFeatureModifiedDate  
ON [dbo].[ServiceCatalogFeature] 
AFTER UPDATE   
AS 
BEGIN

  IF @@rowcount = 0
    RETURN;

  IF UPDATE(ModifiedOn)
    RETURN;

  ELSE
     UPDATE [dbo].[Product] SET ModifiedOn = CURRENT_TIMESTAMP WHERE Id in (SELECT DISTINCT Id FROM Inserted);

END 
GO  