﻿CREATE TABLE [dbo].[CampaignInvitationNextRun](
	[Id] [uniqueidentifier] NOT NULL,
	[TenantId] [uniqueidentifier] NOT NULL,
	[ContactId] [uniqueidentifier] NOT NULL,
	[CampaignInvitationId] [uniqueidentifier] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[Active] [bit] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_CampaignInvitationNextRun] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[TenantId] ASC
)
)
GO
ALTER TABLE [dbo].[CampaignInvitationNextRun]  WITH CHECK ADD  CONSTRAINT [FK_CampaignInvitationNextRun_CampaignInvitation] FOREIGN KEY([CampaignInvitationId], [TenantId])
REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId])
GO

ALTER TABLE [dbo].[CampaignInvitationNextRun] CHECK CONSTRAINT [FK_CampaignInvitationNextRun_CampaignInvitation]
GO

ALTER TABLE [dbo].[CampaignInvitationNextRun]  WITH CHECK ADD  CONSTRAINT [FK_CampaignInvitationNextRun_Contact] FOREIGN KEY([ContactId], [TenantId])
REFERENCES [dbo].[Contact] ([Id], [Tenantid])
GO

ALTER TABLE [dbo].[CampaignInvitationNextRun] CHECK CONSTRAINT [FK_CampaignInvitationNextRun_Contact]
GO


