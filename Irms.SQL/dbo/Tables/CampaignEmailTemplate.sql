﻿CREATE TABLE [dbo].[CampaignEmailTemplate] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [TenantId]             UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationId] UNIQUEIDENTIFIER NOT NULL,
    [Subject]              NVARCHAR (500)   NOT NULL,
    [Preheader]            NVARCHAR (1000)  NULL,
    [SenderEmail]          NVARCHAR (200)   NOT NULL,
    [Body]                 NVARCHAR (MAX)   NOT NULL,
    [PlainBody]            NVARCHAR (MAX)   NOT NULL,
    [AcceptHtml]           NVARCHAR (MAX)   NULL,
    [RejectHtml]           NVARCHAR (MAX)   NULL,
    [ThemeJson]            NVARCHAR (MAX)   NULL,
    [FormSettings]         NVARCHAR (MAX)   NULL,
    [BackgroundImagePath]  NVARCHAR (300)   NULL,
    [CreatedOn]            DATETIME         NOT NULL,
    [CreatedById]          UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]           DATETIME         NULL,
    [ModifiedById]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CampaignEmailTemplate] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignEmailTemplate_CampaignInvitation] FOREIGN KEY ([CampaignInvitationId], [TenantId]) REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId])
);



