﻿CREATE TABLE [dbo].[Language] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Culture]  NVARCHAR (10)    NOT NULL,
    [Name]     NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_Language_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Language]
    ON [dbo].[Language]([Culture] ASC, [TenantId] ASC);

