﻿CREATE TABLE [dbo].[Tenant] (
    [Id]                          UNIQUEIDENTIFIER CONSTRAINT [Tenant_ID_default] DEFAULT (newid()) NOT NULL,
    [Name]                        VARCHAR (100)    NOT NULL,
    [Description]                 VARCHAR (500)    NOT NULL,
    [Address]                     NVARCHAR (200)   CONSTRAINT [DF_Tenant_Address] DEFAULT (N'Alkhobar') NOT NULL,
    [LogoPath]                    NVARCHAR (200)   NULL,
    [CountryId]                   UNIQUEIDENTIFIER NOT NULL,
    [City]                        NVARCHAR (200)   NULL,
    [ClientURL]                   VARCHAR (255)    NULL,
    [EmailFrom]                   VARCHAR (255)    NULL,
    [SendGridApiKey]              VARCHAR (255)    NULL,
    [MalathUsername]              VARCHAR (255)    NULL,
    [MalathPassword]              VARCHAR (255)    NULL,
    [MalathSender]                VARCHAR (255)    NULL,
    [UnifonicSid]                 VARCHAR (255)    NULL,
    [UnifonicSender]              VARCHAR (255)    NULL,
    [TwilioAccountSid]            VARCHAR (255)    NULL,
    [TwilioAuthToken]             VARCHAR (255)    NULL,
    [TwilioNotificationServiceId] VARCHAR (255)    NULL,
    [HeaderPath]                  NVARCHAR (200)   NULL,
    [FooterPath]                  NVARCHAR (200)   NULL,
    [CreatedOn]                   DATETIME         NOT NULL,
    [HasOwnDomain]                BIT              NULL,
    [IsActive]                    BIT              NOT NULL,
    [IsDeleted]                   BIT              NOT NULL,
    [UpdatedOn]                   DATETIME         NULL,
    [AdminId]                     UNIQUEIDENTIFIER NULL,
    [CreatedBy]                   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedBy]                   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Tenant__CountryI__30C33EC3] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([ID]),
    CONSTRAINT [FK_Tenant_Admin] FOREIGN KEY ([AdminId], [Id]) REFERENCES [dbo].[UserInfo] ([Id], [TenantId])
);







