﻿CREATE TABLE [dbo].[ContactListRFIAnswer]
(
    Id UNIQUEIDENTIFIER NOT NULL,
    TenantId UNIQUEIDENTIFIER NOT NULL,
    ContactId UNIQUEIDENTIFIER NOT NULL,
    ContactListRfiId UNIQUEIDENTIFIER NOT NULL,
    DateAnswered DATETIME NULL,
    MediaType INT NULL,
    CONSTRAINT PK_ContactListRFIAnswer PRIMARY KEY CLUSTERED (Id ASC, TenantId ASC),
    CONSTRAINT FK_ContactListRFIAnswer_Tenant FOREIGN KEY (TenantId) REFERENCES [dbo].[Tenant] (Id),
    CONSTRAINT [FK_ContactListRFIAnswer_Contact] FOREIGN KEY (ContactId, [TenantId]) REFERENCES [dbo].Contact ([Id], [Tenantid]),
    CONSTRAINT [FK_ContactListRFIAnswer_ContactListRfi] FOREIGN KEY ([ContactListRfiId], [TenantId]) REFERENCES [dbo].ContactListRfi ([Id], [Tenantid]),
)
