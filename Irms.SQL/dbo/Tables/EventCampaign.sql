﻿CREATE TABLE [dbo].[EventCampaign] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [EventId]             UNIQUEIDENTIFIER NOT NULL,
    [Name]                NVARCHAR (200)   NOT NULL,
    [GuestListId]         UNIQUEIDENTIFIER NULL,
    [CampaignType]        INT              NOT NULL,
    [EntryCriteriaTypeId] TINYINT          CONSTRAINT [DF_EventCampaign_EntryCriteriaTypeId] DEFAULT ((0)) NULL,
    [ExitCriteriaType]    TINYINT          NULL,
    [CampaignStatus]      TINYINT          NOT NULL,
    [Active]              BIT              NOT NULL,
    [Deleted]             BIT              NOT NULL,
    [CreatedOn]           DATETIME         NOT NULL,
    [CreatedById]         UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]          DATETIME         NULL,
    [ModifiedById]        UNIQUEIDENTIFIER NULL,
    [AcceptCode]          NVARCHAR (20)    NULL,
    [RejectCode]          NVARCHAR (20)    NULL,
    CONSTRAINT [PK_EventCampaign] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_EventCampaign_CampaignEntryCriteriaType] FOREIGN KEY ([EntryCriteriaTypeId]) REFERENCES [dbo].[CampaignEntryCriteriaType] ([Id]),
    CONSTRAINT [FK_EventCampaign_Event] FOREIGN KEY ([EventId], [TenantId]) REFERENCES [dbo].[Event] ([Id], [TenantId])
);





