﻿CREATE TABLE [dbo].[WhatsappApprovedTemplate] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [InvitationTypeId] INT              NOT NULL,
    [TemplateBody]     NVARCHAR (1000)  NOT NULL,
    [Name]             NVARCHAR (50)    NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [CreatedById]      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]       DATETIME         NULL,
    [ModifiedById]     UNIQUEIDENTIFIER NULL,
    [HasRfi]           BIT              DEFAULT ((0)) NOT NULL,
    [IsBot]            BIT              NULL,
    CONSTRAINT [PK_WhatsappApprovedTemplate] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


