﻿CREATE TABLE [dbo].[Currency] (
    [Id]    TINYINT       IDENTITY (1, 1) NOT NULL,
    [Title] NVARCHAR (20) NULL,
    CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED ([Id] ASC)
);

