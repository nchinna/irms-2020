﻿CREATE TABLE [dbo].[ProviderLogs] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [ContactId]    UNIQUEIDENTIFIER NULL,
    [UserId]       UNIQUEIDENTIFIER NULL,
    [ProviderType] INT              NOT NULL,
    [Request]      NVARCHAR (MAX)   NOT NULL,
    [RequestDate]  DATETIME         NOT NULL,
    [Response]     NVARCHAR (MAX)   NULL,
    [ResponseDate] DATETIME         NULL,
    [CampaignInvitationId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProviderLogs] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ProviderLogs_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId]),
    CONSTRAINT [FK_ThirdPartyLogs_UserInfo] FOREIGN KEY ([UserId], [TenantId]) REFERENCES [dbo].[UserInfo] ([Id], [TenantId])
);

