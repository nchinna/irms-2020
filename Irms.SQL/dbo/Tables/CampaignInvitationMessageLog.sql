﻿CREATE TABLE [dbo].[CampaignInvitationMessageLog] (
    [Id]                       UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                 UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationId]     UNIQUEIDENTIFIER NOT NULL,
    [ContactId]                UNIQUEIDENTIFIER NOT NULL,
    [InvitationTemplateTypeId] INT              NULL,
    [MessageId]                NVARCHAR (50)    NULL,
    [MessageStatus]            INT              NOT NULL,
    [MessageText]              NVARCHAR (MAX)   NULL,
    [TimeStamp]                DATETIME         NOT NULL,
    [ProviderTypeId]           INT              NULL,
    CONSTRAINT [PK_CampaignInvitationSentLog] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignInvitationSentLog_CampaignInvitation] FOREIGN KEY ([CampaignInvitationId], [TenantId]) REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId]),
    CONSTRAINT [FK_CampaignInvitationSentLog_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId])
);







