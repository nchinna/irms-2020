﻿CREATE TABLE [dbo].[CampaignPlaceholder] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Title]            NVARCHAR (200) NOT NULL,
    [Placeholder]      NVARCHAR (200) NOT NULL,
    [TemplateTypeId]   INT            NULL,
    [InvitationTypeId] INT            NULL,
    [Response]         BIT            CONSTRAINT [DF__CampaignP__Respo__2FBA0BF1] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_CampaignPlaceholder] PRIMARY KEY CLUSTERED ([Id] ASC)
);







