USE mvms

MERGE INTO [dbo].[OnlineTraining] as target
USING(SELECT NEWID(), t.ID, 'Test training', 'This is a test training', GETDATE(), DATEADD(year, 10, GETDATE()), 3, 'Test training body', 'https://www.youtube.com/watch?v=Bey4XXJAqS8'
FROM Tenant t
) as source([Id],[TenantId],[Title],[Description],[StartDate],[EndDate],[ScoreToPass],[Body],[VideoLink])
on target.Title = source.Title AND target.[TenantId] = source.[TenantId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([Id],[TenantId],[Title],[Description],[StartDate],[EndDate],[ScoreToPass],[Body],[VideoLink])
	VALUES([Id],[TenantId],[Title],[Description],[StartDate],[EndDate],[ScoreToPass],[Body],[VideoLink]);
GO

MERGE INTO [dbo].[OnlineTrainingQuestion] as target
USING(
SELECT NEWID(), t.ID, (SELECT TOP 1 ID FROM [OnlineTraining] WHERE TenantId = t.ID AND Title = 'Test training'), X.Body AS Body, X.[Sequence] AS [Sequence]
		FROM (VALUES
			  ('Question 1', 0),
			  ('Question 2', 1),
			  ('Question 3', 2),
			  ('Question 4', 3),
			  ('Question 5', 4)
		) AS X([Body], [Sequence])
	, Tenant t
) as source([Id],[TenantId],[OnlineTrainingId],[Body],[Sequence])
on target.Body = source.Body AND target.[TenantId] = source.[TenantId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([Id],[TenantId],[OnlineTrainingId],[Body],[Sequence])
	VALUES([Id],[TenantId],[OnlineTrainingId],[Body],[Sequence]);

MERGE INTO [dbo].[OnlineTrainingAnswer] as target
USING(
SELECT NEWID(), t.ID, (SELECT TOP 1 ID FROM [OnlineTrainingQuestion] WHERE TenantId = t.ID AND Body = X.QuestionBody), X.Label AS Label, X.IsCorrect AS Sequence
		FROM (VALUES
			  ('Question 1', 'Answer 1 1', 0),
			  ('Question 1', 'Answer 1 2 Correct', 1),
			  ('Question 1', 'Answer 1 3 Correct', 1),

			  ('Question 2', 'Answer 2 1 Correct', 1),
			  ('Question 2', 'Answer 2 2 Correct', 1),
			  ('Question 2', 'Answer 2 3 Correct', 1),

			  ('Question 3', 'Answer 3 1', 0),
			  ('Question 3', 'Answer 3 2', 0),
			  ('Question 3', 'Answer 3 3 Correct', 1),

			  ('Question 4', 'Answer 4 1', 0),
			  ('Question 4', 'Answer 4 2 Correct', 1),
			  ('Question 4', 'Answer 4 3 Correct', 1),

			  ('Question 5', 'Answer 5 1 Correct', 1),
			  ('Question 5', 'Answer 5 2', 0),
			  ('Question 5', 'Answer 5 3', 0)
		) AS X([QuestionBody], [Label], [IsCorrect])
	, Tenant t
) as source([Id],[TenantId],[OnlineTrainingQuestionId],[Label],[IsCorrect])
on target.Label = source.Label AND target.[TenantId] = source.[TenantId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([Id],[TenantId],[OnlineTrainingQuestionId],[Label],[IsCorrect])
	VALUES([Id],[TenantId],[OnlineTrainingQuestionId],[Label],[IsCorrect]);
GO