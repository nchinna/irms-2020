USE [mvms];

GO
PRINT N'Creating [ftCatalog]...';


GO
CREATE FULLTEXT CATALOG [ftCatalog]
    WITH ACCENT_SENSITIVITY = ON
    AS DEFAULT
    AUTHORIZATION [dbo];


GO
PRINT N'Creating [dbo].[AcceptReason]...';


GO
CREATE TABLE [dbo].[AcceptReason] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_AcceptReason] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[AcceptReasonTranslation]...';


GO
CREATE TABLE [dbo].[AcceptReasonTranslation] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [AcceptReasonId] UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]     UNIQUEIDENTIFIER NOT NULL,
    [Translation]    NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_AcceptReasonTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[AcceptReasonTranslation].[UX_AcceptReasonTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_AcceptReasonTranslation]
    ON [dbo].[AcceptReasonTranslation]([AcceptReasonId] ASC, [LanguageId] ASC);


GO
PRINT N'Creating [dbo].[AspNetRoleClaims]...';


GO
CREATE TABLE [dbo].[AspNetRoleClaims] (
    [Id]         INT              IDENTITY (1, 1) NOT NULL,
    [ClaimType]  NVARCHAR (MAX)   NULL,
    [ClaimValue] NVARCHAR (MAX)   NULL,
    [RoleId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetRoleClaims].[IX_AspNetRoleClaims_RoleId]...';


GO
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId]
    ON [dbo].[AspNetRoleClaims]([RoleId] ASC);


GO
PRINT N'Creating [dbo].[AspNetRoles]...';


GO
CREATE TABLE [dbo].[AspNetRoles] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ConcurrencyStamp] NVARCHAR (MAX)   NULL,
    [Name]             NVARCHAR (256)   NULL,
    [NormalizedName]   NVARCHAR (256)   NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetRoles].[RoleNameIndex]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([NormalizedName] ASC) WHERE ([NormalizedName] IS NOT NULL);


GO
PRINT N'Creating [dbo].[AspNetUserClaims]...';


GO
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]         INT              IDENTITY (1, 1) NOT NULL,
    [ClaimType]  NVARCHAR (MAX)   NULL,
    [ClaimValue] NVARCHAR (MAX)   NULL,
    [UserId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetUserClaims].[IX_AspNetUserClaims_UserId]...';


GO
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId]
    ON [dbo].[AspNetUserClaims]([UserId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUserLogins]...';


GO
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider]       NVARCHAR (450)   NOT NULL,
    [ProviderKey]         NVARCHAR (450)   NOT NULL,
    [ProviderDisplayName] NVARCHAR (MAX)   NULL,
    [UserId]              UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetUserLogins].[IX_AspNetUserLogins_UserId]...';


GO
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId]
    ON [dbo].[AspNetUserLogins]([UserId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUserRoles]...';


GO
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetUserRoles].[IX_AspNetUserRoles_RoleId]...';


GO
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId]
    ON [dbo].[AspNetUserRoles]([RoleId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUsers]...';


GO
CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   UNIQUEIDENTIFIER   NOT NULL,
    [AccessFailedCount]    INT                NOT NULL,
    [ConcurrencyStamp]     NVARCHAR (MAX)     NULL,
    [Email]                NVARCHAR (256)     NULL,
    [EmailConfirmed]       BIT                NOT NULL,
    [IsSuperAdmin]         BIT                NOT NULL,
    [LockoutEnabled]       BIT                NOT NULL,
    [LockoutEnd]           DATETIMEOFFSET (7) NULL,
    [NormalizedEmail]      NVARCHAR (256)     NULL,
    [NormalizedUserName]   NVARCHAR (256)     NULL,
    [PasswordHash]         NVARCHAR (MAX)     NULL,
    [PhoneNumber]          NVARCHAR (50)      NULL,
    [PhoneNumberConfirmed] BIT                NOT NULL,
    [SecurityStamp]        NVARCHAR (MAX)     NULL,
    [TwoFactorEnabled]     BIT                NOT NULL,
    [UserName]             NVARCHAR (256)     NULL,
    [TenantId]             UNIQUEIDENTIFIER   NOT NULL,
    [IsEnabled]            BIT                NOT NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Creating [dbo].[AspNetUsers].[UX_UserNameIndex]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_UserNameIndex]
    ON [dbo].[AspNetUsers]([NormalizedUserName] ASC, [TenantId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUsers].[UX_PhoneIndex]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PhoneIndex]
    ON [dbo].[AspNetUsers]([PhoneNumber] ASC, [TenantId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUsers].[UX_EmailIndex]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_EmailIndex]
    ON [dbo].[AspNetUsers]([Email] ASC, [TenantId] ASC);


GO
PRINT N'Creating [dbo].[AspNetUserTokens]...';


GO
CREATE TABLE [dbo].[AspNetUserTokens] (
    [UserId]        UNIQUEIDENTIFIER NOT NULL,
    [LoginProvider] NVARCHAR (450)   NOT NULL,
    [Name]          NVARCHAR (450)   NOT NULL,
    [Value]         NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED ([UserId] ASC, [LoginProvider] ASC, [Name] ASC)
);


GO
PRINT N'Creating [dbo].[City]...';


GO
CREATE TABLE [dbo].[City] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[CityTranslation]...';


GO
CREATE TABLE [dbo].[CityTranslation] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [CityId]      UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]  UNIQUEIDENTIFIER NOT NULL,
    [Translation] NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_CityTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[CityTranslation].[UX_CityTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CityTranslation]
    ON [dbo].[CityTranslation]([CityId] ASC, [LanguageId] ASC);


GO
PRINT N'Creating [dbo].[Clinic]...';


GO
CREATE TABLE [dbo].[Clinic] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_ClinicLookup] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[ClinicTranslation]...';


GO
CREATE TABLE [dbo].[ClinicTranslation] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]  UNIQUEIDENTIFIER NOT NULL,
    [Translation] NVARCHAR (200)   NOT NULL,
    [ClinicId]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_ClinicTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[ClinicTranslation].[UX_ClinicTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_ClinicTranslation]
    ON [dbo].[ClinicTranslation]([LanguageId] ASC, [ClinicId] ASC);


GO
PRINT N'Creating [dbo].[Department]...';


GO
CREATE TABLE [dbo].[Department] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [ClientURL] VARCHAR (255)   NOT NULL,
    [IsActive]  BIT             NOT NULL,
    [IsDefault] BIT             NOT NULL,
    [IsDeleted] BIT             NOT NULL,
    [Name]      NVARCHAR (255)  NOT NULL,
    [EmailFrom] VARCHAR (255)   NOT NULL,
    [LogoId]   UNIQUEIDENTIFIER NULL,
    [HeaderId] UNIQUEIDENTIFIER NULL,
    [FooterId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[Tenant].[UX_Tenant_ClientURL]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Department_ClientURL]
    ON [dbo].[Department]([ClientURL] ASC, [IsActive] ASC, [IsDeleted] ASC);


GO
PRINT N'Creating [dbo].[Employee]...';


GO
CREATE TABLE [dbo].[Employee] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [RoleId]          INT              NOT NULL,
    [FullName]        NVARCHAR (100)   NOT NULL,
    [GenderId]        INT              NOT NULL,
    [BirthDate]       DATETIME         NOT NULL,
    [PhoneNo]         NVARCHAR (50)    NOT NULL,
    [Email]           NVARCHAR (50)    NOT NULL,
    [MessagingHandle] NVARCHAR (50)    NULL,
    [IsActive]        BIT              NOT NULL,
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [AvatarId]        UNIQUEIDENTIFIER NULL,
    [IsDeleted]       BIT              NOT NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[EventLog]...';


GO
CREATE TABLE [dbo].[EventLog] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [EventType]  NVARCHAR (256)   NOT NULL,
    [OccurredOn] DATETIME         NOT NULL,
    [UserId]     UNIQUEIDENTIFIER NULL,
    [Data]       NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[File]...';


GO
CREATE TABLE [dbo].[File] (
    [ID]          UNIQUEIDENTIFIER NOT NULL,
    [TenantID]    UNIQUEIDENTIFIER NOT NULL,
    [Name]        VARCHAR (256)    NULL,
    [MimeType]    VARCHAR (256)    NULL,
    [Description] VARCHAR (1024)   NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [Data]        VARBINARY (MAX)  NULL,
    [UpdatedOn]   DATETIME         NULL,
    [Thumbnail]   VARBINARY (MAX)  NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([ID] ASC, [TenantID] ASC)
);


GO
PRINT N'Creating [dbo].[Interview]...';


GO
CREATE TABLE [dbo].[Interview] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [TypeId]   INT              NOT NULL,
    [Name]     NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_Interview] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[InterviewAppointment]...';


GO
CREATE TABLE [dbo].[InterviewAppointment] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [InterviewScheduleId] UNIQUEIDENTIFIER NOT NULL,
    [VolunteerId]         UNIQUEIDENTIFIER NOT NULL,
    [InterviewResultId]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_InterviewAppointment] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[InterviewResult]...';


GO
CREATE TABLE [dbo].[InterviewResult] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [IsApproved]     BIT              NOT NULL,
    [AcceptReasonId] UNIQUEIDENTIFIER NULL,
    [RejectReasonId] UNIQUEIDENTIFIER NULL,
    [Remarks]        NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_InterviewResult] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[InterviewSchedule]...';


GO
CREATE TABLE [dbo].[InterviewSchedule] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [InterviewId] UNIQUEIDENTIFIER NOT NULL,
    [Date]        DATETIME         NOT NULL,
    [Duration]    INT              NOT NULL,
    CONSTRAINT [PK_InterviewSchedule] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[JoiningReason]...';


GO
CREATE TABLE [dbo].[JoiningReason] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_JoiningReason] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[JoiningReasonTranslation]...';


GO
CREATE TABLE [dbo].[JoiningReasonTranslation] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]      UNIQUEIDENTIFIER NOT NULL,
    [Translation]     NVARCHAR (100)   NOT NULL,
    [JoiningReasonId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_JoiningReasonTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[JoiningReasonTranslation].[UX_JoiningReasonTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_JoiningReasonTranslation]
    ON [dbo].[JoiningReasonTranslation]([LanguageId] ASC, [JoiningReasonId] ASC);


GO
PRINT N'Creating [dbo].[Language]...';


GO
CREATE TABLE [dbo].[Language] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Culture]  NCHAR (10)       NOT NULL,
    [Name]     NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[Language].[UX_Language]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Language]
    ON [dbo].[Language]([Culture] ASC, [TenantId] ASC);


GO
PRINT N'Creating [dbo].[Medical]...';


GO
CREATE TABLE [dbo].[Medical] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [TypeId]   INT              NOT NULL,
    [Name]     NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_Medical] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[MedicalAppointment]...';


GO
CREATE TABLE [dbo].[MedicalAppointment] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [MedicalScheduleId] UNIQUEIDENTIFIER NOT NULL,
    [VolunteerId]       UNIQUEIDENTIFIER NOT NULL,
    [MedicalResultId]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MedicalAppointment] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[MedicalResult]...';


GO
CREATE TABLE [dbo].[MedicalResult] (
    [Id]                     UNIQUEIDENTIFIER NOT NULL,
    [TenantId]               UNIQUEIDENTIFIER NOT NULL,
    [BloodTypeId]            INT              NOT NULL,
    [BloodPressureSystolic]  INT              NOT NULL,
    [BloodPressureDiastolic] INT              NOT NULL,
    [BodyTemperature]        DECIMAL (18, 4)     NOT NULL,
    [Remarks]                NVARCHAR (200)   NULL,
    [IsApproved]             BIT              NOT NULL,
    CONSTRAINT [PK_VolunteerMedicalState] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[MedicalResultFiles]...';


GO
CREATE TABLE [dbo].[MedicalResultFiles] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [MedicalResultId] UNIQUEIDENTIFIER NOT NULL,
    [FileId]          UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_MedicalResultFiles] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[MedicalSchedule]...';


GO
CREATE TABLE [dbo].[MedicalSchedule] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [MedicalId]   UNIQUEIDENTIFIER NOT NULL,
    [Date]        DATETIME         NOT NULL,
    [Duration]    INT              NOT NULL,
    [CityId]      UNIQUEIDENTIFIER NOT NULL,
    [ClinicId]    UNIQUEIDENTIFIER NOT NULL,
    [PhysicianId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_MedicalSchedule] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[Profession]...';


GO
CREATE TABLE [dbo].[Profession] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_Profession] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[ProfessionTranslation]...';


GO
CREATE TABLE [dbo].[ProfessionTranslation] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]   UNIQUEIDENTIFIER NOT NULL,
    [Translation]  NVARCHAR (100)   NOT NULL,
    [ProfessionId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_ProfessionTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[ProfessionTranslation].[UX_ProfessionTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_ProfessionTranslation]
    ON [dbo].[ProfessionTranslation]([LanguageId] ASC, [ProfessionId] ASC);


GO
PRINT N'Creating [dbo].[RejectReason]...';


GO
CREATE TABLE [dbo].[RejectReason] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_RejectReason] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[RejectReasonTranslation]...';


GO
CREATE TABLE [dbo].[RejectReasonTranslation] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]     UNIQUEIDENTIFIER NOT NULL,
    [Translation]    NVARCHAR (100)   NOT NULL,
    [RejectReasonId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_RejectReasonTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[RejectReasonTranslation].[UX_RejectReasonTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_RejectReasonTranslation]
    ON [dbo].[RejectReasonTranslation]([LanguageId] ASC, [RejectReasonId] ASC);


GO
PRINT N'Creating [dbo].[Skill]...';


GO
CREATE TABLE [dbo].[Skill] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_Skill] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[SkillTranslation]...';


GO
CREATE TABLE [dbo].[SkillTranslation] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]  UNIQUEIDENTIFIER NOT NULL,
    [Translation] NVARCHAR (100)   NOT NULL,
    [SkillId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_SkillTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[SkillTranslation].[UX_SkillTranslation]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SkillTranslation]
    ON [dbo].[SkillTranslation]([SkillId] ASC, [LanguageId] ASC);


GO
PRINT N'Creating [dbo].[Tenant]...';


GO
CREATE TABLE [dbo].[Tenant] (
    [ID]          UNIQUEIDENTIFIER NOT NULL,
    [CreateDate]  DATETIME         NOT NULL,
    [Description] VARCHAR (400)    NULL,
    [ExpiryDate]  DATETIME         NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [IsDeleted]   BIT              NOT NULL,
    [Name]        VARCHAR (50)     NOT NULL,
    [UpdateDate]  DATETIME         NOT NULL,
    [AdminId]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO
PRINT N'Creating [dbo].[TenantToSystemModule]...';


GO
CREATE TABLE [dbo].[TenantToSystemModule] (
    [ID]             UNIQUEIDENTIFIER NOT NULL,
    [TenantID]       UNIQUEIDENTIFIER NOT NULL,
    [SystemModuleID] INT              NOT NULL,
    CONSTRAINT [PK_TenantToSystemModule_1] PRIMARY KEY CLUSTERED ([ID] ASC, [TenantID] ASC)
);


GO
PRINT N'Creating [dbo].[TenantToSystemModule].[UX_TenantToSystemModule]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TenantToSystemModule]
    ON [dbo].[TenantToSystemModule]([TenantID] ASC, [SystemModuleID] ASC);


GO
PRINT N'Creating [dbo].[Training]...';


GO
CREATE TABLE [dbo].[Training] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [TypeId]   INT              NOT NULL,
    [Name]     NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_Training] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[TrainingAppointment]...';


GO
CREATE TABLE [dbo].[TrainingAppointment] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [TenantId]           UNIQUEIDENTIFIER NOT NULL,
    [TrainingScheduleId] UNIQUEIDENTIFIER NOT NULL,
    [VolunteerId]        UNIQUEIDENTIFIER NOT NULL,
    [TrainingResultId]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrainingAppointment] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[TrainingResult]...';


GO
CREATE TABLE [dbo].[TrainingResult] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [Remarks]    NVARCHAR (200)   NOT NULL,
    [Score]      DECIMAL (18, 4)  NOT NULL,
    [IsApproved] BIT              NOT NULL,
    CONSTRAINT [PK_TrainingResult] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[TrainingSchedule]...';


GO
CREATE TABLE [dbo].[TrainingSchedule] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [TrainingId] UNIQUEIDENTIFIER NOT NULL,
    [Date]       DATETIME         NOT NULL,
    [Duration]   INT              NOT NULL,
    CONSTRAINT [PK_TrainingSchedule] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[Volunteer]...';


GO
CREATE TABLE [dbo].[Volunteer] (
    [ID]                     UNIQUEIDENTIFIER NOT NULL,
    [TenantId]               UNIQUEIDENTIFIER NOT NULL,
    [FirstName]              NVARCHAR (50)    NOT NULL,
    [MiddleName]             NVARCHAR (50)    NOT NULL,
    [LastName]               NVARCHAR (50)    NOT NULL,
    [GenderId]               INT              NOT NULL,
    [BirthDate]              DATETIME         NOT NULL,
    [NationalityId]          INT              NOT NULL,
    [NationalId]             NVARCHAR (50)    NULL,
    [PassportNo]             NVARCHAR (50)    NULL,
    [ProfessionId]           UNIQUEIDENTIFIER NULL,
    [AccountTypeId]          INT              NOT NULL,
    [BadgeNo]                NVARCHAR (50)    NULL,
    [MartialStatusId]        INT              NOT NULL,
    [CityId]                 UNIQUEIDENTIFIER NOT NULL,
    [PhoneNo]                NVARCHAR (50)    NOT NULL,
    [Email]                  NVARCHAR (250)   NOT NULL,
    [EmergencyContact]       NVARCHAR (50)    NOT NULL,
    [HasExperience]          BIT              NOT NULL,
    [ExperienceLocation]     NVARCHAR (200)   NULL,
    [ExperienceType]         NVARCHAR (200)   NULL,
    [ValidationLink]         NVARCHAR (1000)  NULL,
    [JoiningReasonId]        UNIQUEIDENTIFIER NULL,
    [ParticipationTimeId]    INT              NOT NULL,
    [MaxHoursPerDay]         INT              NOT NULL,
    [HasDiseases]            BIT              NOT NULL,
    [Diseases]               NVARCHAR (1000)  NULL,
    [AvatarId]               UNIQUEIDENTIFIER NULL,
    [UserId]                 UNIQUEIDENTIFIER NOT NULL,
    [IsDeleted]              BIT              NOT NULL,
    [Address]                NVARCHAR (250)   NOT NULL,
    [SearchContents]         NVARCHAR (MAX)   NOT NULL,
    [IsRegistrationFinished] BIT              NOT NULL,
    CONSTRAINT [PK_Volunteer] PRIMARY KEY CLUSTERED ([ID] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[Volunteer].[FTUX_Volunteer]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [FTUX_Volunteer]
    ON [dbo].[Volunteer]([ID] ASC);


GO
PRINT N'Creating [dbo].[VolunteerParticipationDays]...';


GO
CREATE TABLE [dbo].[VolunteerParticipationDays] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [VolunteerId] UNIQUEIDENTIFIER NOT NULL,
    [DayOfWeekId] INT              NOT NULL,
    CONSTRAINT [PK_VolunteerParticipationDays] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[VolunteerParticipationDays].[UX_VolunteerParticipationDays]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_VolunteerParticipationDays]
    ON [dbo].[VolunteerParticipationDays]([VolunteerId] ASC, [DayOfWeekId] ASC);


GO
PRINT N'Creating [dbo].[VolunteerRegistration]...';


GO
CREATE TABLE [dbo].[VolunteerRegistration] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [RegistrationDate]    DATETIME         NOT NULL,
    [StatusId]            INT              NOT NULL,
    [VolunteerName]       NVARCHAR (150)   NOT NULL,
    [VolunteerEmail]      NVARCHAR (250)   NOT NULL,
    [VolunteerPhone]      NVARCHAR (50)    NOT NULL,
    [VolunteerNationalId] NVARCHAR (50)    NOT NULL,
    [IsDeleted]           BIT              NOT NULL,
    CONSTRAINT [PK_VolunteerRegistration] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[VolunteerRegistration].[IX_VolunteerRegistration]...';


GO
CREATE NONCLUSTERED INDEX [IX_VolunteerRegistration]
    ON [dbo].[VolunteerRegistration]([TenantId] ASC, [StatusId] ASC, [RegistrationDate] ASC);


GO
PRINT N'Creating [dbo].[VolunteerToSkill]...';


GO
CREATE TABLE [dbo].[VolunteerToSkill] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [VolunteerId] UNIQUEIDENTIFIER NOT NULL,
    [SkillId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_VolunteerSkills] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);


GO
PRINT N'Creating [dbo].[AcceptReason_Id_default]...';


GO
ALTER TABLE [dbo].[AcceptReason]
    ADD CONSTRAINT [AcceptReason_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[AcceptReasonTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[AcceptReasonTranslation]
    ADD CONSTRAINT [AcceptReasonTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[City_Id_default]...';


GO
ALTER TABLE [dbo].[City]
    ADD CONSTRAINT [City_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[CityTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[CityTranslation]
    ADD CONSTRAINT [CityTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[Clinic_Id_default]...';


GO
ALTER TABLE [dbo].[Clinic]
    ADD CONSTRAINT [Clinic_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[ClinicTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[ClinicTranslation]
    ADD CONSTRAINT [ClinicTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[Employee_Id_default]...';


GO
ALTER TABLE [dbo].[Employee]
    ADD CONSTRAINT [Employee_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[JoiningReason_Id_default]...';


GO
ALTER TABLE [dbo].[JoiningReason]
    ADD CONSTRAINT [JoiningReason_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[JoiningReasonTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[JoiningReasonTranslation]
    ADD CONSTRAINT [JoiningReasonTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[VolunteerMedicalState_Id_default]...';


GO
ALTER TABLE [dbo].[MedicalResult]
    ADD CONSTRAINT [VolunteerMedicalState_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[VolunteerMedicalStateFiles_Id_default]...';


GO
ALTER TABLE [dbo].[MedicalResultFiles]
    ADD CONSTRAINT [VolunteerMedicalStateFiles_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[ProfessionTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[ProfessionTranslation]
    ADD CONSTRAINT [ProfessionTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[RejectReason_Id_default]...';


GO
ALTER TABLE [dbo].[RejectReason]
    ADD CONSTRAINT [RejectReason_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[RejectReasonTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[RejectReasonTranslation]
    ADD CONSTRAINT [RejectReasonTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[Skill_Id_default]...';


GO
ALTER TABLE [dbo].[Skill]
    ADD CONSTRAINT [Skill_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[SkillTranslation_Id_default]...';


GO
ALTER TABLE [dbo].[SkillTranslation]
    ADD CONSTRAINT [SkillTranslation_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[Tenant_ID_default]...';


GO
ALTER TABLE [dbo].[Tenant]
    ADD CONSTRAINT [Tenant_ID_default] DEFAULT (newid()) FOR [ID];


GO
PRINT N'Creating [dbo].[TenantToSystemModule_ID_default]...';


GO
ALTER TABLE [dbo].[TenantToSystemModule]
    ADD CONSTRAINT [TenantToSystemModule_ID_default] DEFAULT (newid()) FOR [ID];


GO
PRINT N'Creating [dbo].[Volunteer_ID_default]...';


GO
ALTER TABLE [dbo].[Volunteer]
    ADD CONSTRAINT [Volunteer_ID_default] DEFAULT (newid()) FOR [ID];


GO
PRINT N'Creating [dbo].[DF_Volunteer_SearchContents]...';


GO
ALTER TABLE [dbo].[Volunteer]
    ADD CONSTRAINT [DF_Volunteer_SearchContents] DEFAULT (N'') FOR [SearchContents];


GO
PRINT N'Creating [dbo].[DF_Volunteer_IsRegistrationFinished]...';


GO
ALTER TABLE [dbo].[Volunteer]
    ADD CONSTRAINT [DF_Volunteer_IsRegistrationFinished] DEFAULT ((0)) FOR [IsRegistrationFinished];


GO
PRINT N'Creating [dbo].[VolunteerParticipationDays_Id_default]...';


GO
ALTER TABLE [dbo].[VolunteerParticipationDays]
    ADD CONSTRAINT [VolunteerParticipationDays_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[VolunteerToSkill_Id_default]...';


GO
ALTER TABLE [dbo].[VolunteerToSkill]
    ADD CONSTRAINT [VolunteerToSkill_Id_default] DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating [dbo].[FK_AcceptReason_Tenant]...';


GO
ALTER TABLE [dbo].[AcceptReason] WITH NOCHECK
    ADD CONSTRAINT [FK_AcceptReason_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_AcceptReasonTranslation_AcceptReason]...';


GO
ALTER TABLE [dbo].[AcceptReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_AcceptReasonTranslation_AcceptReason] FOREIGN KEY ([AcceptReasonId], [TenantId]) REFERENCES [dbo].[AcceptReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_AcceptReasonTranslation_Language]...';


GO
ALTER TABLE [dbo].[AcceptReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_AcceptReasonTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_AcceptReasonTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[AcceptReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_AcceptReasonTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_AspNetRoleClaims_AspNetRoles_RoleId]...';


GO
ALTER TABLE [dbo].[AspNetRoleClaims] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_AspNetUserClaims_AspNetUsers_UserId]...';


GO
ALTER TABLE [dbo].[AspNetUserClaims] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_AspNetUserLogins_AspNetUsers_UserId]...';


GO
ALTER TABLE [dbo].[AspNetUserLogins] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_AspNetUserRoles_AspNetRoles_RoleId]...';


GO
ALTER TABLE [dbo].[AspNetUserRoles] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_AspNetUserRoles_AspNetUsers_UserId]...';


GO
ALTER TABLE [dbo].[AspNetUserRoles] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_AspNetUsers_Tenant]...';


GO
ALTER TABLE [dbo].[AspNetUsers] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUsers_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_AspNetUserTokens_AspNetUsers_UserId]...';


GO
ALTER TABLE [dbo].[AspNetUserTokens] WITH NOCHECK
    ADD CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE;


GO
PRINT N'Creating [dbo].[FK_City_Tenant]...';


GO
ALTER TABLE [dbo].[City] WITH NOCHECK
    ADD CONSTRAINT [FK_City_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_CityTranslation_City]...';


GO
ALTER TABLE [dbo].[CityTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_CityTranslation_City] FOREIGN KEY ([CityId], [TenantId]) REFERENCES [dbo].[City] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_CityTranslation_Language]...';


GO
ALTER TABLE [dbo].[CityTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_CityTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_CityTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[CityTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_CityTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Clinic_Tenant]...';


GO
ALTER TABLE [dbo].[Clinic] WITH NOCHECK
    ADD CONSTRAINT [FK_Clinic_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_ClinicTranslation_Clinic]...';


GO
ALTER TABLE [dbo].[ClinicTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ClinicTranslation_Clinic] FOREIGN KEY ([ClinicId], [TenantId]) REFERENCES [dbo].[Clinic] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_ClinicTranslation_Language]...';


GO
ALTER TABLE [dbo].[ClinicTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ClinicTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_ClinicTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[ClinicTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ClinicTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Department_Footer]...';


GO
ALTER TABLE [dbo].[Department] WITH NOCHECK
    ADD CONSTRAINT [FK_Department_Footer] FOREIGN KEY ([FooterId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_Department_Header]...';


GO
ALTER TABLE [dbo].[Department] WITH NOCHECK
    ADD CONSTRAINT [FK_Department_Header] FOREIGN KEY ([HeaderId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_Department_Logo]...';


GO
ALTER TABLE [dbo].[Department] WITH NOCHECK
    ADD CONSTRAINT [FK_Department_Logo] FOREIGN KEY ([LogoId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_Department_Tenant]...';


GO
ALTER TABLE [dbo].[Department] WITH NOCHECK
    ADD CONSTRAINT [FK_Department_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Employee_AspNetUsers]...';


GO
ALTER TABLE [dbo].[Employee] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee_AspNetUsers] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]);


GO
PRINT N'Creating [dbo].[FK_Employee_File]...';


GO
ALTER TABLE [dbo].[Employee] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee_File] FOREIGN KEY ([AvatarId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_Employee_Tenant]...';


GO
ALTER TABLE [dbo].[Employee] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_EventLog_Tenant]...';


GO
ALTER TABLE [dbo].[EventLog] WITH NOCHECK
    ADD CONSTRAINT [FK_EventLog_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_File_AspNetUsers_CreatedBy]...';


GO
ALTER TABLE [dbo].[File] WITH NOCHECK
    ADD CONSTRAINT [FK_File_AspNetUsers_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AspNetUsers] ([Id]);


GO
PRINT N'Creating [dbo].[FK_File_AspNetUsers_UpdatedBy]...';


GO
ALTER TABLE [dbo].[File] WITH NOCHECK
    ADD CONSTRAINT [FK_File_AspNetUsers_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[AspNetUsers] ([Id]);


GO
PRINT N'Creating [dbo].[FK_File_Tenant]...';


GO
ALTER TABLE [dbo].[File] WITH NOCHECK
    ADD CONSTRAINT [FK_File_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Interview_Tenant]...';


GO
ALTER TABLE [dbo].[Interview] WITH NOCHECK
    ADD CONSTRAINT [FK_Interview_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_InterviewAppointment_InterviewResult]...';


GO
ALTER TABLE [dbo].[InterviewAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewAppointment_InterviewResult] FOREIGN KEY ([InterviewResultId], [TenantId]) REFERENCES [dbo].[InterviewResult] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewAppointment_InterviewSchedule]...';


GO
ALTER TABLE [dbo].[InterviewAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewAppointment_InterviewSchedule] FOREIGN KEY ([InterviewScheduleId], [TenantId]) REFERENCES [dbo].[InterviewSchedule] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewAppointment_Tenant]...';


GO
ALTER TABLE [dbo].[InterviewAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewAppointment_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_InterviewAppointment_Volunteer]...';


GO
ALTER TABLE [dbo].[InterviewAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewAppointment_Volunteer] FOREIGN KEY ([VolunteerId], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewResult_AcceptReason]...';


GO
ALTER TABLE [dbo].[InterviewResult] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewResult_AcceptReason] FOREIGN KEY ([AcceptReasonId], [TenantId]) REFERENCES [dbo].[AcceptReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewResult_RejectReason]...';


GO
ALTER TABLE [dbo].[InterviewResult] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewResult_RejectReason] FOREIGN KEY ([RejectReasonId], [TenantId]) REFERENCES [dbo].[RejectReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewResult_Tenant]...';


GO
ALTER TABLE [dbo].[InterviewResult] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewResult_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_InterviewSchedule_Interview]...';


GO
ALTER TABLE [dbo].[InterviewSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewSchedule_Interview] FOREIGN KEY ([InterviewId], [TenantId]) REFERENCES [dbo].[Interview] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_InterviewSchedule_Tenant]...';


GO
ALTER TABLE [dbo].[InterviewSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_InterviewSchedule_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_JoiningReason_Tenant]...';


GO
ALTER TABLE [dbo].[JoiningReason] WITH NOCHECK
    ADD CONSTRAINT [FK_JoiningReason_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_JoiningReasonTranslation_JoiningReason]...';


GO
ALTER TABLE [dbo].[JoiningReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_JoiningReasonTranslation_JoiningReason] FOREIGN KEY ([JoiningReasonId], [TenantId]) REFERENCES [dbo].[JoiningReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_JoiningReasonTranslation_Language]...';


GO
ALTER TABLE [dbo].[JoiningReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_JoiningReasonTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_JoiningReasonTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[JoiningReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_JoiningReasonTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Language_Tenant]...';


GO
ALTER TABLE [dbo].[Language] WITH NOCHECK
    ADD CONSTRAINT [FK_Language_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Medical_Tenant]...';


GO
ALTER TABLE [dbo].[Medical] WITH NOCHECK
    ADD CONSTRAINT [FK_Medical_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_MedicalAppointment_MedicalResult]...';


GO
ALTER TABLE [dbo].[MedicalAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalAppointment_MedicalResult] FOREIGN KEY ([MedicalResultId], [TenantId]) REFERENCES [dbo].[MedicalResult] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalAppointment_MedicalSchedule]...';


GO
ALTER TABLE [dbo].[MedicalAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalAppointment_MedicalSchedule] FOREIGN KEY ([MedicalScheduleId], [TenantId]) REFERENCES [dbo].[MedicalSchedule] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalAppointment_Tenant]...';


GO
ALTER TABLE [dbo].[MedicalAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalAppointment_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_MedicalAppointment_Volunteer]...';


GO
ALTER TABLE [dbo].[MedicalAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalAppointment_Volunteer] FOREIGN KEY ([VolunteerId], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalResult_Tenant]...';


GO
ALTER TABLE [dbo].[MedicalResult] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalResult_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_MedicalResultFiles_File]...';


GO
ALTER TABLE [dbo].[MedicalResultFiles] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalResultFiles_File] FOREIGN KEY ([FileId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_MedicalResultFiles_MedicalResult]...';


GO
ALTER TABLE [dbo].[MedicalResultFiles] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalResultFiles_MedicalResult] FOREIGN KEY ([MedicalResultId], [TenantId]) REFERENCES [dbo].[MedicalResult] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalResultFiles_Tenant]...';


GO
ALTER TABLE [dbo].[MedicalResultFiles] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalResultFiles_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_MedicalSchedule_City]...';


GO
ALTER TABLE [dbo].[MedicalSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalSchedule_City] FOREIGN KEY ([CityId], [TenantId]) REFERENCES [dbo].[City] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalSchedule_Clinic]...';


GO
ALTER TABLE [dbo].[MedicalSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalSchedule_Clinic] FOREIGN KEY ([ClinicId], [TenantId]) REFERENCES [dbo].[Clinic] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalSchedule_Employee]...';


GO
ALTER TABLE [dbo].[MedicalSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalSchedule_Employee] FOREIGN KEY ([PhysicianId], [TenantId]) REFERENCES [dbo].[Employee] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalSchedule_Medical]...';


GO
ALTER TABLE [dbo].[MedicalSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalSchedule_Medical] FOREIGN KEY ([MedicalId], [TenantId]) REFERENCES [dbo].[Medical] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_MedicalSchedule_Tenant]...';


GO
ALTER TABLE [dbo].[MedicalSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_MedicalSchedule_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Profession_Tenant]...';


GO
ALTER TABLE [dbo].[Profession] WITH NOCHECK
    ADD CONSTRAINT [FK_Profession_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_ProfessionTranslation_Language]...';


GO
ALTER TABLE [dbo].[ProfessionTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ProfessionTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_ProfessionTranslation_Profession]...';


GO
ALTER TABLE [dbo].[ProfessionTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ProfessionTranslation_Profession] FOREIGN KEY ([ProfessionId], [TenantId]) REFERENCES [dbo].[Profession] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_ProfessionTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[ProfessionTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_ProfessionTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_RejectReason_Tenant]...';


GO
ALTER TABLE [dbo].[RejectReason] WITH NOCHECK
    ADD CONSTRAINT [FK_RejectReason_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_RejectReasonTranslation_Language]...';


GO
ALTER TABLE [dbo].[RejectReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_RejectReasonTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_RejectReasonTranslation_RejectReason]...';


GO
ALTER TABLE [dbo].[RejectReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_RejectReasonTranslation_RejectReason] FOREIGN KEY ([RejectReasonId], [TenantId]) REFERENCES [dbo].[RejectReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_RejectReasonTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[RejectReasonTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_RejectReasonTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Skill_Tenant]...';


GO
ALTER TABLE [dbo].[Skill] WITH NOCHECK
    ADD CONSTRAINT [FK_Skill_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_SkillTranslation_Language]...';


GO
ALTER TABLE [dbo].[SkillTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_SkillTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_SkillTranslation_Skill]...';


GO
ALTER TABLE [dbo].[SkillTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_SkillTranslation_Skill] FOREIGN KEY ([SkillId], [TenantId]) REFERENCES [dbo].[Skill] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_SkillTranslation_Tenant]...';


GO
ALTER TABLE [dbo].[SkillTranslation] WITH NOCHECK
    ADD CONSTRAINT [FK_SkillTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Tenant_Admin]...';


GO
ALTER TABLE [dbo].[Tenant] WITH NOCHECK
    ADD CONSTRAINT [FK_Tenant_Admin] FOREIGN KEY ([AdminId], [ID]) REFERENCES [dbo].[Employee] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_TenantToSystemModule_Tenant]...';


GO
ALTER TABLE [dbo].[TenantToSystemModule] WITH NOCHECK
    ADD CONSTRAINT [FK_TenantToSystemModule_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Training_Tenant]...';


GO
ALTER TABLE [dbo].[Training] WITH NOCHECK
    ADD CONSTRAINT [FK_Training_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_TrainingAppointment_Tenant]...';


GO
ALTER TABLE [dbo].[TrainingAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingAppointment_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_TrainingAppointment_TrainingResult]...';


GO
ALTER TABLE [dbo].[TrainingAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingAppointment_TrainingResult] FOREIGN KEY ([TrainingResultId], [TenantId]) REFERENCES [dbo].[TrainingResult] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_TrainingAppointment_TrainingSchedule]...';


GO
ALTER TABLE [dbo].[TrainingAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingAppointment_TrainingSchedule] FOREIGN KEY ([TrainingScheduleId], [TenantId]) REFERENCES [dbo].[TrainingSchedule] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_TrainingAppointment_Volunteer]...';


GO
ALTER TABLE [dbo].[TrainingAppointment] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingAppointment_Volunteer] FOREIGN KEY ([VolunteerId], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_TrainingResult_Tenant]...';


GO
ALTER TABLE [dbo].[TrainingResult] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingResult_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_TrainingSchedule_Tenant]...';


GO
ALTER TABLE [dbo].[TrainingSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingSchedule_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_TrainingSchedule_Training]...';


GO
ALTER TABLE [dbo].[TrainingSchedule] WITH NOCHECK
    ADD CONSTRAINT [FK_TrainingSchedule_Training] FOREIGN KEY ([TrainingId], [TenantId]) REFERENCES [dbo].[Training] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_AspNetUsers]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_AspNetUsers] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_City]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_City] FOREIGN KEY ([CityId], [TenantId]) REFERENCES [dbo].[City] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_File]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_File] FOREIGN KEY ([AvatarId], [TenantId]) REFERENCES [dbo].[File] ([ID], [TenantID]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_JoiningReason]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_JoiningReason] FOREIGN KEY ([JoiningReasonId], [TenantId]) REFERENCES [dbo].[JoiningReason] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_Profession]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_Profession] FOREIGN KEY ([ProfessionId], [TenantId]) REFERENCES [dbo].[Profession] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_Volunteer_Tenant]...';


GO
ALTER TABLE [dbo].[Volunteer] WITH NOCHECK
    ADD CONSTRAINT [FK_Volunteer_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_VolunteerParticipationDays_Tenant]...';


GO
ALTER TABLE [dbo].[VolunteerParticipationDays] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerParticipationDays_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_VolunteerParticipationDays_Volunteer]...';


GO
ALTER TABLE [dbo].[VolunteerParticipationDays] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerParticipationDays_Volunteer] FOREIGN KEY ([VolunteerId], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_VolunteerRegistration_Tenant]...';


GO
ALTER TABLE [dbo].[VolunteerRegistration] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerRegistration_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[FK_VolunteerRegistration_Volunteer]...';


GO
ALTER TABLE [dbo].[VolunteerRegistration] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerRegistration_Volunteer] FOREIGN KEY ([Id], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_VolunteerSkills_Skill]...';


GO
ALTER TABLE [dbo].[VolunteerToSkill] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerSkills_Skill] FOREIGN KEY ([SkillId], [TenantId]) REFERENCES [dbo].[Skill] ([Id], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_VolunteerSkills_Volunteer]...';


GO
ALTER TABLE [dbo].[VolunteerToSkill] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerSkills_Volunteer] FOREIGN KEY ([VolunteerId], [TenantId]) REFERENCES [dbo].[Volunteer] ([ID], [TenantId]);


GO
PRINT N'Creating [dbo].[FK_VolunteerToSkill_Tenant]...';


GO
ALTER TABLE [dbo].[VolunteerToSkill] WITH NOCHECK
    ADD CONSTRAINT [FK_VolunteerToSkill_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([ID]);


GO
PRINT N'Creating [dbo].[VolunteerSearchSync]...';


GO
CREATE TRIGGER dbo.VolunteerSearchSync
ON  dbo.Volunteer
   AFTER INSERT, UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE Volunteer SET SearchContents = s.SearchContents
	FROM Volunteer c INNER JOIN
		(
			SELECT c.ID,
			ISNULL(c.FirstName, '') + ' ' +
			ISNULL(c.MiddleName, '') + ' ' +
			ISNULL(c.LastName, '') + ' ' +
			ISNULL(c.Email, '') + ' ' +
			ISNULL(c.PhoneNo, '') + ' ' +
			ISNULL(c.NationalId, '') + ' ' +
			ISNULL(c.PassportNo, '') as SearchContents
			FROM Volunteer c INNER JOIN inserted u ON c.ID = u.ID

			) s ON (s.ID = c.ID)
END
GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [mvms];


GO
ALTER TABLE [dbo].[AcceptReason] WITH CHECK CHECK CONSTRAINT [FK_AcceptReason_Tenant];

ALTER TABLE [dbo].[AcceptReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_AcceptReasonTranslation_AcceptReason];

ALTER TABLE [dbo].[AcceptReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_AcceptReasonTranslation_Language];

ALTER TABLE [dbo].[AcceptReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_AcceptReasonTranslation_Tenant];

ALTER TABLE [dbo].[AspNetRoleClaims] WITH CHECK CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId];

ALTER TABLE [dbo].[AspNetUserClaims] WITH CHECK CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId];

ALTER TABLE [dbo].[AspNetUserLogins] WITH CHECK CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId];

ALTER TABLE [dbo].[AspNetUserRoles] WITH CHECK CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId];

ALTER TABLE [dbo].[AspNetUserRoles] WITH CHECK CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId];

ALTER TABLE [dbo].[AspNetUsers] WITH CHECK CHECK CONSTRAINT [FK_AspNetUsers_Tenant];

ALTER TABLE [dbo].[AspNetUserTokens] WITH CHECK CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId];

ALTER TABLE [dbo].[City] WITH CHECK CHECK CONSTRAINT [FK_City_Tenant];

ALTER TABLE [dbo].[CityTranslation] WITH CHECK CHECK CONSTRAINT [FK_CityTranslation_City];

ALTER TABLE [dbo].[CityTranslation] WITH CHECK CHECK CONSTRAINT [FK_CityTranslation_Language];

ALTER TABLE [dbo].[CityTranslation] WITH CHECK CHECK CONSTRAINT [FK_CityTranslation_Tenant];

ALTER TABLE [dbo].[Clinic] WITH CHECK CHECK CONSTRAINT [FK_Clinic_Tenant];

ALTER TABLE [dbo].[ClinicTranslation] WITH CHECK CHECK CONSTRAINT [FK_ClinicTranslation_Clinic];

ALTER TABLE [dbo].[ClinicTranslation] WITH CHECK CHECK CONSTRAINT [FK_ClinicTranslation_Language];

ALTER TABLE [dbo].[ClinicTranslation] WITH CHECK CHECK CONSTRAINT [FK_ClinicTranslation_Tenant];

ALTER TABLE [dbo].[Department] WITH CHECK CHECK CONSTRAINT [FK_Department_Footer];

ALTER TABLE [dbo].[Department] WITH CHECK CHECK CONSTRAINT [FK_Department_Header];

ALTER TABLE [dbo].[Department] WITH CHECK CHECK CONSTRAINT [FK_Department_Logo];

ALTER TABLE [dbo].[Department] WITH CHECK CHECK CONSTRAINT [FK_Department_Tenant];

ALTER TABLE [dbo].[Employee] WITH CHECK CHECK CONSTRAINT [FK_Employee_AspNetUsers];

ALTER TABLE [dbo].[Employee] WITH CHECK CHECK CONSTRAINT [FK_Employee_File];

ALTER TABLE [dbo].[Employee] WITH CHECK CHECK CONSTRAINT [FK_Employee_Tenant];

ALTER TABLE [dbo].[EventLog] WITH CHECK CHECK CONSTRAINT [FK_EventLog_Tenant];

ALTER TABLE [dbo].[File] WITH CHECK CHECK CONSTRAINT [FK_File_AspNetUsers_CreatedBy];

ALTER TABLE [dbo].[File] WITH CHECK CHECK CONSTRAINT [FK_File_AspNetUsers_UpdatedBy];

ALTER TABLE [dbo].[File] WITH CHECK CHECK CONSTRAINT [FK_File_Tenant];

ALTER TABLE [dbo].[Interview] WITH CHECK CHECK CONSTRAINT [FK_Interview_Tenant];

ALTER TABLE [dbo].[InterviewAppointment] WITH CHECK CHECK CONSTRAINT [FK_InterviewAppointment_InterviewResult];

ALTER TABLE [dbo].[InterviewAppointment] WITH CHECK CHECK CONSTRAINT [FK_InterviewAppointment_InterviewSchedule];

ALTER TABLE [dbo].[InterviewAppointment] WITH CHECK CHECK CONSTRAINT [FK_InterviewAppointment_Tenant];

ALTER TABLE [dbo].[InterviewAppointment] WITH CHECK CHECK CONSTRAINT [FK_InterviewAppointment_Volunteer];

ALTER TABLE [dbo].[InterviewResult] WITH CHECK CHECK CONSTRAINT [FK_InterviewResult_AcceptReason];

ALTER TABLE [dbo].[InterviewResult] WITH CHECK CHECK CONSTRAINT [FK_InterviewResult_RejectReason];

ALTER TABLE [dbo].[InterviewResult] WITH CHECK CHECK CONSTRAINT [FK_InterviewResult_Tenant];

ALTER TABLE [dbo].[InterviewSchedule] WITH CHECK CHECK CONSTRAINT [FK_InterviewSchedule_Interview];

ALTER TABLE [dbo].[InterviewSchedule] WITH CHECK CHECK CONSTRAINT [FK_InterviewSchedule_Tenant];

ALTER TABLE [dbo].[JoiningReason] WITH CHECK CHECK CONSTRAINT [FK_JoiningReason_Tenant];

ALTER TABLE [dbo].[JoiningReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_JoiningReasonTranslation_JoiningReason];

ALTER TABLE [dbo].[JoiningReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_JoiningReasonTranslation_Language];

ALTER TABLE [dbo].[JoiningReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_JoiningReasonTranslation_Tenant];

ALTER TABLE [dbo].[Language] WITH CHECK CHECK CONSTRAINT [FK_Language_Tenant];

ALTER TABLE [dbo].[Medical] WITH CHECK CHECK CONSTRAINT [FK_Medical_Tenant];

ALTER TABLE [dbo].[MedicalAppointment] WITH CHECK CHECK CONSTRAINT [FK_MedicalAppointment_MedicalResult];

ALTER TABLE [dbo].[MedicalAppointment] WITH CHECK CHECK CONSTRAINT [FK_MedicalAppointment_MedicalSchedule];

ALTER TABLE [dbo].[MedicalAppointment] WITH CHECK CHECK CONSTRAINT [FK_MedicalAppointment_Tenant];

ALTER TABLE [dbo].[MedicalAppointment] WITH CHECK CHECK CONSTRAINT [FK_MedicalAppointment_Volunteer];

ALTER TABLE [dbo].[MedicalResult] WITH CHECK CHECK CONSTRAINT [FK_MedicalResult_Tenant];

ALTER TABLE [dbo].[MedicalResultFiles] WITH CHECK CHECK CONSTRAINT [FK_MedicalResultFiles_File];

ALTER TABLE [dbo].[MedicalResultFiles] WITH CHECK CHECK CONSTRAINT [FK_MedicalResultFiles_MedicalResult];

ALTER TABLE [dbo].[MedicalResultFiles] WITH CHECK CHECK CONSTRAINT [FK_MedicalResultFiles_Tenant];

ALTER TABLE [dbo].[MedicalSchedule] WITH CHECK CHECK CONSTRAINT [FK_MedicalSchedule_City];

ALTER TABLE [dbo].[MedicalSchedule] WITH CHECK CHECK CONSTRAINT [FK_MedicalSchedule_Clinic];

ALTER TABLE [dbo].[MedicalSchedule] WITH CHECK CHECK CONSTRAINT [FK_MedicalSchedule_Employee];

ALTER TABLE [dbo].[MedicalSchedule] WITH CHECK CHECK CONSTRAINT [FK_MedicalSchedule_Medical];

ALTER TABLE [dbo].[MedicalSchedule] WITH CHECK CHECK CONSTRAINT [FK_MedicalSchedule_Tenant];

ALTER TABLE [dbo].[Profession] WITH CHECK CHECK CONSTRAINT [FK_Profession_Tenant];

ALTER TABLE [dbo].[ProfessionTranslation] WITH CHECK CHECK CONSTRAINT [FK_ProfessionTranslation_Language];

ALTER TABLE [dbo].[ProfessionTranslation] WITH CHECK CHECK CONSTRAINT [FK_ProfessionTranslation_Profession];

ALTER TABLE [dbo].[ProfessionTranslation] WITH CHECK CHECK CONSTRAINT [FK_ProfessionTranslation_Tenant];

ALTER TABLE [dbo].[RejectReason] WITH CHECK CHECK CONSTRAINT [FK_RejectReason_Tenant];

ALTER TABLE [dbo].[RejectReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_RejectReasonTranslation_Language];

ALTER TABLE [dbo].[RejectReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_RejectReasonTranslation_RejectReason];

ALTER TABLE [dbo].[RejectReasonTranslation] WITH CHECK CHECK CONSTRAINT [FK_RejectReasonTranslation_Tenant];

ALTER TABLE [dbo].[Skill] WITH CHECK CHECK CONSTRAINT [FK_Skill_Tenant];

ALTER TABLE [dbo].[SkillTranslation] WITH CHECK CHECK CONSTRAINT [FK_SkillTranslation_Language];

ALTER TABLE [dbo].[SkillTranslation] WITH CHECK CHECK CONSTRAINT [FK_SkillTranslation_Skill];

ALTER TABLE [dbo].[SkillTranslation] WITH CHECK CHECK CONSTRAINT [FK_SkillTranslation_Tenant];

ALTER TABLE [dbo].[Tenant] WITH CHECK CHECK CONSTRAINT [FK_Tenant_Admin];

ALTER TABLE [dbo].[TenantToSystemModule] WITH CHECK CHECK CONSTRAINT [FK_TenantToSystemModule_Tenant];

ALTER TABLE [dbo].[Training] WITH CHECK CHECK CONSTRAINT [FK_Training_Tenant];

ALTER TABLE [dbo].[TrainingAppointment] WITH CHECK CHECK CONSTRAINT [FK_TrainingAppointment_Tenant];

ALTER TABLE [dbo].[TrainingAppointment] WITH CHECK CHECK CONSTRAINT [FK_TrainingAppointment_TrainingResult];

ALTER TABLE [dbo].[TrainingAppointment] WITH CHECK CHECK CONSTRAINT [FK_TrainingAppointment_TrainingSchedule];

ALTER TABLE [dbo].[TrainingAppointment] WITH CHECK CHECK CONSTRAINT [FK_TrainingAppointment_Volunteer];

ALTER TABLE [dbo].[TrainingResult] WITH CHECK CHECK CONSTRAINT [FK_TrainingResult_Tenant];

ALTER TABLE [dbo].[TrainingSchedule] WITH CHECK CHECK CONSTRAINT [FK_TrainingSchedule_Tenant];

ALTER TABLE [dbo].[TrainingSchedule] WITH CHECK CHECK CONSTRAINT [FK_TrainingSchedule_Training];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_AspNetUsers];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_City];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_File];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_JoiningReason];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_Profession];

ALTER TABLE [dbo].[Volunteer] WITH CHECK CHECK CONSTRAINT [FK_Volunteer_Tenant];

ALTER TABLE [dbo].[VolunteerParticipationDays] WITH CHECK CHECK CONSTRAINT [FK_VolunteerParticipationDays_Tenant];

ALTER TABLE [dbo].[VolunteerParticipationDays] WITH CHECK CHECK CONSTRAINT [FK_VolunteerParticipationDays_Volunteer];

ALTER TABLE [dbo].[VolunteerRegistration] WITH CHECK CHECK CONSTRAINT [FK_VolunteerRegistration_Tenant];

ALTER TABLE [dbo].[VolunteerRegistration] WITH CHECK CHECK CONSTRAINT [FK_VolunteerRegistration_Volunteer];

ALTER TABLE [dbo].[VolunteerToSkill] WITH CHECK CHECK CONSTRAINT [FK_VolunteerSkills_Skill];

ALTER TABLE [dbo].[VolunteerToSkill] WITH CHECK CHECK CONSTRAINT [FK_VolunteerSkills_Volunteer];

ALTER TABLE [dbo].[VolunteerToSkill] WITH CHECK CHECK CONSTRAINT [FK_VolunteerToSkill_Tenant];


CREATE FULLTEXT INDEX ON [dbo].[Volunteer]
    ([SearchContents] LANGUAGE 1033)
    KEY INDEX [FTUX_Volunteer]
    ON [ftCatalog];


GO
PRINT N'Update complete.';


GO
