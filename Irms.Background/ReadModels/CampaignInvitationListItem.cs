﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Background.ReadModels
{
    public class CampaingInvitationListItem
    {
        public Guid Id { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
    }
}
