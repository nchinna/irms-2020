﻿using Irms.Application.CampaignInvitations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Background.ReadModels
{
    public class PrefferedMediaReadModel
    {
        public PrefferedMediaReadModel() { }
        public PrefferedMediaReadModel(EventGuest guest, bool hasEmail, bool hasSms)
        {
            Guest = guest;
            HasEmail = hasEmail;
            HasSms = hasSms;
        }

        public EventGuest Guest { get; }
        public bool HasEmail { get; }
        public bool HasSms { get; }
    }
}
