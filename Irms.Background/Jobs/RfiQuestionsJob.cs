﻿using AutoMapper;
using Hangfire;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Background.Abstract;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Domain.Entities.ContactListRFI;

namespace Irms.Background.Jobs
{
    [AutomaticRetry(Attempts = 0)]
    public class RfiQuestionsJob
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IBackgroundJobsHelperService _backgroundJobService;

        private const string EmailSubject = "RFI";
        private const string DefaultSmsMessage = "Please fill info on {{rfi_link}}";
        private const string DefaultEmailMessage = "Please fill info on {{rfi_link}}";

        public RfiQuestionsJob(IMediator mediator,
            IMapper mapper,
            IBackgroundJobsHelperService backgroundJobsHelper)
        {
            _mediator = mediator;
            _mapper = mapper;
            _backgroundJobService = backgroundJobsHelper;
        }

        public async Task DoActions(Guid contactRfiId, CancellationToken token)
        {
            var rfi = await _backgroundJobService.FindContactListRfi(contactRfiId, token);
            if(rfi == null)
            {
                throw new IncorrectRequestException($"Contact list rfi with id {contactRfiId} was not found");
            }

            await _backgroundJobService.UpdateContactRfiFormStatus(rfi.Id, ContactListRfiStatus.InProgress, token);
            var prefferedMedias = await _backgroundJobService.FetchPrefferedMediasFromContactListRfiForm(rfi.GuestListId, token);
            
            var answers = await _backgroundJobService.CreateContactListRfiAnswers(prefferedMedias, rfi, token);

            foreach (var prefference in prefferedMedias)
            {
                if(prefference.HasEmail)
                {
                    var emailAnswer = answers
                        .FirstOrDefault(x => x.ContactId == prefference.Guest.Id 
                        && x.MediaType == MediaType.Email);

                    string body = DefaultEmailMessage
                        .Replace("{{rfi_link}}", emailAnswer.Id.ToString());

                    await _backgroundJobService.SendContactListRfiEmail(prefference, EmailSubject, body, rfi.TenantId, token);
                }

                if(prefference.HasSms)
                {
                    var smsAnswer = answers
                        .FirstOrDefault(x => x.ContactId == prefference.Guest.Id
                        && x.MediaType == MediaType.Sms);

                    string body = DefaultSmsMessage
                        .Replace("{{rfi_link}}", smsAnswer.Id.ToString());

                    await _backgroundJobService.SendContactListRfiSms(prefference, body, rfi.TenantId, token);
                }
            }

            await _backgroundJobService.UpdateContactRfiFormStatus(rfi.Id, ContactListRfiStatus.Sent, token);
        }
    }
}
