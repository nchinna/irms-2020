﻿using System;

namespace Irms.Domain.Entities
{
    public class ProviderResponse
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationMessageLogId { get; set; }
        public int MessageStatus { get; set; }
        public int ProviderType { get; set; }
        public DateTime ResponseDate { get; set; }
        public string ResponseJson { get; set; }
    }
}
