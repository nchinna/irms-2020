﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class CampaignEmailTemplate : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string Subject { get; set; }
        public string Preheader { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string PlainBody { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }

        /// <summary>
        /// intializing data
        /// </summary>
        public void Create()
        {
            Id = Guid.NewGuid();

            //set default html
            (string accept, string reject) = GetDefaultHtml();
            AcceptHtml = accept;
            RejectHtml = reject;
        }

        private (string accept, string reject) GetDefaultHtml()
        {
            string accept = $"<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">Thank you for your response!</span></p>" +
                $"<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We will see you at the&nbsp;</span></p>" +
                $"<h2 style=\"text-align: center;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>" +
                $"<p style=\"text-align: center;\">&nbsp;</p>";

            string reject = $"<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">Thank you for your response!</span></p>" +
                $"<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We were expecting you to see at the&nbsp;&nbsp;</span></p>" +
                $"<h2 style=\"text-align: center;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>" +
                $"         <p style=\"text-align: center;\">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>";

            return (accept, reject);
        }
    }
}
