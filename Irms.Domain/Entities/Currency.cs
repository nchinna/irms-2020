﻿namespace Irms.Domain.Entities
{
    public class Currency
    {
        public Currency(byte id)
        {
            Id = id;
        }

        public Currency(byte id, string title) : this(id)
        {
            Title = title;
        }

        public byte Id { get; set; }
        public string Title { get; set; }
    }
}