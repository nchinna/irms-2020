﻿using Irms.Domain.Abstract;
using System;

namespace Irms.Domain.Entities
{
    public class ServiceCatalogFeature : IEntity<Guid>
    {
        public ServiceCatalogFeature(Guid featureId, string featureTitle, bool isFeatureEnabled, bool? isCustom)
        {
            Id = featureId;
            FeatureTitle = featureTitle;
            IsFeatureEnabled = isFeatureEnabled;
            IsCustom = isCustom;
        }
        public Guid Id { get; set; }

        public bool? IsCustom { get; set; }
        public string FeatureTitle { get; set; }
        public bool IsFeatureEnabled { get; set; }
    }
}
