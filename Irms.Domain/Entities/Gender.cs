﻿namespace Irms.Domain.Entities
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}
