﻿using System;

namespace Irms.Domain.Entities
{
    public class Translation
    {
        public Translation(Guid languageId, string value)
        {
            Value = value;
            LanguageId = languageId;
        }

        public Guid LanguageId { get; }
        public string Value { get; }
    }
}
