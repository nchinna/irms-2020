﻿using System;

namespace Irms.Domain.Entities.Templates
{
    public class TemplateTranslation
    {
        public TemplateTranslation(Guid languageId, string smsText)
        {
            LanguageId = languageId;
            SmsText = smsText;
        }

        public TemplateTranslation(Guid languageId, string emailSubject, string emailBody)
        {
            LanguageId = languageId;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
        }

        public TemplateTranslation(Guid languageId, string emailSubject, string emailBody, string smsText)
        {
            LanguageId = languageId;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            SmsText = smsText;
        }

        public Guid LanguageId { get; }

        public string EmailSubject { get; }
        public string EmailBody { get; }

        public string SmsText { get; }
    }
}
