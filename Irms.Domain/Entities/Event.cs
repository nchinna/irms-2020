﻿using System;
using System.Collections.Generic;
using System.Linq;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class Event : IEntity<Guid>
    {
        public Event()
        {
            Location = new EventLocation();
        }

        //event info
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public short? EventTypeId { get; set; }
        public bool IsLimitedAttendees { get; set; }
        public int? MaximumAttendees { get; set; }
        public int? OverflowAttendees { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        //public string TimeZoneUtcOffset { get; set; }
        //public string TimeZoneName { get; set; }

        //features & location
        public List<Guid> Features { get; set; }
        public EventLocation Location { get; set; }

        /// <summary>
        /// intializing ids, dates
        /// </summary>
        public void Create(Guid tenantId, string features)
        {
            Id = Guid.NewGuid();
            Features = features.Split(',').Select(s => Guid.Parse(s)).ToList();
        }

        /// <summary>
        /// set logo path
        /// </summary>
        public void SetImagePath(string path)
        {
            ImagePath = path;
        }

        /// <summary>
        /// intializing contact info
        /// </summary>
        public void CreateLocation(EventLocation location)
        {
            Location = location;
            Location.Id = Guid.NewGuid();
            Location.EventId = Id;
        }
    }

    public class EventLocation
    {
        //location info
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Country { get; set; }
    }
}
