﻿namespace Irms.Domain.Entities
{
    public enum InvitationStatus
    {
        Draft = 0,
        InProgress = 1,
        Sent = 2
    }
}
