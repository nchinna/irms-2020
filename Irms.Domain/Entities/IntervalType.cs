﻿namespace Irms.Domain.Entities
{
    public enum IntervalType
    {
        Days = 0,
        Hours = 1,
        Minutes = 2,
    }
}
