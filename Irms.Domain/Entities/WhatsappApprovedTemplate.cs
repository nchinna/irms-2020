﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class WhatsappApprovedTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public int InvitationTypeId { get; set; }
        public string Name { get; set; }
        public string TemplateBody { get; set; }
        public bool HasRfi { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.UtcNow;
        }
    }
}
