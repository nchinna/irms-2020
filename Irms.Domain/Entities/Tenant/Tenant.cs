﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities.Tenant
{
    public class Tenant : IEntity<Guid>
    {
        public Tenant()
        {
            ContactInfo = new TenantContactInfo();
        }
        //tenant info
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string LogoPath { get; set; }
        public Guid CountryId { get; set; }
        public string City { get; set; }
        public string ClientUrl { get; set; }
        public string EmailFrom { get; set; }
        public string SendGridApiKey { get; set; }
        public string MalathUsername { get; set; }
        public string MalathPassword { get; set; }
        public string MalathSender { get; set; }
        public string UnifonicSid { get; set; }
        public string UnifonicSender { get; set; }
        public string TwilioAccountSid { get; set; }
        public string TwilioAuthToken { get; set; }
        public string TwilioNotificationServiceId { get; set; }
        public string HeaderPath { get; set; }
        public string FooterPath { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool? HasOwnDomain { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Guid? AdminId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }

        public TenantContactInfo ContactInfo { get; set; }

        /// <summary>
        /// intializing ids, dates
        /// </summary>
        public void Create()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.UtcNow;

        }

        /// <summary>
        /// set logo path
        /// </summary>
        public void SetLogoPath(string path)
        {
            LogoPath = path;
        }

        public void SetDefaultConfig(
            string sendGridApiKey,
            string twilioAccountSid,
            string twilioAuthToken,
            string twilioNotificationServiceId)
        {
            SendGridApiKey = sendGridApiKey;
            TwilioAccountSid = twilioAccountSid;
            TwilioAuthToken = twilioAuthToken;
            TwilioNotificationServiceId = twilioNotificationServiceId;
        }
        /// <summary>
        /// intializing contact info
        /// </summary>
        public void CreateContactInfo(TenantContactInfo contactInfo)
        {
            ContactInfo = contactInfo;
            ContactInfo.Id = Guid.NewGuid();
            ContactInfo.TenantId = Id;
        }

        public void CanActivate()
        {
            if (IsActive && string.IsNullOrEmpty(ClientUrl))
            {
                throw new IncorrectRequestException("Can't activate customer. Please add configuration.");
            }
        }
        /// <summary>
        /// disable tenant
        /// </summary>
        public void Disable()
        {
            IsActive = false;
            UpdatedOn = DateTime.UtcNow;
        }

        /// <summary>
        /// enable tenant
        /// </summary>
        public void Enable()
        {
            IsActive = true;
            UpdatedOn = DateTime.UtcNow;
        }

        /// <summary>
        /// update tenant fields
        /// </summary>
        /// <param name="name"></param>
        /// <param name="expiryDate"></param>
        /// <param name="description"></param>
        public void Update(string name, DateTime expiryDate, string description)
        {
            throw new NotImplementedException();
        }
    }
    public class TenantContactInfo
    {
        //contact info
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
    }
}
