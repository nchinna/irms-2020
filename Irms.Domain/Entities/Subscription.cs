﻿using Irms.Domain.Abstract;
using System;

namespace Irms.Domain.Entities
{
    public class Subscription : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ProductId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime ExpiryDateTime { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            StartDateTime = DateTime.UtcNow;
            IsActive = true;
            CreatedOn = DateTime.UtcNow;
        }
    }
}
