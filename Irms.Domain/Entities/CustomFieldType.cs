﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public enum CustomFieldType
    {
        Text = 0,
        Number = 1,
        Date = 2,
        Email = 3,
        Phone = 4
    }
}
