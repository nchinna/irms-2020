﻿namespace Irms.Domain.Entities
{
    public enum CampaignStatus
    {
        Draft = 0,
        Live = 1,
        Disabled = 2
    }
}
