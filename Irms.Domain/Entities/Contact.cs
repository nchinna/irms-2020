﻿using System;
using System.Collections.Generic;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class Contact : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }

        public string Title { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public int? GenderId { get; set; }

        public string Email { get; set; }
        public string AlternativeEmail { get; set; }

        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }

        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public Guid? IssuingCountryId { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }


        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public bool? IsGuest { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public IEnumerable<ContactCustomField> CustomFields { get; set; }

        /// <summary>
        /// intializing ids, dates
        /// </summary>
        public void Create(Guid tenantId)
        {
            Id = Guid.NewGuid();
            TenantId = tenantId;
            Active = true;
            Deleted = false;
            CreatedOn = DateTime.UtcNow;
        }

        public void Update()
        {
            ModifiedOn = DateTime.UtcNow;
        }

        public void SetGuest()
        {
            IsGuest = true;
        }
    }
}
