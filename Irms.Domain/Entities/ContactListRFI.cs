﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ContactListRFI : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid RfiFormId { get; set; }
        public Guid GuestListId { get; set; }
        public ContactListRfiStatus Status { get; set; }
        public DateTime? SentDate { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public void Create(Guid tenantId)
        {
            Id = Guid.NewGuid();
            TenantId = tenantId;
            CreatedOn = DateTime.UtcNow;
        }

        public enum ContactListRfiStatus
        {
            Draft = 0,
            InProgress = 1,
            Sent = 2
        }
    }
}
