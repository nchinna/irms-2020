﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ContactListRFIAnswer : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }

        public Guid ContactId { get; set; }
        public Guid ContactListRfiId { get; set; }
        public DateTime? DateAnswered { get; set; }
        public MediaType? MediaType { get; set; }

        public void Create(Guid tenantId)
        {
            Id = Guid.NewGuid();
            TenantId = tenantId;
        }
    }
}
