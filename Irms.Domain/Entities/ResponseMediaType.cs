﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public enum ResponseMediaType : int
    {
        UNSET = 0,
        ACCEPTED = 1,
        REJECTED = 2
    }
}
