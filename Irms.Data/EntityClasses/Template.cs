﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Template
    {
        public Template()
        {
            TemplateSelectionTemplate = new HashSet<TemplateSelection>();
            TemplateSelectionTemplateNavigation = new HashSet<TemplateSelection>();
            TemplateTranslation = new HashSet<TemplateTranslation>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public bool SmsCompatible { get; set; }
        public bool EmailCompatible { get; set; }
        public string SmsText { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid ModifiedById { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual UserInfo UserInfo { get; set; }
        public virtual ICollection<TemplateSelection> TemplateSelectionTemplate { get; set; }
        public virtual ICollection<TemplateSelection> TemplateSelectionTemplateNavigation { get; set; }
        public virtual ICollection<TemplateTranslation> TemplateTranslation { get; set; }
    }
}
