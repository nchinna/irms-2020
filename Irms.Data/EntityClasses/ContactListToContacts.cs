﻿using System;

namespace Irms.Data.EntityClasses
{
    public class ContactListToContacts
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactListId { get; set; }
        public Guid ContactId { get; set; }

        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ContactList ContactList { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
