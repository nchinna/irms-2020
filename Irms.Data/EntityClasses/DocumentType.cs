﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class DocumentType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
