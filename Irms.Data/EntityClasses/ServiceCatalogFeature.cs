﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class ServiceCatalogFeature
    {
        public ServiceCatalogFeature()
        {
            ProductServiceCatalogFeature = new HashSet<ProductServiceCatalogFeature>();
        }

        public Guid Id { get; set; }
        public Guid ServiceCatalogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsCustom { get; set; }
        public string CustomText { get; set; }
        public bool IsActive { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ServiceCatalog ServiceCatalog { get; set; }
        public virtual ICollection<ProductServiceCatalogFeature> ProductServiceCatalogFeature { get; set; }
    }
}
