﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class ContactInvitationTelemetry 
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
        public DateTime OpenedOn { get; set; }
        public int MediaType { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual CampaignInvitation CampaignInvitation { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
