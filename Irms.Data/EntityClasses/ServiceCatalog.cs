﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class ServiceCatalog
    {
        public ServiceCatalog()
        {
            ProductServiceCatalogFeature = new HashSet<ProductServiceCatalogFeature>();
            ServiceCatalogFeature = new HashSet<ServiceCatalogFeature>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool? IsActive { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<ProductServiceCatalogFeature> ProductServiceCatalogFeature { get; set; }
        public virtual ICollection<ServiceCatalogFeature> ServiceCatalogFeature { get; set; }
    }
}
