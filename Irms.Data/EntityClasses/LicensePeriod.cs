﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class LicensePeriod
    {
        public LicensePeriod()
        {
            Product = new HashSet<Product>();
        }

        public byte Id { get; set; }
        public string Title { get; set; }
        public int? Days { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
