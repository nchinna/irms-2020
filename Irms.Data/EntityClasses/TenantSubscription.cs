﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class TenantSubscription
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ProductId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime ExpiryDateTime { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Product Product { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
