﻿using System;

namespace Irms.Data.EntityClasses
{
    public partial class ListAnalysisImportantFields
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid GuestListId { get; set; }
        public int FieldType { get; set; }
        public string Field { get; set; }
        public bool IsRequired { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }

        public virtual ContactList GuestList { get; set; }
    }
}