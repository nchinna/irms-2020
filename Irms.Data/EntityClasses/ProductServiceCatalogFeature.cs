﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class ProductServiceCatalogFeature
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid ServiceCatalogId { get; set; }
        public Guid ServiceCatalogFeatureId { get; set; }
        public int? ServiceCatalogFeatureCount { get; set; }

        public virtual Product Product { get; set; }
        public virtual ServiceCatalog ServiceCatalog { get; set; }
        public virtual ServiceCatalogFeature ServiceCatalogFeature { get; set; }
    }
}
