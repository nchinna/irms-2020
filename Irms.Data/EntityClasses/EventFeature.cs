﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class EventFeature
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public Guid ServiceCatalogFeatureId { get; set; }

        public virtual Event Event { get; set; }
    }
}
