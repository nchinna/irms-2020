﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    class ProductMapper : AutoMapper.Profile
    {
        public ProductMapper()
        {
            CreateMap<Domain.Entities.Product, EntityClasses.Product>()
                .ForMember(x => x.LicensePeriod, x => x.MapFrom(m => m.License));
        }

    }
}
