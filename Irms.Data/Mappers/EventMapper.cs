﻿using Irms.Application.Events.Commands;
using System.Linq;

namespace Irms.Data.Mappers
{
    public class EventMapper : AutoMapper.Profile
    {
        public EventMapper()
        {
            CreateMap<Domain.Entities.Event, EntityClasses.Event>()
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.Event, Domain.Entities.Event>()
                .ForMember(x => x.Location, y => y.MapFrom(z => z.EventLocation.FirstOrDefault()));

            //create tenant map
            CreateMap<CreateEventCmd, Domain.Entities.Event>();
            CreateMap<CreateEventCmd, Domain.Entities.EventLocation>()
                .ForMember(x => x.Name, x => x.MapFrom(m => m.LocationName));

            CreateMap<Domain.Entities.EventLocation, EntityClasses.EventLocation>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.EventId, x => x.Ignore())
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.EventLocation, Domain.Entities.EventLocation>()
                .ForMember(x => x.Name, y => y.MapFrom(z => z.Name));



            //update tenant
            CreateMap<UpdateEventCmd, Domain.Entities.Event>();
            CreateMap<UpdateEventCmd, Domain.Entities.EventLocation>()
                .ForMember(x => x.Name, x => x.MapFrom(m => m.LocationName));

            ////update features
            CreateMap<UpdateEventFeaturesCmd, Domain.Entities.Event>();
        }
    }
}
