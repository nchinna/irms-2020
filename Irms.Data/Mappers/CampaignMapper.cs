﻿using Irms.Application.Campaigns.Commands;

namespace Irms.Data.Mappers
{
    public class CampaignMapper : AutoMapper.Profile
    {
        public CampaignMapper()
        {
            CreateMap<Domain.Entities.Campaign, EntityClasses.EventCampaign>()
                .ForMember(x => x.CampaignStatus, x => x.MapFrom(m => (byte)m.CampaignStatus))
                .ForMember(x => x.CampaignType, x => x.MapFrom(m => (int)m.CampaignType))
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.EventCampaign, Domain.Entities.Campaign>()
                .ForMember(x => x.CampaignStatus, x => x.MapFrom(m => m.CampaignStatus))
                .ForMember(x => x.CampaignType, x => x.MapFrom(m => m.CampaignType));

            //create
            CreateMap<CreateCampaignCmd, Domain.Entities.Campaign>();
            CreateMap<CreateListAnalysisCampaignCmd, Domain.Entities.Campaign>()
                .ForMember(x => x.GuestListId, x => x.MapFrom(m => m.GuestListId));

            //update
            CreateMap<UpdateCampaignCmd, Domain.Entities.Campaign>();

            //preffered media mappers
            CreateMap<GuestPreferredMedia, EntityClasses.CampaignPrefferedMedia>()
                .ForMember(x => x.ContactId, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());
        }
    }
}
