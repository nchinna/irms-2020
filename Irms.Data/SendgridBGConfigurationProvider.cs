﻿using Irms.Application;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services.Sendgrid;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    public class SendgridBGConfigurationProvider : ISendgridBGConfigurationProvider
    {
        private readonly IrmsDataBackgroundJobContext _context;
        public SendgridBGConfigurationProvider(IrmsDataBackgroundJobContext context)
        {
            _context = context;
        }

        public async Task<SendgridConfiguration> GetConfiguration(Guid tenantId, CancellationToken token)
        {
            var tenant = await _context.Tenant.FirstOrDefaultAsync(x => x.Id == tenantId);
            var result = new SendgridConfiguration(tenant.SendGridApiKey, tenant.EmailFrom);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Sendgrid configuration is invalid!");
            }

            return result;
        }
    }
}
