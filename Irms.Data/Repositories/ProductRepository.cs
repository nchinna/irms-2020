﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Irms.Data.Repositories
{
    public class ProductRepository : IProductRepository<Product, Guid>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IrmsTenantDataContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        public ProductRepository(TenantBasicInfo tenant, IrmsTenantDataContext context, ICurrentUser user, IMapper mapper)
        {
            _tenant = tenant;
            _context = context;
            _mapper = mapper;
            _user = user;
        }
        
        /// <summary>
        /// this method create <see cref="Product"/> in databae with all needed references like <see cref="Currency"/>, <see cref="LicensePeriod"/> and services
        /// </summary>
        /// <param name="product"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(Product product, CancellationToken token)
        {
            var currency = await _context.Currency.FirstOrDefaultAsync(c => c.Id == product.CurrencyId, token);
            if(currency == null)
            {
                throw new IncorrectRequestException($"currency id {product.CurrencyId} not found");
            }
            if(_user != null)
            {
                product.SetIdOfUserCreatedBy(_user.Id);
            }
                        
            var productEntity = _mapper.Map<EntityClasses.Product>(product);

            if (!currency.Product.Any() || !currency.Product.Contains(productEntity))
            {
                currency.Product.Add(productEntity);
            }
            var license = await _context.LicensePeriod.FirstOrDefaultAsync(l => l.Days == product.License.Days, token);
            if (license == null)
            {
                var lastIndex = await _context.LicensePeriod.MaxAsync(l => l.Id, token);
                lastIndex++;
                license = new EntityClasses.LicensePeriod { Id = lastIndex, Days = product.License.Days, Title = product.License.Title };

                license.Product.Add(productEntity);
                productEntity.LicensePeriod.Id = license.Id;
            }
            else
            {
                productEntity.LicensePeriod = null;
            }
            productEntity.LicensePeriodId = license.Id;
            productEntity.LicensePeriodDays = license.Days.Value;

            product.ServicesCatalog.ForEach(sc => {

                sc.ServiceCatalogFeatures.ForEach(scf =>
                {
                    productEntity.ProductServiceCatalogFeature.Add(new EntityClasses.ProductServiceCatalogFeature
                    {
                        Id = Guid.NewGuid(),
                        ProductId = productEntity.Id,
                        ServiceCatalogId = sc.Id,
                        ServiceCatalogFeatureId = scf.Id,
                    });
                });                
            });

            //TODO TEST THIS
            await _context.Product.AddAsync(productEntity, token);
            await _context.ProductServiceCatalogFeature.AddRangeAsync(productEntity.ProductServiceCatalogFeature, token);

            await _context.SaveChangesAsync(token);

            return productEntity.Id;
        }
        /// <summary>
        /// this method mark product as deleted by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Delete(Guid id, CancellationToken token)
        {
            var subscriprion = await _context.TenantSubscription
                .Select(ts => new EntityClasses.TenantSubscription { ProductId = ts.ProductId})
                .FirstOrDefaultAsync(ps => ps.ProductId == id, token);
            if(subscriprion?.ProductId != null)
            {
                throw new IncorrectRequestException("Cannot delete product which using in subscription");
            }
            
            var product = await _context.Product.FirstOrDefaultAsync(p => p.Id == id, token);
            if(product == null)
            {
                throw new IncorrectRequestException("Incorrect product id");
            }
            product.IsDeleted = true;
            await _context.SaveChangesAsync(token);
        }
        /// <summary>
        /// this method mark list of products as deleted by ids
        /// </summary>
        /// <param name="ids">List of <see cref="Product"/> ids</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeleteList(IEnumerable<Guid> ids, CancellationToken token)
        {
            var productIdsWithSubscription = await _context.TenantSubscription
                .Where(ps => ids.Contains(ps.ProductId)).Select(ps => ps.ProductId).ToListAsync(token);

            var productIdsWithoutSubscription = ids.Where(i => !productIdsWithSubscription.Any(p => p == i));
            if(productIdsWithoutSubscription == null || !productIdsWithoutSubscription.Any())
            {
                throw new IncorrectRequestException("Has no products from selected list without subscriptions. Cannot delete product which using in subscription.");
            }

            var products = await _context.Product.Where(p => productIdsWithoutSubscription.Contains(p.Id)).ToListAsync(token);

            if (products == null || !products.Any())
            {
                throw new IncorrectRequestException("No products found by Ids");
            }

            products.ForEach(p => p.IsDeleted = true);
            await _context.SaveChangesAsync(token);
        }
        
        public Task<Product> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException("Method implementd in Irms.Data.Read project");
        }
        /// <summary>
        /// this method can modify <see cref="Product"/> and all his references
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Product entity, CancellationToken token)
        {
            var product = await _context.Product
                .Include(p => p.ProductServiceCatalogFeature)
                .FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (product == null)
            {
                throw new IncorrectRequestException("Product id is incorrect");
            }
            List<EntityClasses.ProductServiceCatalogFeature> productServiceCatalogFeatures = new List<EntityClasses.ProductServiceCatalogFeature>();

            product.IsDeleted = entity.IsDeleted;
            product.Title = entity.Title;
            product.Price = entity.Price;
            product.DiscountPercentage = (decimal)entity.DiscountPercentage;
            product.CurrencyId = entity.CurrencyId;
            product.LicensePeriodId = entity.License.Id;

            entity.ServicesCatalog.ForEach(sc => {

                sc.ServiceCatalogFeatures.ForEach(scf =>
                {
                    if (scf.IsFeatureEnabled)
                    {
                        productServiceCatalogFeatures.Add(new EntityClasses.ProductServiceCatalogFeature
                        {
                            Id = Guid.NewGuid(),
                            ProductId = entity.Id,
                            ServiceCatalogId = sc.Id,
                            ServiceCatalogFeatureId = scf.Id,
                        });
                    }                    
                });
            });

            var (add, del, upd) = product.ProductServiceCatalogFeature.Compare(productServiceCatalogFeatures, (o, n) => { return o.ProductId == n.ProductId && o.ServiceCatalogId == n.ServiceCatalogId && o.ServiceCatalogFeatureId == n.ServiceCatalogFeatureId; }) ;

            //add
            await _context.ProductServiceCatalogFeature.AddRangeAsync(
                add.Select(a => 
                new EntityClasses.ProductServiceCatalogFeature 
                {
                    Id = Guid.NewGuid(),
                    ProductId = a.ProductId,
                    ServiceCatalogId = a.ServiceCatalogId,
                    ServiceCatalogFeatureId = a.ServiceCatalogFeatureId,
                }), token);

            //delete
            var delIds = del.Select(x => x.Id).ToList();
            await _context.ProductServiceCatalogFeature
                .Where(x => delIds.Contains(x.Id))
                .DeleteAsync(token);

            //update
            foreach (var (o, n) in upd)
            {
                o.ProductId = n.ProductId;
                o.ServiceCatalogId = n.ServiceCatalogId;
                o.ServiceCatalogFeatureId = n.ServiceCatalogFeatureId;
            }
            
            await _context.SaveChangesAsync(token);
        }
    }
}
