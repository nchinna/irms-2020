﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories.Base;
using System.Linq;

namespace Irms.Data.Repositories
{
    public class SubscriptionRepository : ISubscriptionRepository<Subscription, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;

        public SubscriptionRepository(IrmsDataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// get user info by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Subscription> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// create subscription
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(Subscription entity, CancellationToken token)
        {
            var product = await _dataContext.Product
                .FirstOrDefaultAsync(x => x.Id == entity.ProductId, token);

            if (product == null)
            {
                throw new IncorrectRequestException("Can't find product by id.");
            }

            var data = _mapper.Map<EntityClasses.TenantSubscription>(entity);
            data.ExpiryDateTime = DateTime.UtcNow.AddDays(product.LicensePeriodDays);
            data.CreatedOn = DateTime.UtcNow;
            data.CreatedById = Guid.Parse("3AE6D177-1897-4C2C-97F9-FB0336E642A4");//TODO: set it to current user

            await _dataContext.TenantSubscription.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task Update(Subscription entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

    }
}
