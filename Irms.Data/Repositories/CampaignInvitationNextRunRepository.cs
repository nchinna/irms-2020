﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class CampaignInvitationNextRunRepository : ICampaignInvitationNextRunRepository<CampaignInvitationNextRun, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public CampaignInvitationNextRunRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        public async Task<Guid> Create(CampaignInvitationNextRun entity, CancellationToken token)
        {
            entity.Create();
            var data = _mapper.Map<EntityClasses.CampaignInvitationNextRun>(entity);
            _dataContext.CampaignInvitationNextRun
                .Add(data);

            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        public async Task Delete(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationNextRun
                   .FirstOrDefaultAsync(x => x.Id == id, token);

            _dataContext.CampaignInvitationNextRun
                .Remove(data);

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<CampaignInvitationNextRun> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationNextRun
                   .FirstOrDefaultAsync(x => x.Id == id, token);

            var result = _mapper.Map<EntityClasses.CampaignInvitationNextRun, Domain.Entities.CampaignInvitationNextRun>(data);
            return result;
        }

        public async Task Update(CampaignInvitationNextRun entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationNextRun
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Campaign invitation next run id is incorrect");
            }

            data.StartDate = entity.StartDate;
            data.Active = entity.Active;
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task CreateRange(IEnumerable<CampaignInvitationNextRun> entites, CancellationToken token)
        {
            var entities = _mapper.Map<IEnumerable<EntityClasses.CampaignInvitationNextRun>>(entites);
            _dataContext.AddRange(entities);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateRange(IEnumerable<CampaignInvitationNextRun> entities, CancellationToken token)
        {
            var entitiesIds = entities.Select(x => x.Id)
                .ToList();

            var data = await _dataContext.CampaignInvitationNextRun
                .Where(x => entitiesIds.Contains(x.Id))
                .ToListAsync(token);

            data.ForEach(x =>
            {
                var selectedItem = entities.FirstOrDefault(i => i.Id == x.Id);
                x.Status = (int)selectedItem.Status;
                x.Active = selectedItem.Active;
            });

            await _dataContext.SaveChangesAsync(token);
        }
    }
}
