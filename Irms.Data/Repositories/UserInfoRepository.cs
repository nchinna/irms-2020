﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories.Base;

namespace Irms.Data.Repositories
{
    public class UserInfoRepository : IUserInfoRepository<UserInfo, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;

        public UserInfoRepository(IrmsDataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// get user info by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<UserInfo> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.UserInfo.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("User info Id is incorrect");
            }
            var result = _mapper.Map<EntityClasses.UserInfo, UserInfo>(data);
            return result;
        }

        /// <summary>
        /// get tenant admin by tenant id
        /// </summary>
        /// <param name="id">tenant id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<UserInfo> GetTenantAdmin(Guid id, CancellationToken token)
        {
            var data = await _dataContext.UserInfo.FirstOrDefaultAsync(x => x.TenantNavigation.Id == id
            && x.RoleId == (int)RoleType.TenantAdmin, token);

            if (data == null)
            {
                throw new IncorrectRequestException("User info Id is incorrect");
            }

            var result = _mapper.Map<EntityClasses.UserInfo, UserInfo>(data);
            return result;
        }
        /// <summary>
        /// create user info 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Create(UserInfo entity, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.UserInfo>(entity);
            data.CreatedOn = DateTime.UtcNow;
            data.TenantId = entity.TenantId;

            await _dataContext.UserInfo.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(UserInfo entity, CancellationToken token)
        {
            var data = await _dataContext.UserInfo.FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Tenant Id is incorrect");
            }

            _mapper.Map(entity, data);
            data.UpdatedOn = DateTime.UtcNow;
            data.UpdatedBy = entity.UserId;//Guid.Parse("3AE6D177-1897-4C2C-97F9-FB0336E642A4");//TODO: set it to current user
            await _dataContext.SaveChangesAsync(token);
        }

    }
}
