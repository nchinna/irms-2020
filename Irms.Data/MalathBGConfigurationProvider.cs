﻿using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    /// <summary>
    /// Configuration class for Twilio API
    /// </summary>
    public class MalathBGConfigurationProvider : IMalathBGConfigurationProvider
    {
        private readonly ITenantRepository<Tenant, Guid> _tenantReposiotry;
        public MalathBGConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository)
        {
            _tenantReposiotry = tenantRepository;
        }

        public async Task<MalathConfiguration> GetConfiguration(Guid tenantId, CancellationToken token)
        {
            var tenant = await _tenantReposiotry.GetById(tenantId, token);
            var result = new MalathConfiguration(
                username: tenant.MalathUsername,
                password: tenant.MalathPassword,
                senderName: tenant.MalathSender);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Malath configuration is invalid!");
            }

            return result;
        }
    }
}
