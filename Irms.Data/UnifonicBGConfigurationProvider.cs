﻿//using Irms.Application.Abstract.Repositories;
//using Irms.Domain;
//using Irms.Domain.Entities.Tenant;
//using Irms.Infrastructure.Services.Unifonic;
//using System;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Data
//{
//    public class UnifonicBGConfigurationProvider : IUnifonicBGConfigurationProvider
//    {
//        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;

//        public UnifonicBGConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository)
//        {
//            _tenantRepository = tenantRepository;
//        }

//        public async Task<UnifonicConfiguration> GetConfiguration(Guid tenantId, CancellationToken token)
//        {
//            var tenant = await _tenantRepository.GetById(tenantId, token);
//            var result = new UnifonicConfiguration(tenant.UnifonicSid, tenant.UnifonicSender);

//            if (!result.IsValid)
//            {
//                throw new IncorrectRequestException("Unifonic configuration is invalid!");
//            }

//            return result;
//        }
//    }
//}
