﻿using Irms.Application.Abstract;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace Irms.Data
{
    internal class BasicTransaction : ITransaction
    {
        private readonly IDbContextTransaction _transaction;

        public BasicTransaction(IDbContextTransaction transaction)
        {
            _transaction = transaction;
        }

        public Guid Id => _transaction.TransactionId;

        public void Commit() => _transaction.Commit();
        public void Rollback() => _transaction.Rollback();
        public void Dispose() => _transaction.Dispose();
    }
}