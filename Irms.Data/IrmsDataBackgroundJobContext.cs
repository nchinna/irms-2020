﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data
{
    public class IrmsDataBackgroundJobContext : IrmsDataContext
    {
        public IrmsDataBackgroundJobContext()
        {

        }

        public IrmsDataBackgroundJobContext(DbContextOptions<IrmsDataContext> options)
            : base(options)
        {
        }
    }
}
