FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim
RUN apt-get update && apt-get install -y libgdiplus
WORKDIR /app
COPY release ./
ENTRYPOINT ["dotnet", "Irms.WebApi.dll"]