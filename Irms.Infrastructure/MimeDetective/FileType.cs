﻿using System;

namespace Irms.Infrastructure.MimeDetective
{
    public class FileType : IEquatable<FileType>
    {
        public FileType(byte?[] header, string extension, string mime)
        {
            Header = header;
            Extension = extension;
            Mime = mime;
            HeaderOffset = 0;
        }


        public FileType(byte?[] header, int offset, string extension, string mime)
        {
            Header = null;
            Header = header;
            HeaderOffset = offset;
            Extension = extension;
            Mime = mime;
        }

        public byte?[] Header { get; }
        public int HeaderOffset { get; }
        public string Extension { get;  }
        public string Mime { get; }

        public override bool Equals(object other)
        {
            if (!(other is FileType))
            {
                return false;
            }

            return Equals((FileType)other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Extension != null ? Extension.GetHashCode() : 0) * 397) ^ (Mime != null ? Mime.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return Extension;
        }

        public bool Equals(FileType other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(Extension, other.Extension) && string.Equals(Mime, other.Mime);
        }
    }
}
