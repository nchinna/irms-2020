﻿using Irms.Application.Abstract;
using Nager.PublicSuffix;
using System;

namespace Irms.Infrastructure.Services
{
    public class DomainParser : IDomainParser
    {
        private readonly Microsoft.Extensions.Configuration.IConfiguration _config;
        Nager.PublicSuffix.DomainParser domainParser;

        public DomainParser(Microsoft.Extensions.Configuration.IConfiguration config)
        {
            _config = config;
            domainParser = new Nager.PublicSuffix.DomainParser(new WebTldRuleProvider());
        }

        /// <summary>
        /// extract sub domain from url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetSubDomain(string url)
        {
            var domainName = domainParser.Get((new Uri(url)).Authority);
            return domainName.SubDomain;
        }

        /// <summary>
        /// get domain with scheme
        /// </summary>
        /// <returns></returns>
        public (string scheme, string host) GetDomain()
        {
            var domain = new Uri(_config["JwtAuthority:Website"]);
#if DEBUG
            domain = new Uri("http://apiqa.takamulstg.com");
#endif
            string authority = domainParser.Get(domain.Authority).RegistrableDomain;
            return (domain.Scheme, authority);
        }

        /// <summary>
        /// get domain with scheme
        /// </summary>
        /// <returns></returns>
        public string GetHost(string url)
        {
            var domain = new Uri(_config["JwtAuthority:Website"]);
            return domainParser.Get(domain.Authority).RegistrableDomain;
        }
    }
}
