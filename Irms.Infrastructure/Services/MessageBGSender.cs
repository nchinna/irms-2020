﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Infrastructure.Services.Sendgrid;
using Irms.Infrastructure.Services.Twilio;
using PhoneNumbers;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class MessageBGSender : ISmsBGSender, IEmailBGSender
    {
        private static readonly string SectionTitleSeparator = new string('-', 20) + Environment.NewLine;
        private static readonly int[] GccCountryCodes = new int[] { 966, 971, 965, 973, 974, 968 };

        private readonly ISendgridEmailBGSender _sendgrid;
        private readonly IMalathSmsBGSender _malath;
        private readonly ITwilioSmsBGSender _twilio;


        public MessageBGSender(ISendgridEmailBGSender sendgrid,
            IMalathSmsBGSender malath,
            ITwilioSmsBGSender twilio)
        {
            _sendgrid = sendgrid;
            _malath = malath;
            _twilio = twilio;
        }

        #region email
        public Task<bool> SendEmail(Guid tenantId, EmailMessage message, CancellationToken token)
        {
            var result = _sendgrid.SendEmail(tenantId, message, token);
            return result;
        }
        #endregion

        #region sms
        public Task<bool> SendSms(Guid tenantId, SmsMessage message, CancellationToken token)
        {
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "SMS");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            foreach (var recipient in message.Recipients)
            {
                var body = message.MessageTemplate ?? string.Empty;

                foreach (var variable in recipient.TemplateVariables)
                {
                    body = body.Replace(variable.Name, variable.Value);
                }

                File.WriteAllText(Path.Combine(dir, $"{recipient.Phone}-{DateTime.Now.Ticks}.html"), body);
            }

            return Task.FromResult(true);
        }

        public async Task<bool> SendWebhookSms(Guid tenantId, SmsMessage message, CancellationToken token)
        {
            var isInternationalPhoneNumber = message.Recipients.Any(x => IsInternationalPhone(x.Phone));
            if (isInternationalPhoneNumber)
            {
                var result = await _twilio.SendWebhookSms(tenantId, message, token);
                return result;
            }
            else
            {
                var result = await _malath.SendWebhookSms(tenantId, message, token);
                return result;
            }
        }

        private bool IsInternationalPhone(string unvalidatedPhone)
        {
            var util = PhoneNumberUtil.GetInstance();
            try
            {

                var phone = util.Parse(unvalidatedPhone, null);
                bool isValid = util.IsValidNumber(phone);

                //check if code is on Gcc Country Codes array (not international)
                if (isValid && GccCountryCodes.Contains(phone.CountryCode))
                {
                    return false;
                }

                return true;
            }
            catch (NumberParseException)
            {
                return false;
            }
        }

        #endregion
    }
}
