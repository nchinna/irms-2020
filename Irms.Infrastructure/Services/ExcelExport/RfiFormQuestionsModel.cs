﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Infrastructure.Services.ExcelExport
{
    public class RfiFormQuestionsModel
    {
        public Guid Id { get; set; }
        public string Question { get; set; }
        public int SortOrder { get; set; }
    }
}
