﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services.Sendgrid
{
    public interface ISendgridConfigurationProvider
    {
        Task<SendgridConfiguration> GetConfiguration(CancellationToken token);
    }
}
