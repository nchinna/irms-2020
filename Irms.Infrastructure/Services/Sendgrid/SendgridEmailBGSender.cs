﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Irms.Application.Abstract.Services.Notifications;
using SendGrid;
using SendGrid.Helpers.Mail;
using Irms.Application.Events.Commands;
using Irms.Domain.Entities;
using Newtonsoft.Json;
using MediatR;

namespace Irms.Infrastructure.Services.Sendgrid
{
    /// <summary>
    /// Implementation of Email sender by SendGrid API
    /// </summary>
    public class SendgridEmailBGSender : ISendgridEmailBGSender
    {
        private readonly ISendgridBGConfigurationProvider _configurationProvider;
        private readonly ILogger<SendgridEmailSender> _logger;
        private readonly IMediator _mediator;

        public SendgridEmailBGSender(ISendgridBGConfigurationProvider configurationProvider,
            ILogger<SendgridEmailSender> logger,
            IMediator mediator)
        {
            _configurationProvider = configurationProvider;
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<bool> SendEmail(Guid tenantId, EmailMessage message, CancellationToken token)
        {
            var cfg = await _configurationProvider.GetConfiguration(tenantId, token);
            if (!cfg.IsValid)
            {
                throw new Exception("Sendgrid email is not configured");
            }

            return await SendEmailInternal(tenantId, message, cfg, token);
        }

        public async Task<bool> SendEmailInternal(Guid tenantId, EmailMessage message, SendgridConfiguration cfg, CancellationToken token)
        {
            var client = new SendGridClient(cfg.ApiKey);

            var msg = ConvertMessage(message, cfg.EmailFrom);

            var response = await client.SendEmailAsync(msg, token);
            if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK)
            {
                if (response.Headers.TryGetValues("X-Message-Id", out var values))
                {
                    message.MessageId = values.First();
                    message.ProviderType = ProviderType.Sendgrid;
                }

                var cmd = new ProviderLogsCmd
                {
                    ProviderType = ProviderType.Sendgrid,
                    TenantId = tenantId,
                    Request = JsonConvert.SerializeObject(msg),
                    RequestDate = DateTime.UtcNow,
                    Response = JsonConvert.SerializeObject(response),
                    ResponseDate = DateTime.UtcNow,
                    CampaignInvitationId = message.CampaignInvitationId
                };

                await _mediator.Send(cmd, token);
                foreach (var recipient in message.Recipients)
                {
                    _logger.LogInformation("Email \"{0}\" to address \"{1}\" has been sent", recipient.Subject, recipient.EmailAddress);
                }
                return true;
            }
            else
            {
                _logger.LogError("Can not send email: " + await response.Body.ReadAsStringAsync() + " " + response.StatusCode);
                return false;
            }
        }

        internal SendGridMessage ConvertMessage(EmailMessage src, string emailFrom)
        {
            return new SendGridMessage
            {
                From = new EmailAddress(emailFrom),
                HtmlContent = src.HtmlTemplate,
                Personalizations = src.Recipients.Select(ConvertRecipient).ToList()
            };
        }

        internal Personalization ConvertRecipient(EmailMessage.Recipient src)
        {
            return new Personalization
            {
                Subject = src.Subject,
                Tos = new List<EmailAddress> { new EmailAddress(src.EmailAddress) },
                Substitutions = src.TemplateVariables.ToDictionary(k => k.Name, v => v.Value)
            };
        }
    }
}
