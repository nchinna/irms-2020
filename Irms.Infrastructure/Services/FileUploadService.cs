﻿using Irms.Application.Abstract;
using System.IO;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class FileUploadService : IFileUploadService
    {
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;

        public FileUploadService(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private const string CONTAINER = "files-container";

        /// <summary>
        /// upload file to azure
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public async Task UploadFile(string fileName, Stream content)
        {
            var client = new AzureBlobClient(_configuration["AzureStorage:DefaultConnection"], true);
            await client.UploadFile(CONTAINER, fileName, content);
        }

        /// <summary>
        /// load file method definition
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<byte[]> LoadFile(string fileName)
        {
            var client = new AzureBlobClient(_configuration["AzureStorage:DefaultConnection"]);
            return await client.DownloadFile(CONTAINER, fileName);
        }

        /// <summary>
        /// Completes existing file url
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public string LoadFileUrl(string filename)
        {
            return _configuration["AzureStorage:BaseUrl"] + filename;
        }

        /// <summary>
        /// delete file 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task DeleteFile(string fileName)
        {
            var client = new AzureBlobClient(_configuration["AzureStorage:DefaultConnection"]);
            await client.DeleteFile(CONTAINER, fileName);
        }
    }
}
