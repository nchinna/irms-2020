﻿namespace Irms.Infrastructure.Services.Twilio
{
    public sealed class TwilioConfiguration
    {
        public TwilioConfiguration(string accountSid, string authToken, string notificationServiceId)
        {
            AccountSid = accountSid;
            AuthToken = authToken;
            NotificationServiceId = notificationServiceId;
        }

        public string AccountSid { get; }
        public string AuthToken { get; }
        public string NotificationServiceId { get; }
        public string TwilioUrl => "https://notify.twilio.com";
        public string NotificationServiceUrl => "v1/Services/{0}/Notifications";

        public bool IsValid =>
            !string.IsNullOrWhiteSpace(AccountSid)
            && !string.IsNullOrWhiteSpace(AuthToken)
            && !string.IsNullOrWhiteSpace(NotificationServiceId);
    }
}
