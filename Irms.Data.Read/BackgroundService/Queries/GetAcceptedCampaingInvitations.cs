﻿using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.BackgroundService.Queries
{
    public class GetAcceptedCampaingInvitations : IRequest<IEnumerable<ReadModels.CampaingInvitationListItem>>
    {
    }
}
