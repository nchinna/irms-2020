﻿using Irms.Data.Abstract;
using Irms.Data.Read.BackgroundService.Queries;
using Irms.Data.Read.BackgroundService.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.BackgroundService.QueryHandlers
{
    public class GetCampaingInvitationsHandler :
        IRequestHandler<GetRsvpPendingCampaingInvitations, IEnumerable<CampaingInvitationListItem>>,
        IRequestHandler<GetAcceptedCampaingInvitations, IEnumerable<CampaingInvitationListItem>>,
        IRequestHandler<GetRejectedCampaingInvitations, IEnumerable<CampaingInvitationListItem>>
    {
        private const string RsvpPending = @"
                    SELECT ci.Id,
                           ci.IsInstant,
                           ci.StartDate
                    FROM CampaignInvitation ci
                    WHERE ci.Active = 1
                    AND (ci.IsInstant = 1 OR ci.StartDate <= @now)
                    AND ci.Status = @status
                    AND ci.InvitationTypeId = @type
                    AND (EXISTS
                           (SELECT CampaignInvitationId
                            FROM CampaignEmailTemplate
                            WHERE CampaignInvitationId=ci.Id)
                         OR EXISTS
                           (SELECT CampaignInvitationId
                            FROM CampaignSMSTemplate
                            WHERE CampaignInvitationId=ci.Id))";

        private const string Accepted = @"
                SELECT distinct(ci.Id)
                FROM CampaignInvitation ci
                INNER JOIN EventCampaign ec ON ci.EventCampaignId = ec.Id
                INNER JOIN ContactListToContacts clc ON ec.GuestListId = clc.ContactListId OUTER APPLY
                  (SELECT TOP 1 *
                   FROM CampaignInvitationResponse
                   WHERE ci.Id = CampaignInvitationId
                     AND clc.ContactId = ContactId
                   ORDER BY ResponseDate DESC) cir
                WHERE ci.Active = 1
                  AND (ci.IsInstant = 1
                       OR ci.StartDate <= @now)
                  AND ci.Status = 0
                  AND ci.InvitationTypeId = 2 /* inv accepted */
                  AND cir.Answer = 1 /* accepted */
                  AND (EXISTS
                         (SELECT CampaignInvitationId
                          FROM CampaignEmailTemplate
                          WHERE CampaignInvitationId=ci.Id)
                       OR EXISTS
                         (SELECT CampaignInvitationId
                          FROM CampaignSMSTemplate
                          WHERE CampaignInvitationId=ci.Id))";

        private const string Rejected = @"
                SELECT distinct(ci.Id)
                FROM CampaignInvitation ci
                INNER JOIN EventCampaign ec ON ci.EventCampaignId = ec.Id
                INNER JOIN ContactListToContacts clc ON ec.GuestListId = clc.ContactListId OUTER APPLY
                  (SELECT TOP 1 *
                   FROM CampaignInvitationResponse
                   WHERE ci.Id = CampaignInvitationId
                     AND clc.ContactId = ContactId
                   ORDER BY ResponseDate DESC) cir
                WHERE ci.Active = 1
                  AND (ci.IsInstant = 1
                       OR ci.StartDate <= @now)
                  AND ci.Status = 0
                  AND ci.InvitationTypeId = 3 /* inv rejected */
                  AND cir.Answer = 0 /* rejected */
                  AND (EXISTS
                         (SELECT CampaignInvitationId
                          FROM CampaignEmailTemplate
                          WHERE CampaignInvitationId=ci.Id)
                       OR EXISTS
                         (SELECT CampaignInvitationId
                          FROM CampaignSMSTemplate
                          WHERE CampaignInvitationId=ci.Id))";

        private readonly IConnectionString _connectionString;

        public GetCampaingInvitationsHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// get product list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CampaingInvitationListItem>> Handle(GetRsvpPendingCampaingInvitations request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                now = DateTime.UtcNow,
                type = (int)request.InvitationType,
                status = (int)InvitationStatus.Draft
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CampaingInvitationListItem>(RsvpPending, parameters);
                return list;
            }
        }

        public async Task<IEnumerable<CampaingInvitationListItem>> Handle(GetAcceptedCampaingInvitations request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                now = DateTime.UtcNow,
                status = (int)InvitationStatus.Draft
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CampaingInvitationListItem>(Accepted, parameters);
                return list;
            }
        }

        public async Task<IEnumerable<CampaingInvitationListItem>> Handle(GetRejectedCampaingInvitations request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                now = DateTime.UtcNow,
                status = (int)InvitationStatus.Draft
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CampaingInvitationListItem>(Rejected, parameters);
                return list;
            }
        }
    }
}