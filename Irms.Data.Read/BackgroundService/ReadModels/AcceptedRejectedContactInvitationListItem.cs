﻿using System;

namespace Irms.Data.Read.BackgroundService.ReadModels
{
    public class AcceptedRejectedContactInvitationListItem
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string PreferredName { get; set; }
        public string Email { get; set; }
        public bool EmailPreferred { get; set; }
        public string Phone { get; set; }
        public bool SmsPreferred { get; set; }
        public bool WhatsappPreferred { get; set; }
        public string FullName { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string SenderName { get; set; }
        public string EmailAcceptedLink { get; set; }
        public string EmailRejectedLink { get; set; }
        public string SmsAcceptedLink { get; set; }
        public string SmsRejectedLink { get; set; }
        public string WhatsappAcceptedLink { get; set; }
        public string WhatsappRejectedLink { get; set; }

    }
}