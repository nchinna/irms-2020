﻿using System;

namespace Irms.Data.Read.BackgroundService.ReadModels
{
    public class CampaingInvitationListItem
    {
        public Guid Id { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
    }
}
