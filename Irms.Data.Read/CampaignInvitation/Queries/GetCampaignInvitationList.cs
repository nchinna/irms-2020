﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignInvitationList : IRequest<IEnumerable<CampaignInvitationListItem>>
    {
        public GetCampaignInvitationList(Guid campaignId, InvitationType invitationType)
        {
            CampaignId = campaignId;
            InvitationType = invitationType;
        }

        public Guid CampaignId { get; }
        public InvitationType InvitationType { get; }
    }
}
