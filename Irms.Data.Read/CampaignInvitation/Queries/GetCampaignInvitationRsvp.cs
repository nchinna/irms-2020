﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignInvitationRsvp : IRequest<CampaignInvitationRsvp>
    {

        public GetCampaignInvitationRsvp(Guid campaignId)
        {
            CampaignId = campaignId;
        }

        public Guid CampaignId { get; }
    }
}
