﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetWhatsappApprovedTemplateQuery : IRequest<IEnumerable<WhatsappApprovedTemplate>>
    {
        public GetWhatsappApprovedTemplateQuery() { }

        public GetWhatsappApprovedTemplateQuery(Guid invitationId) => InvitationId = invitationId;

        public Guid? InvitationId { get; set; }
    }
}
