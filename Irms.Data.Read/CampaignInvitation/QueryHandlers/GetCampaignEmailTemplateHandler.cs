﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignEmailTemplateHandler : IRequestHandler<GetCampaignEmailTemplate, ReadModels.CampaignEmailTemplate>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public GetCampaignEmailTemplateHandler(IrmsDataContext context,
            IMapper mapper,
            IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        /// <summary>
        /// get campaign email template
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ReadModels.CampaignEmailTemplate> Handle(GetCampaignEmailTemplate request, CancellationToken token)
        {
            var tem = await _context.CampaignEmailTemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == request.CampaignInvitationId, token);

            if (tem == null)
            {
                return null;
            }

            var template = _mapper.Map<ReadModels.CampaignEmailTemplate>(tem);
            template.ListName = await _context.CampaignInvitation
                .Where(x => x.Id == request.CampaignInvitationId)
                .Select(x => x.EventCampaign.GuestList.Name)
                .FirstOrDefaultAsync(token);

            if (template != null && !string.IsNullOrEmpty(template.BackgroundImagePath))
            {
                template.BackgroundImagePath = $"{_config["AzureStorage:BaseUrl"]}{template.BackgroundImagePath}";
            }

            return template;
        }
    }
}
