﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using Irms.Data.Read.Registration;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    [HasSqlQuery(nameof(PageQuery))]
    public class GetCampaignInvitationListHandler : IRequestHandler<GetCampaignInvitationList, IEnumerable<CampaignInvitationListItem>>
    {
        private const string PageQuery = @"
                SELECT Id, 
						Title,
						IsInstant,
						SortOrder,
						Interval,
						IntervalType
                FROM CampaignInvitation
                WHERE EventCampaignId = @campaignId
                    AND InvitationTypeId = @type
                ORDER BY SortOrder";

        private readonly IConnectionString _connectionString;

        public GetCampaignInvitationListHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// this method will return list of reminders
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CampaignInvitationListItem>> Handle(GetCampaignInvitationList request, CancellationToken cancellationToken)
        {

            var parameters = new
            {
                campaignId = request.CampaignId,
                type = (int)request.InvitationType
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var list = await connection.QueryAndLog<CampaignInvitationListItem>(PageQuery, parameters);
                return list;
            }
        }
    }
}
