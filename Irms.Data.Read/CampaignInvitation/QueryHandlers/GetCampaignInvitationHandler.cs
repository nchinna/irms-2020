﻿using Autofac.Core.Lifetime;
using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.BackgroundService.Queries;
using Irms.Application.Campaigns.Commands;
using Irms.Data.Read.BackgroundService.Queries;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignInvitationHandler : IRequestHandler<GetCampaignInvitation, ReadModels.CampaignInvitation>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        private readonly IFileUploadService _fileUploadService;
        private readonly IConfiguration _config;
        private readonly IMediator _mediator;
        public GetCampaignInvitationHandler(IMediator mediator,
            IrmsDataContext context,
            IMapper mapper,
            IFileUploadService fileUploadService,
            IConfiguration config)
        {
            _mediator = mediator;
            _context = context;
            _mapper = mapper;
            _fileUploadService = fileUploadService;
            _config = config;
        }

        /// <summary>
        /// get campaign invitation pending info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ReadModels.CampaignInvitation> Handle(GetCampaignInvitation request, CancellationToken token)
        {
            EntityClasses.CampaignInvitation inv = null;
            if (request.InvitationType == InvitationType.Rsvp)
            {
                inv = await _context.CampaignInvitation
                    .Include(x => x.EventCampaign)
                    .Include(x => x.CampaignEmailTemplate)
                    .Include(x => x.CampaignSmsTemplate)
                    .Include(x => x.CampaignWhatsappTemplate)
                    .Include(x => x.RfiForm)
                    .FirstOrDefaultAsync(x => x.EventCampaignId == request.Id
                    && x.InvitationTypeId == (int)InvitationType.Rsvp, token);

                if (inv == null)
                {
                    var id = await _mediator.Send(new CreateOrUpdateCampaignInvitationCmd
                    {
                        Id = Guid.NewGuid(),
                        CampaignId = request.Id,
                        IsInstant = true
                    }, token);

                    inv = await _context.CampaignInvitation
                        .Include(x => x.EventCampaign)
                        .Include(x => x.CampaignEmailTemplate)
                        .Include(x => x.CampaignSmsTemplate)
                        .Include(x => x.CampaignWhatsappTemplate)
                        .Include(x => x.RfiForm)
                        .FirstOrDefaultAsync(x => x.Id == id
                        && x.InvitationTypeId == (int)InvitationType.Rsvp, token);
                }
            }
            else
            {
                inv = await _context.CampaignInvitation
                    .Include(x => x.EventCampaign)
                    .Include(x => x.CampaignEmailTemplate)
                    .Include(x => x.CampaignSmsTemplate)
                    .Include(x => x.CampaignWhatsappTemplate)
                    .Include(x => x.RfiForm)
                    .FirstOrDefaultAsync(x => x.Id == request.Id
                    && x.InvitationTypeId == (int)request.InvitationType, token);
            }

            var invitation = _mapper.Map<ReadModels.CampaignInvitation>(inv);

            var et = inv.CampaignEmailTemplate.FirstOrDefault();
            var st = inv.CampaignSmsTemplate.FirstOrDefault();
            var wt = inv.CampaignWhatsappTemplate.FirstOrDefault();

            invitation.IntervalType = (IntervalType?)inv.IntervalType;
            invitation.EmailSubject = et?.Subject;
            invitation.EmailSender = et?.SenderEmail;
            invitation.SmsSender = st?.SenderName;

            //whatsapp template
            invitation.WhatsappSender = wt?.SenderName;

            if (inv.InvitationTypeId == (int)InvitationType.Accepted || inv.InvitationTypeId == (int)InvitationType.Rejected)
            {
                var rfi = inv.RfiForm.FirstOrDefault();

                invitation.FormCreated = rfi != null;

                if (rfi != null && rfi.ThemeBackgroundImagePath.IsNotNullOrEmpty())
                {
                    invitation.FormBackgroundPath = $"{_config["AzureStorage:BaseUrl"]}{rfi.ThemeBackgroundImagePath}";
                }
            }

            if (et != null)
            {
                if (!string.IsNullOrEmpty(et.BackgroundImagePath))
                {
                    invitation.EmailImage = _fileUploadService.LoadFileUrl(et.BackgroundImagePath);
                }
                else
                {
                    invitation.EmailImage = CutImageFromBody(et.Body);
                }
            }

            if (st != null)
            {
                if (!string.IsNullOrEmpty(st.BackgroundImagePath))
                {
                    invitation.SmsImage = _fileUploadService.LoadFileUrl(st.BackgroundImagePath);
                }
                else
                {
                    invitation.SmsImage = CutImageFromBody(st.WelcomeHtml);
                }
            }

            if (wt != null)
            {
                if (!string.IsNullOrEmpty(wt.BackgroundImagePath))
                {
                    invitation.WhatsappImage = _fileUploadService.LoadFileUrl(wt.BackgroundImagePath);
                }
                else
                {
                    invitation.WhatsappImage = CutImageFromBody(wt.WelcomeHtml);
                }
            }

            //active medias
            invitation.ActiveMedia = new ReadModels.ActiveMedia();

            invitation.ActiveMedia.Email = await _context.CampaignPrefferedMedia
                .AnyAsync(x => x.EventCampaignId == inv.EventCampaign.Id
                && x.Email
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == inv.EventCampaign.GuestListId),
                token);

            invitation.ActiveMedia.Sms = await _context.CampaignPrefferedMedia
                .AnyAsync(x => x.EventCampaignId == inv.EventCampaign.Id
                && x.Sms
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == inv.EventCampaign.GuestListId),
                token);

            invitation.ActiveMedia.Whatsapp = await _context.CampaignPrefferedMedia
                .AnyAsync(x => x.EventCampaignId == inv.EventCampaign.Id
                && x.WhatsApp
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == inv.EventCampaign.GuestListId),
                token);

            //webhook statistics 
            var logs = await _context.CampaignInvitationMessageLog
                .Where(x => x.CampaignInvitationId == inv.Id)
                .ToListAsync(token);

            var statuses = await _context.ProviderResponse
                .Include(x => x.CampaignInvitationMessageLog)
                .Where(x => x.CampaignInvitationMessageLog.CampaignInvitationId == inv.Id)
                .ToListAsync(token);

            var responses = await _context.CampaignInvitationResponse
                .Where(x => x.CampaignInvitationId == inv.Id)
                .ToListAsync(token);

            invitation.Statistics = new ReadModels.WebhookStatistics
            {
                Email = new ReadModels.EmailStatistics
                {
                    Triggered = logs.LongCount(x => x.InvitationTemplateTypeId == (int)InvitationTemplateType.Email
                    && x.MessageStatus == (int)InvitationMessageStatus.Triggered),

                    Processed = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Processed).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    // due to timehook issues:
                    // 4 - delivered
                    // 5 - 
                    // 6 -
                    // 7 - 
                    Delivered = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && (x.MessageStatus == (int)InvitationMessageStatus.Delivered)
                    ).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    //Delivered = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    //&& (x.MessageStatus == (int)InvitationMessageStatus.Delivered // 4
                    //||  x.MessageStatus == (int)InvitationMessageStatus.Processed // 5
                    //||  x.MessageStatus == (int)InvitationMessageStatus.Open      // 6
                    //||  x.MessageStatus == (int)InvitationMessageStatus.Click)    // 7
                    //).GroupBy(x => x.ContactId).Count(),

                    Clicks = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Click),

                    UniqueClicks = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Click).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    Opens = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Open),

                    UniqueOpens = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Open).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    Bounces = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Bounce).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    SpamReports = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.SpamReported).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    Accepted = responses.LongCount(x => x.Answer == (int)ResponseMediaType.ACCEPTED
                    && x.ResponseMediaType == (int)MediaType.Email),

                    Rejected = responses.LongCount(x => x.Answer == (int)ResponseMediaType.REJECTED
                    && x.ResponseMediaType == (int)MediaType.Email),

                    Blocked = statuses.Where(x => x.ProviderType == (int)ProviderType.Sendgrid
                    && x.MessageStatus == (int)InvitationMessageStatus.Blocked).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count()
                },
                Sms = new ReadModels.SmsStatistics
                {
                    Triggered = logs.LongCount(x => x.InvitationTemplateTypeId == (int)InvitationTemplateType.Sms
                    && x.MessageStatus == (int)InvitationMessageStatus.Triggered),

                    Sent = statuses.Where(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.Sms
                    && x.MessageStatus == (int)InvitationMessageStatus.Sent).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    Delivered = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.Sms
                    && x.MessageStatus == (int)InvitationMessageStatus.Delivered),

                    UnDelivered = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.Sms
                    && x.MessageStatus == (int)InvitationMessageStatus.Undelivered),

                    Accepted = responses.LongCount(x => x.Answer == (int)ResponseMediaType.ACCEPTED
                    && x.ResponseMediaType == (int)MediaType.Sms),

                    Rejected = responses.LongCount(x => x.Answer == (int)ResponseMediaType.REJECTED
                    && x.ResponseMediaType == (int)MediaType.Sms),

                    Failed = statuses.Where(x => x.ProviderType == (int)InvitationMessageStatus.Failed).Count()
                },
                Whatsapp = new ReadModels.WhatsappStatistics
                {
                    Initiated = logs.LongCount(x => x.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
                     && x.MessageStatus == (int)InvitationMessageStatus.Triggered),

                    Sent = statuses.Where(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
                    && x.MessageStatus == (int)InvitationMessageStatus.Sent).Select(x => new { x.ContactId, x.MessageStatus })
                    .Distinct().Count(),

                    Read = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
                    && x.MessageStatus == (int)InvitationMessageStatus.Read),

                    Delivered = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
                    && x.MessageStatus == (int)InvitationMessageStatus.Delivered),

                    Failed = statuses.LongCount(x => x.ProviderType == (int)ProviderType.Twilio
                    && x.CampaignInvitationMessageLog.InvitationTemplateTypeId == (int)InvitationTemplateType.WhatsApp
                    && x.MessageStatus == (int)InvitationMessageStatus.Failed),

                    Accepted = responses.LongCount(x => x.Answer == (int)ResponseMediaType.ACCEPTED
                    && x.ResponseMediaType == (int)MediaType.WhatsApp),

                    Rejected = responses.LongCount(x => x.Answer == (int)ResponseMediaType.REJECTED
                    && x.ResponseMediaType == (int)MediaType.WhatsApp),
                }
            };

            return invitation;
        }

        /// <summary>
        /// Cuts first image from email body, uses as a thumbnail, if needed
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private string CutImageFromBody(string body)
        {
            string pattern = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(body);
            if (matches.Count > 0)
            {
                var image = matches[0].Groups[1].Value;
                return image;
            }

            return default;
        }
    }
}
