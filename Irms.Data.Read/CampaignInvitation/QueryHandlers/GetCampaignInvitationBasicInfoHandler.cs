﻿using AutoMapper;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignInvitationBasicInfoHandler : IRequestHandler<GetCampaignInvitationBasicInfo, CampaignInvitationBasicInfo>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        public GetCampaignInvitationBasicInfoHandler(IrmsDataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// returns basic info about invitation
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignInvitationBasicInfo> Handle(GetCampaignInvitationBasicInfo request, CancellationToken token)
        {
            var inv = await _context.CampaignInvitation
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            var result = _mapper.Map<CampaignInvitationBasicInfo>(inv);

            return result;
        }
    }
}
