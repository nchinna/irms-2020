﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class AssociatedListReadModel
    {
        public Guid Id { get; set; }
        public string ListName { get; set; }
    }
}
