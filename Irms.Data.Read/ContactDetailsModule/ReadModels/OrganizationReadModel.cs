﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class OrganizationReadModel
    {
        public Guid Id { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}
