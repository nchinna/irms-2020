﻿using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.UsersInfo.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInfoEmailsByIdsHandler : IRequestHandler<GetUserInfoEmailsByIds, IEnumerable<string>>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _currentUser;

        public GetUserInfoEmailsByIdsHandler(IrmsTenantDataContext context, ICurrentUser currentUser, TenantBasicInfo tenant)
        {
            _context = context;
            _currentUser = currentUser;
            _tenant = tenant;
        }

        public async Task<IEnumerable<string>> Handle(GetUserInfoEmailsByIds request, CancellationToken cancellationToken)
        {
            var employees = await _context.UserInfo
                .Where(x => request.Ids.Contains(x.Id))
                .ToListAsync(cancellationToken);

            if (employees.Count > 0)
            {
                return employees.Select(x => x.Email);
            }
            else
            {
                return new List<string>();
            }
        }
    }
}
