﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Application.UsersInfo.Queries;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class IsNationalIdUniqueHandler : IRequestHandler<IsNationalIdUnique, bool>
    {
        private readonly IrmsTenantDataContext _context;

        public IsNationalIdUniqueHandler(IrmsTenantDataContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(IsNationalIdUnique request, CancellationToken cancellationToken)
        {
            var query = _context.UserInfo.Where(x => x.NationalId == request.NationalId);

            if (request.UserInfoId.HasValue && request.UserInfoId.Value != default)
            {
                query = query.Where(x => x.Id != request.UserInfoId);
            }

            var result = await query.Take(1).CountAsync(cancellationToken);

            return result == 0;
        }
    }
}
