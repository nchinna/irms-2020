﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Application.Abstract;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Data.Read.UserInfo.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Application;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetMyProfileHandler : IRequestHandler<GetMyProfile, MyProfile>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _user;

        public GetMyProfileHandler(IrmsTenantDataContext context, ICurrentUser user, TenantBasicInfo tenant)
        {
            _context = context;
            _user = user;
            _tenant = tenant;
        }

        public async Task<MyProfile> Handle(GetMyProfile request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserInfo
                .FirstOrDefaultAsync(
                    x => x.UserId == request.Id
                         && !x.IsDeleted
                         && x.TenantId == _tenant.Id,
                    cancellationToken);

            if (userProfile == null)
            {
                throw new IncorrectRequestException("Can't find active user with requested id");
            }

            return new MyProfile(
                userProfile.FullName,
                userProfile.MobileNo,
                userProfile.Email,
                userProfile.RoleId);
        }
    }
}
