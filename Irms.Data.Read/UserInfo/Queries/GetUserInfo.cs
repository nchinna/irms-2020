﻿using System;
using MediatR;
using Irms.Data.Read.UserInfo.ReadModels;

namespace Irms.Data.Read.UserInfo.Queries
{
    public class GetUserInfo : IRequest<UserInfoReadModel>
    {
        public GetUserInfo(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
