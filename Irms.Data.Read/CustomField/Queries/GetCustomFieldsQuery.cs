﻿using Irms.Data.Read.CustomField.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.CustomField.Queries
{
    public class GetCustomFieldsQuery : IRequest<IEnumerable<CustomFieldReadModel>>
    {
        public IEnumerable<Guid> Fields { get; set; }
    }
}
