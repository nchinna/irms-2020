﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Data.Read.CustomField.Queries;
using Irms.Data.Read.CustomField.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CustomField.QueryHandler
{
    public class GetCustomFieldQueryHandler : IRequestHandler<GetCustomFieldQuery, CustomFieldReadModel>
    {
        private readonly IMapper _mapper;
        private readonly IrmsDataContext _context;
        public GetCustomFieldQueryHandler(
            IMapper mapper,
            IrmsDataContext context
        )
        {
            _mapper = mapper;
            _context = context;
        }


        public async Task<CustomFieldReadModel> Handle(GetCustomFieldQuery request, CancellationToken token)
        {
            var data = await _context.CustomField
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            return _mapper.Map<CustomFieldReadModel>(data);
        }
    }
}
