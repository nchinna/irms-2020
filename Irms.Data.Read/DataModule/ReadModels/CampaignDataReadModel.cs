﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class CampaignDataReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
