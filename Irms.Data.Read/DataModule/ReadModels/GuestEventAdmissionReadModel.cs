﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class GuestEventAdmissionReadModel 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Parking { get; set; }
        public string Seat { get; set; }
        public DateTime Date { get; set; }
    }
}
