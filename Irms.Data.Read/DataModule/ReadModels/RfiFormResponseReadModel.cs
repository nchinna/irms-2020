﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    /// <summary>
    /// Read model for rfi form response data fetch
    /// </summary>
    public class RfiFormResponseReadModel
    {
        public Guid Id { get; set; }
        public string GuestName { get; set; }
        public DateTime SubmissionDate { get; set; }
    }
}
