﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetSelectedImportantFieldsQuery : IRequest<IEnumerable<SelectedImportantField>>
    {
        public GetSelectedImportantFieldsQuery(Guid listId)
        {
            ListId = listId;
        }
        public Guid ListId { get; set; }
    }
}
