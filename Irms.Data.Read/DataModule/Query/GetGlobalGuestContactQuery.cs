﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetGlobalGuestContactQuery : IPageQuery<GlobalGuestContactReadModel>
    {
        public GetGlobalGuestContactQuery(
            int pageNo,
            int pageSize,
            string searchText,
            Guid listId)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
            ListId = listId;
        }

        public Guid ListId { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public string SearchText { get; set; }
    }
}

