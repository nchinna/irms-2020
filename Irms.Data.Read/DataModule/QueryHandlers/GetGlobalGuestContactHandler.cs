﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetGlobalGuestContactHandler : IPageHandler<GetGlobalGuestContactQuery, GlobalGuestContactReadModel>
    {
        private const string CountQuery = @"
			SELECT
			COUNT(*)
			FROM ContactListToContacts cltc
			INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
			WHERE cltc.ContactListId=@listId
			AND c.Deleted = 0
			AND cltc.TenantId=@tenantId";

        private const string PageQuery = @"
			WITH STATUS AS 
			(
			SELECT 
				cltc.ContactId AS Id,
				MAX(COALESCE(cir.Answer, 0) )AS Answer, 
				cl.Id AS contactListId, 
				e.id AS eventId,
				c.id AS contactId
			FROM ContactList cl 
				INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
				INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
				INNER JOIN dbo.ContactListToContacts cltc ON cltc.ContactListId = cl.Id
				INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
				INNER JOIN dbo.CampaignInvitation AS ci ON ci.EventCampaignId = ec.Id AND ci.InvitationTypeId IN (0,1)
				LEFT JOIN dbo.CampaignInvitationResponse AS cir ON 
				cir.CampaignInvitationId = ci.Id AND c.Id = cir.ContactId
			WHERE cl.Id = @listId
			AND ec.Id = (SELECT TOP 1 ec.Id FROM EventCampaign ec
								WHERE ec.GuestListId=@listId
								AND ec.Active = 1 
								ORDER BY ec.CreatedOn DESC)
			GROUP BY cl.id, e.id, c.id, cltc.ContactId
			)
			SELECT 
				c.Id AS Id,
				c.FullName AS GuestName,
				c.MobileNumber AS MobileNumber,
				c.Email as Email,
				CASE
					WHEN st.Answer = 0 OR st.Answer = 3 THEN 'Pending'
					WHEN st.Answer = 1 THEN 'Accepted'
					WHEN st.Answer = 2 THEN 'Rejected'
					WHEN st.Answer IS NULL THEN ''
				END AS Status
			FROM dbo.Contact c
				INNER JOIN dbo.ContactListToContacts AS cltc ON cltc.ContactId = c.Id
				INNER JOIN dbo.ContactList AS cl ON cl.Id=cltc.ContactListId 
				LEFT JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
				LEFT JOIN dbo.Event AS e ON ec.EventId = e.Id
				LEFT JOIN STATUS AS st ON st.eventId = e.Id AND st.contactListId = cl.Id AND st.contactId = c.Id
			WHERE cltc.ContactListId=@listId
			AND c.Deleted = 0
			AND cltc.TenantId=@tenantId
			{0}
			GROUP BY c.Id, c.FullName, c.MobileNumber, c.Email,st.Answer
			ORDER BY c.FullName
            OFFSET @skip ROWS
            FETCH NEXT @take ROWS ONLY";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetGlobalGuestContactHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _connectionString = connectionString;
        }

        private const string SearchClause = @"

			AND c.FullName like @searchText";

        /// <summary>
        /// Retrieves contacts in guest contact list
        /// </summary>
        /// <param name="request">list id and query params</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<GlobalGuestContactReadModel>> Handle(GetGlobalGuestContactQuery request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var pageQuery = string.Format(PageQuery, string.Join("\r\n", clauses));

            var parameters = new
            {
                listId = request.ListId,
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                tenantId = _tenant.Id,
                skip = request.Skip(),
                take = request.Take()
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(CountQuery, parameters);
                var data = await connection.QueryAndLog<GlobalGuestContactReadModel>(pageQuery, parameters);
                return new PageResult<GlobalGuestContactReadModel>(data, total);
            }
        }
    }
}
