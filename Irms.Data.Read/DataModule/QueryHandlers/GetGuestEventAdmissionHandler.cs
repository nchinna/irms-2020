﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
	[HasSqlQuery(DataQuery)]
    public class GetGuestEventAdmissionHandler : IPageHandler<GetGuestEventAdmissionQuery, GuestEventAdmissionReadModel>
    {
		private const string CountQuery = @"
			WITH STATUS AS (
				SELECT 
					MAX(COALESCE(cir.Answer, 0) )AS Answer, 
					cl.Id AS contactListId, 
					e.id AS eventId,
					c.id AS contactId
				FROM ContactList cl 
					INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
					INNER JOIN dbo.ContactListToContacts cltc ON cltc.ContactListId = cl.Id
					INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
					INNER JOIN dbo.CampaignInvitation AS ci ON ci.EventCampaignId = ec.Id AND ci.InvitationTypeId IN (0,1)
					LEFT JOIN dbo.CampaignInvitationResponse AS cir ON 
					cir.CampaignInvitationId = ci.Id AND c.Id = cir.ContactId
				WHERE cltc.ContactId=@contactId
				AND cltc.TenantId=@tenantId
				AND ec.Active = 1
				GROUP BY cl.id, e.id, c.id
			)
			SELECT 
					Count(c.Id)
			FROM dbo.Contact as c
					INNER JOIN dbo.ContactListToContacts AS cltc ON cltc.ContactId = c.Id
					INNER JOIN dbo.ContactList AS cl ON cl.Id=cltc.ContactListId 
					LEFT JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					LEFT JOIN dbo.Event AS e ON ec.EventId = e.Id
					LEFT JOIN STATUS AS st ON st.eventId = e.Id AND st.contactListId = cl.Id AND st.contactId = c.Id
			WHERE c.Id=@contactId AND st.Answer IS NOT NULL
			AND c.TenantId=@tenantId
			GROUP BY c.Id
			ORDER BY c.Id";

		private const string DataQuery = @"
			WITH STATUS AS (
				SELECT 
					MAX(COALESCE(cir.Answer, 0) )AS Answer, 
					cl.Id AS contactListId, 
					e.id AS eventId,
					c.id AS contactId
				FROM ContactList cl 
					INNER JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					INNER JOIN dbo.Event AS e ON ec.EventId = e.Id
					INNER JOIN dbo.ContactListToContacts cltc ON cltc.ContactListId = cl.Id
					INNER JOIN dbo.Contact AS c ON cltc.ContactId = c.Id
					INNER JOIN dbo.CampaignInvitation AS ci ON ci.EventCampaignId = ec.Id AND ci.InvitationTypeId IN (0,1)
					LEFT JOIN dbo.CampaignInvitationResponse AS cir ON 
					cir.CampaignInvitationId = ci.Id AND c.Id = cir.ContactId
				WHERE cltc.ContactId=@contactId
				AND cltc.TenantId=@tenantId
				AND ec.Active = 1
				GROUP BY cl.id, e.id, c.id
			)
			SELECT 
					c.Id,
					ec.Name AS Name,
					CASE
						WHEN st.Answer = 0 OR st.Answer = 3 THEN 'Pending'
						WHEN st.Answer = 1 THEN 'Accepted'
						WHEN st.Answer = 2 THEN 'Rejected'
						WHEN st.Answer IS NULL THEN ''
					END AS Status,
					'P4 16' AS Parking,
					'S4 R2' AS Seat,
					e.StartDateTime AS Date
			FROM dbo.Contact as c
					INNER JOIN dbo.ContactListToContacts AS cltc ON cltc.ContactId = c.Id
					INNER JOIN dbo.ContactList AS cl ON cl.Id=cltc.ContactListId 
					LEFT JOIN dbo.EventCampaign AS ec ON ec.GuestListId = cl.Id
					LEFT JOIN dbo.Event AS e ON ec.EventId = e.Id
					LEFT JOIN STATUS AS st ON st.eventId = e.Id AND st.contactListId = cl.Id AND st.contactId = c.Id
			WHERE c.Id=@contactId AND st.Answer IS NOT NULL
			AND c.TenantId=@tenantId
			ORDER BY c.Id
  			OFFSET @skip ROWS
  			FETCH NEXT @take ROWS ONLY";

		private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetGuestEventAdmissionHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _connectionString = connectionString;
        }

		/// <summary>
		/// Loads event admission information by user for all participated events
		/// </summary>
		/// <param name="request">guest id</param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
        public async Task<IPageResult<GuestEventAdmissionReadModel>> Handle(GetGuestEventAdmissionQuery request, CancellationToken cancellationToken)
        {
			var parameters = new
			{
				contactId = request.GuestId,
				tenantId = _tenant.Id,
				skip = request.Skip(),
				take = request.Take()
			};

			using (IDbConnection connection = new SqlConnection(_connectionString.Value))
			{
				var count = await connection.ExecuteScalarAndLog<int>(CountQuery, parameters);
				var data = await connection.QueryAndLog<GuestEventAdmissionReadModel>(DataQuery, parameters);
				return new PageResult<GuestEventAdmissionReadModel>(data, count);
			}
		}
    }
}
