﻿using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetRfiFormNameHandler : IRequestHandler<GetRfiFormNameQuery, RfiFormNameReadModel>
    {
        private readonly IrmsDataContext _context;
        public GetRfiFormNameHandler(
            IrmsDataContext context
            )
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves RFI form name and responses count
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<RfiFormNameReadModel> Handle(GetRfiFormNameQuery request, CancellationToken token)
        {
            var rfi = await _context.RfiForm
                .Include(x => x.RfiFormResponses)
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(rfi.FormSettings);

            var responses = rfi.RfiFormResponses
                .GroupBy(x => x.ContactId)
                .Count();

            return new RfiFormNameReadModel(dict["title"], responses);
        }
    }
}
