﻿using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(GetRfiFormInfoQuery)]
    public class GetRfiFormInfoHandler : IRequestHandler<GetRfiFormInfoQuery, IPageResult<RfiFormInfoReadModel>>
    {
        public const string GetRfiFormInfoQuery = @"
            SELECT
            rf.Id,
            rf.FormSettings AS Title,
            CASE
	            WHEN ci.InvitationTypeId = 2 THEN 'Accepted '
	            WHEN ci.InvitationTypeId = 3 THEN 'Rejected '
            END AS Target,
            COUNT (DISTINCT rfr.ContactId) As Responses,
            COUNT (DISTINCT cit.ContactId) As UniqueVisits,
            COUNT (DISTINCT cit.Id) AS TotalVisits
            FROM RfiForm rf
            INNER JOIN CampaignInvitation ci on ci.Id = rf.CampaignInvitationId
            LEFT JOIN RfiFormResponse rfr on rfr.RfiFormId = rf.Id
            LEFT JOIN ContactInvitationTelemetry cit on cit.CampaignInvitationId = ci.Id
            INNER JOIN EventCampaign ec on ec.Id = ci.EventCampaignId
            WHERE ec.Id = @campaignId
            GROUP BY rf.Id, rf.FormSettings, ci.InvitationTypeId
            ORDER BY rf.FormSettings DESC
            OFFSET @skip ROWS
            FETCH NEXT @take ROWS ONLY";

        private readonly IConnectionString _connectionString;
        public GetRfiFormInfoHandler(
            IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IPageResult<RfiFormInfoReadModel>> Handle(GetRfiFormInfoQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                campaignId = request.Id,
                skip = request.Skip(),
                take = request.Take(),
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var result = await connection.QueryAndLog<RfiFormInfoReadModel>(GetRfiFormInfoQuery, parameters);
                return new PageResult<RfiFormInfoReadModel>(result, result.Count());
            }
        }
    }
}
