﻿using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(CountQuery)]
    [HasSqlQuery(GetListAnalysisRfiFormInfoQuery)]
    public class GetListAnalysisRfiFormInfoHandler : IRequestHandler<GetListAnalysisRfiFormInfoQuery, IPageResult<ListAnalysisRfiFormInfoReadModel>>
    {
        public const string CountQuery = @"
		SELECT COUNT(*) FROM (
            SELECT
            rf.Id,
            rf.FormSettings AS Title,
            cl.Name AS ListName,
            COUNT (DISTINCT rfr.ContactId) As Responses,
            COUNT (DISTINCT cit.ContactId) As UniqueVisits,
            COUNT (DISTINCT cit.Id) AS TotalVisits
            FROM RfiForm rf
            INNER JOIN CampaignInvitation ci on ci.Id = rf.CampaignInvitationId
            LEFT JOIN RfiFormResponse rfr on rfr.RfiFormId = rf.Id
            LEFT JOIN ContactInvitationTelemetry cit on cit.CampaignInvitationId = ci.Id
            INNER JOIN EventCampaign ec on ec.Id = ci.EventCampaignId
            INNER JOIN ContactList cl on cl.Id = ec.GuestListId
            WHERE ec.EventId = @eventId
            AND ec.CampaignType = 1
            GROUP BY rf.Id, rf.FormSettings, ci.InvitationTypeId, cl.Name
		) InnerQuery";

        public const string GetListAnalysisRfiFormInfoQuery = @"
            SELECT
            rf.Id,
            rf.FormSettings AS Title,
            cl.Name AS ListName,
            COUNT (DISTINCT rfr.ContactId) As Responses,
            COUNT (DISTINCT cit.ContactId) As UniqueVisits,
            COUNT (DISTINCT cit.Id) AS TotalVisits
            FROM RfiForm rf
            INNER JOIN CampaignInvitation ci on ci.Id = rf.CampaignInvitationId
            LEFT JOIN RfiFormResponse rfr on rfr.RfiFormId = rf.Id
            LEFT JOIN ContactInvitationTelemetry cit on cit.CampaignInvitationId = ci.Id
            INNER JOIN EventCampaign ec on ec.Id = ci.EventCampaignId
            INNER JOIN ContactList cl on cl.Id = ec.GuestListId
            WHERE ec.EventId = @eventId
            AND ec.CampaignType = 1
            GROUP BY rf.Id, rf.FormSettings, ci.InvitationTypeId, cl.Name
            ORDER BY rf.FormSettings DESC
            OFFSET @skip ROWS
            FETCH NEXT @take ROWS ONLY";

        private readonly IConnectionString _connectionString;
        public GetListAnalysisRfiFormInfoHandler(
            IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IPageResult<ListAnalysisRfiFormInfoReadModel>> Handle(GetListAnalysisRfiFormInfoQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                eventId = request.EventId,
                skip = request.Skip(),
                take = request.Take(),
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(CountQuery, parameters);
                var result = await connection.QueryAndLog<ListAnalysisRfiFormInfoReadModel>(GetListAnalysisRfiFormInfoQuery, parameters);
                return new PageResult<ListAnalysisRfiFormInfoReadModel>(result, total);
            }
        }
    }
}
