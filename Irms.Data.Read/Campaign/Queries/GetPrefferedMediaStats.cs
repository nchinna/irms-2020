﻿using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetPrefferedMediaStats : IRequest<PrefferedMediaStats>
    {

        public GetPrefferedMediaStats(Guid id, Guid listId)
        {
            Id = id;
            ListId = listId;
        }

        public Guid Id { get; }
        public Guid ListId { get; set; }
    }

}
