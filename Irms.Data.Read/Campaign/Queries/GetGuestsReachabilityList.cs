﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetGuestsReachabilityList : IRequest<IPageResult<GuestReachabilityListItem>>
    {
        public string SearchText { get; set; }
        public Guid CampaignId { get; set; }
        public Guid GuestListId { get; set; }
        public List<MediaFilter> Filter { get; set; }
        //public bool IgnoreReachability { get; set; }
    }

}
