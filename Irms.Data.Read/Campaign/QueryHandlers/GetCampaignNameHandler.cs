﻿using Irms.Data.Read.Campaign.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Campaign.QueryHandlers
{
    public class GetCampaignNameHandler : IRequestHandler<GetCampaignNameQuery, string>
    {
        private readonly IrmsDataContext _context;
        public GetCampaignNameHandler(
            IrmsDataContext context)
        {
            _context = context;
        }
        public async Task<string> Handle(GetCampaignNameQuery request, CancellationToken token)
        {
            var campaign = await _context.EventCampaign
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            return campaign.Name;
        }
    }
}
