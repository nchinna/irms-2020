﻿using Hangfire.Storage;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Data.Read.Registration;
using Irms.Domain.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Data.Read.Campaign.ReadModels.CampaignListItem;

namespace Irms.Data.Read.Campaign.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery))]
    [HasSqlQuery(nameof(PageQuery))]
    public class GetCampaignListHandler : IPageHandler<GetCampaignList, CampaignListItem>
    {
        private const string CountQuery = @"
                SELECT
                  COUNT(ec.Id)
                FROM EventCampaign ec
                WHERE ec.TenantId = @tenant
                    AND ec.EventId = @eventId
                    AND ec.CampaignType = 0
                    AND ec.Deleted = 0";

        private const string PageQuery = @"
                WITH 
                AllMessageLogs AS (
	                SELECT
	                ciml.InvitationTemplateTypeId,
	                ci.Title AS Name,
					ci.InvitationTypeId AS Type,
	                ciml.MessageStatus AS LogMessageStatus,
	                ciml.InvitationTemplateTypeId AS LogInvitationTemplateTypeId,
	                ciml.CampaignInvitationId,
	                ciml.ContactId AS LogContactId,
	                pr.MessageStatus,
	                pr.ProviderType,
	                pr.ContactId,
	                ec.Id AS EventCampaignId
	                from CampaignInvitationMessageLog ciml
	                INNER JOIN CampaignInvitation ci on ciml.CampaignInvitationId=ci.Id
	                INNER JOIN eventcampaign ec on ci.EventCampaignId=ec.Id
	                LEFT JOIN ProviderResponse pr on pr.CampaignInvitationMessageLogId=ciml.Id
	                WHERE ec.EventId=@eventId
	                GROUP BY ciml.CampaignInvitationId, 
	                        ci.Title,
       		                pr.MessageStatus,
       		                pr.ProviderType,
       		                ciml.InvitationTemplateTypeId,
       		                ciml.MessageStatus,
       		                pr.ContactId,
       		                ciml.InvitationTemplateTypeId,
       		                ciml.ContactId,
			                ec.Id,
							ci.InvitationTypeId
                ),
                Responses AS (
	                SELECT cir.*,ec.Id as EcId FROM CampaignInvitationResponse cir 
	                INNER JOIN CampaignInvitation ci ON cir.CampaignInvitationId = ci.Id
	                INNER JOIN eventcampaign ec on ci.EventCampaignId=ec.Id
	                WHERE ec.EventId=@eventId
                )
                SELECT ec.Id,
                        ec.Name,
                        ec.CampaignStatus AS Status,
                        COUNT(clc.Id) TotalGuests,
		                CASE
			                WHEN ec.CampaignStatus = 1 THEN
				                (SELECT * FROM 
									(SELECT 
										CAST(0x0 AS UNIQUEIDENTIFIER) AS CampaignInvitationId,
										'All' AS Name,
										(SELECT COUNT(DISTINCT Concat(LogContactId,CampaignInvitationId)) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND MessageStatus=4 AND ProviderType=2 AND Type in (0,1)) AS EmailDeliveredSend,
										(SELECT COUNT(DISTINCT Concat(LogContactId,CampaignInvitationId)) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND LogMessageStatus=2 AND LogInvitationTemplateTypeId = 0 AND Type IN (0,1)) AS EmailDeliveredTotal,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND MessageStatus=4 AND ProviderType=0  AND LogInvitationTemplateTypeId=1  AND Type in (0,1)) AS SmsDeliveredSend,
										(SELECT COUNT(LogContactId) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND LogMessageStatus=2 AND LogInvitationTemplateTypeId=1 AND Type in (0,1)) AS SmsDeliveredTotal,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND MessageStatus=4 AND ProviderType=0  AND LogInvitationTemplateTypeId=2 AND Type in (0,1)) AS WhatsappDeliveredSend,
										(SELECT COUNT(DISTINCT Concat(LogContactId,CampaignInvitationId)) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND LogMessageStatus=2 AND LogInvitationTemplateTypeId=2 AND Type in (0,1)) AS WhatsappDeliveredTotal,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=1 AND ResponseMediaType=0) AS EmailAccepted,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=2 AND ResponseMediaType=0) AS EmailRejected,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=1 AND ResponseMediaType=1) AS SmsAccepted,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=2 AND ResponseMediaType=1) AS SmsRejected,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=1 AND ResponseMediaType=2) AS WhatsappAccepted,
										(SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer=2 AND ResponseMediaType=2) AS WhatsappRejected,
										(COUNT(clc.Id) - (SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer IN (1,2))) AS EmailPending,
										(COUNT(clc.Id) - (SELECT COUNT(DISTINCT ContactId) FROM Responses WHERE EcId=ec.Id AND Answer IN (1,2))) AS WhatsappPending,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND ProviderType=2 AND MessageStatus=6) AS EmailOpened,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND ProviderType=2 AND MessageStatus=7) AS EmailClicked,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE EventCampaignId=ec.Id AND ProviderType=0 AND MessageStatus=20 AND LogInvitationTemplateTypeId=2 AND LogInvitationTemplateTypeId=2 AND Type in (0,1)) AS WhatsAppRead
									FROM AllMessageLogs mls
									WHERE mls.EventCampaignId=ec.Id AND mls.Type IN (0,1)
									GROUP BY mls.CampaignInvitationId, mls.EventCampaignId,
										mls.Name
									UNION
									SELECT 
									mls.CampaignInvitationId,
									mls.Name,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND MessageStatus=4 AND ProviderType=2) AS EmailDeliveredSend,
										(SELECT COUNT(DISTINCT LogContactId) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND LogMessageStatus=2 AND LogInvitationTemplateTypeId = 0 AND Type IN (0,1)) AS EmailDeliveredTotal,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND MessageStatus=4 AND ProviderType=0  AND LogInvitationTemplateTypeId=1) AS SmsDeliveredSend,
										(SELECT COUNT(DISTINCT LogContactId) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND LogMessageStatus=2 AND LogInvitationTemplateTypeId=1) AS SmsDeliveredTotal,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND MessageStatus=4 AND ProviderType=0 AND LogInvitationTemplateTypeId=2) AS WhatsappDeliveredSend,
										(SELECT COUNT(DISTINCT LogContactId) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND LogMessageStatus=2 AND LogInvitationTemplateTypeId=2) AS WhatsappDeliveredTotal,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=1 AND ResponseMediaType=0) AS EmailAccepted,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=2 AND ResponseMediaType=0) AS EmailRejected,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=1 AND ResponseMediaType=1) AS SmsAccepted,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=2 AND ResponseMediaType=1) AS SmsRejected,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=1 AND ResponseMediaType=2) AS WhatsappAccepted,
										(SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer=2 AND ResponseMediaType=2) AS WhatsappRejected,
										((SELECT COUNT(DISTINCT LogContactId) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND LogMessageStatus=2 AND LogInvitationTemplateTypeId = 0 AND Type IN (0,1)) - (SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer IN (1,2) AND ResponseMediaType=0)) AS EmailPending,
										((SELECT COUNT(DISTINCT LogContactId) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND LogMessageStatus=2 AND LogInvitationTemplateTypeId=2) - (SELECT COUNT(*) FROM Responses WHERE CampaignInvitationId=mls.CampaignInvitationId AND Answer IN (1,2) AND ResponseMediaType=2)) AS WhatsappPending,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND ProviderType=2 AND MessageStatus=6) AS EmailOpened,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND ProviderType=2 AND MessageStatus=7) AS EmailClicked,
										(SELECT COUNT(*) FROM AllMessageLogs WHERE CampaignInvitationId=mls.CampaignInvitationId AND ProviderType=0 AND MessageStatus=20 AND LogInvitationTemplateTypeId=2) AS WhatsAppRead
									FROM AllMessageLogs mls
									WHERE mls.EventCampaignId=ec.Id AND mls.Type IN (0,1)
									GROUP BY mls.CampaignInvitationId, 
										mls.Name) 
								as iner FOR JSON PATH)
			                ELSE NULL			
		                END AS InvitationData,		
                        ISNULL(ec.ModifiedOn, ec.CreatedOn) AS LastUpdate
                FROM EventCampaign ec
                LEFT JOIN ContactList cl ON ec.GuestListId = cl.Id
                LEFT JOIN ContactListToContacts clc ON cl.Id = clc.ContactListId
                WHERE ec.TenantId=@tenant
	                AND ec.EventId=@eventId
                    AND ec.CampaignType = 0
                    AND ec.Deleted = 0
                GROUP BY ec.Id,
                            ec.Name,
                            ec.CampaignStatus,
                            ec.ModifiedOn,
                            ec.CreatedOn                
                ORDER BY ec.CreatedOn DESC
              OFFSET @skip ROWS
              FETCH NEXT @take ROWS ONLY";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetCampaignListHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// this method will return list of campaign with pagination
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<CampaignListItem>> Handle(GetCampaignList request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();

            var parameters = new
            {
                tenant = _tenant.Id,
                eventId = request.EventId,
                skip = request.Skip(),
                take = request.Take(),
            };

            var conditionStr = string.Join("\r\n", clauses);
            var countQuery = string.Format(CountQuery, conditionStr);
            var pageQuery = string.Format(PageQuery, conditionStr);

            using (var connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var items = await connection.QueryAndLog<CampaignListItem>(pageQuery, parameters);

                return new PageResult<CampaignListItem>(items, total);
            }
        }
    }
}
