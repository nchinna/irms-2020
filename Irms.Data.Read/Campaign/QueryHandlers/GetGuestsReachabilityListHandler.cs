﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Data.Read.Registration;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    [HasSqlQuery(nameof(PageQuery))]
    public class GetGuestsReachabilityListHandler : IRequestHandler<GetGuestsReachabilityList, IPageResult<GuestReachabilityListItem>>
    {
        private const string PageQuery = @"
                SELECT c.Id,
                        c.FullName,
                        cr.Email,
                        cr.Sms,
                        cr.WhatsApp,
		                ISNULL(cpm.Email,0) As EmailChecked,
		                ISNULL(cpm.Sms,0) As SmsChecked,
		                ISNULL(cpm.WhatsApp,0) As WhatsAppChecked,
		                cr.OptStatus As WhatsAppOptedIn
                FROM Contact c
                LEFT JOIN ContactReachability cr ON cr.ContactId = c.Id
                INNER JOIN ContactListToContacts ctc ON ctc.ContactId = c.Id
                LEFT JOIN CampaignPrefferedMedia cpm on c.Id = cpm.ContactId AND cpm.EventCampaignId= @campaignId
                WHERE ctc.ContactListId = @list
                    {0}";

        //private const string IgnoreReachabilityQuery = @"
        //        SELECT c.Id,
        //                c.FullName,
        //                1 AS Email,
        //                1 AS Sms,
        //                1 AS Whatsapp,
        //          ISNULL(cpm.Email,0) As EmailChecked,
        //          ISNULL(cpm.Sms,0) As SmsChecked,
        //          ISNULL(cpm.WhatsApp,0) As WhatsAppChecked,
        //          cr.OptStatus As WhatsAppOptedIn
        //        FROM Contact c
        //        LEFT JOIN ContactReachability cr ON cr.ContactId = c.Id
        //        INNER JOIN ContactListToContacts ctc ON ctc.ContactId = c.Id
        //        LEFT JOIN CampaignPrefferedMedia cpm on c.Id = cpm.ContactId AND cpm.EventCampaignId= @campaignId
        //        WHERE ctc.ContactListId = @list
        //            {0}";

        private const string SearchClause = @"
            AND c.FullName like @searchText";

        private readonly IConnectionString _connectionString;

        public GetGuestsReachabilityListHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// this method will return list of guest reachability with email, phone info
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<GuestReachabilityListItem>> Handle(GetGuestsReachabilityList request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            if (request.Filter != null && request.Filter.Count > 0)
            {
                clauses.Add(GetFilterQuery(request.Filter));
            }

            var parameters = new
            {
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                list = request.GuestListId,
                campaignId = request.CampaignId
            };

            var conditionStr = string.Join("\r\n", clauses);
            var pageQuery = string.Format(PageQuery, conditionStr);
            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var guests = await connection.QueryAndLog<GuestReachabilityListItem>(pageQuery, parameters);
                return new PageResult<GuestReachabilityListItem>(guests, guests.Count());
            }
        }

        private string GetFilterQuery(List<MediaFilter> filters)
        {
            if (filters.Count == Enum.GetNames(typeof(MediaFilter)).Length)
            {
                return string.Empty;
            }

            StringBuilder filterQuery = new StringBuilder();
            filterQuery.Append("AND (");

            for (var i = 0; i < filters.Count; i++)
            {
                if (i > 0)
                {
                    filterQuery.Append(" OR ");
                }

                if (filters[i] == MediaFilter.Email)
                {
                    filterQuery.Append("cr.Email = 1");
                }
                else if (filters[i] == MediaFilter.Sms)
                {
                    filterQuery.Append("cr.Sms = 1");
                }
                else if (filters[i] == MediaFilter.WhatsApp)
                {
                    filterQuery.Append("cr.WhatsApp = 1");
                }
            }

            filterQuery.Append(")");
            return filterQuery.ToString();
        }
    }
}
