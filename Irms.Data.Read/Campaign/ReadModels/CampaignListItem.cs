﻿using Irms.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Irms.Data.Read.Campaign.ReadModels
{
    public class CampaignListItem
    {
        public CampaignListItem() { }

        public CampaignListItem(Guid id,
            string name,
            byte campaignStatus,
            int totalGuests,
            DateTime lastUpdate)
        {
            Id = id;
            Status = (CampaignStatus)campaignStatus;
            Name = name;
            TotalGuests = totalGuests;
            LastUpdate = lastUpdate;
        }

        public Guid Id { get; set; }
        public CampaignStatus Status { get; set; }
        public string Name { get; set; }
        public int TotalGuests { get; set; }
        public DateTime LastUpdate { get; set; }
        public string InvitationData { get; set; }
    }
}
