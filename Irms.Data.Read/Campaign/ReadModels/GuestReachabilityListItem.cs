﻿using System;

namespace Irms.Data.Read.Campaign.ReadModels
{
    public class GuestReachabilityListItem
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool WhatsApp { get; set; }
        public bool EmailChecked { get; set; }
        public bool SmsChecked { get; set; }
        public bool WhatsAppChecked { get; set; }
        public bool? WhatsAppOptedIn { get; set; }
    }
}
