﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Product.ReadModels;

namespace Irms.Data.Read.Product.Queries
{
    public class GetProductListQuery : IPageTimeQuery<ProductListItem>
    {

        public GetProductListQuery(
            int pageNo,
            int pageSize,
            string searchText,
            TimeFilterRange timeFilter)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
            TimeFilter = timeFilter;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public TimeFilterRange TimeFilter { get; }

    }
}
