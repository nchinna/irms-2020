﻿using AutoMapper;
using Dapper;
using Irms.Application.Product.Commands;
using Irms.Data.Abstract;
using Irms.Domain;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Product.QueryHandlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdCmd, Domain.Entities.Product>
    {
        private const string Query = @"
        SELECT [Product].[Id]
          ,[Product].[Title]
          ,[Price]
          ,[DiscountPercentage]
          ,[DiscountAmount]
          ,[FinalPrice]
          ,[CurrencyId]
          ,[LicensePeriodId]
          ,[LicensePeriodDays]
          ,[IsShowOnWeb]
          ,[IsDeleted]
          ,[CreatedById]
          ,[CreatedOn]
          ,[ModifiedById]
          ,[ModifiedOn]
          ,[License].[Id]
          ,[License].[Title]
          ,[License].[Days]
        FROM [dbo].[Product]
        INNER JOIN [dbo].[LicensePeriod] AS License ON License.Id = Product.LicensePeriodId
        WHERE [Product].[Id] = @Id
        ";
        private readonly IConnectionString _connectionString;
        
        public GetProductByIdHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Domain.Entities.Product> Handle(GetProductByIdCmd request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                Id = request.ProductId
            };
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var page = await db.QueryAsync<Domain.Entities.Product, Domain.Entities.LicensePeriod, Domain.Entities.Product>(Query,
                    (product, licensePeriod) =>
                    {
                        if (licensePeriod is null)
                        {
                            throw new ArgumentNullException(nameof(licensePeriod));
                        }

                        product.SetLicense(licensePeriod);
                        return product;
                    },
                    parameters, splitOn: "LicensePeriodId,Id");
                var result = page.ToList();
                if (result == null || !result.Any())
                {
                    throw new IncorrectRequestException("Product not found");
                }
                if(result.Count > 1)
                {
                    throw new Exception("Multiple results for one product Id");
                }
                return page.First();
            }
        }
    }
}
