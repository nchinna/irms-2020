﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Read.Contact.ReadModels;
using System.Collections.Generic;
using AutoMapper;
using System;
using Irms.Data.Read.ListAnalysis.Queries;
using Irms.Data.Read.ListAnalysis.ReadModels;

namespace Irms.Data.Read.ListAnalysis.QueryHandlers
{
    public class GetValidationsCountHandler : IRequestHandler<GetValidationsCountQuery, ValidationsCount>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly IMapper _mapper;

        public GetValidationsCountHandler(IrmsDataContext context,
            TenantBasicInfo tenant,
            IMapper mapper)
        {
            _context = context;
            _tenant = tenant;
            _mapper = mapper;
        }

        /// <summary>
        /// get contact count
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ValidationsCount> Handle(GetValidationsCountQuery request, CancellationToken token)
        {
            var contacts = _context.Contact
                          .Where(x => x.ContactListToContacts.Any(y => y.ContactListId == request.ListId)
                          && x.TenantId == _tenant.Id);

            //var count = await q.CountAsync(token);
            //var contacts = await q
            //    .Skip(request.Skip())
            //    .Take(request.Take())
            //    .ToListAsync(token);

            var customFields = await _context.CustomField
                .Where(x => request.CustomFields.Contains(x.Id) && x.TenantId == _tenant.Id)
                .ToListAsync(token);

            var contactCustomFields = await _context.ContactCustomField
                .Include(x => x.CustomField)
                .Where(x => x.Contact.ContactListToContacts.Any(y => y.ContactListId == request.ListId)
                && x.TenantId == _tenant.Id)
                .ToListAsync(token);

            var result = new List<ContactWithCustomFields>();
            int errors = 0;
            int fieldWarnings = 0;
            int guestWarnings = 0;
            var missingGuests = new List<Guid>();
            foreach (var contact in contacts)
            {
                var mapped = _mapper.Map<ContactWithCustomFields>(contact);
                int gw = 0;
                foreach (var cf in customFields)
                {
                    var field = contactCustomFields.FirstOrDefault(x => x.ContactId == contact.Id && x.CustomFieldId == cf.Id);

                    //field has value
                    if (field != null)
                    {
                        errors += ComputeErrors(cf, field.Value);
                        if (field.Value == null)
                        {
                            gw++;
                        }
                    }
                    else
                    {
                        gw++;
                    }
                }

                //compute warnings...
                gw += ComputeWarnings(contact, request.ReservedFields);
                fieldWarnings += gw;

                if (gw > 0)
                {
                    guestWarnings++;
                    missingGuests.Add(contact.Id);
                }
            }

            return new ValidationsCount { Errors = errors, FieldWarnings = fieldWarnings, GuestWarnings = guestWarnings, MissingGuests = missingGuests };
        }

        private int ComputeWarnings(EntityClasses.Contact contact, IEnumerable<string> reservedFields)
        {
            int warnings = 0;
            foreach (var field in reservedFields)
            {
                if (field.Equals("title", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.Title));
                }
                else if (field.Equals("fullName", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.FullName));
                }

                else if (field.Equals("documentNumber", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.DocumentNumber));
                }
                else if (field.Equals("organization", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.Organization));
                }
                else if (field.Equals("position", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.Position));
                }
                else if (field.Equals("email", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.Email));
                }
                else if (field.Equals("mobileNumber", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.MobileNumber));
                }
                else if (field.Equals("gender", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += contact.GenderId == null ? 1 : 0;
                }
                else if (field.Equals("preferredName", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.PreferredName));
                }

                else if (field.Equals("nationalityId", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += contact.NationalityId == null ? 1 : 0;
                }
                else if (field.Equals("expirationDate", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += contact.ExpirationDate == null ? 1 : 0;
                }
                else if (field.Equals("alternativeEmail", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.AlternativeEmail));
                }
                else if (field.Equals("secondaryMobileNumber", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.SecondaryMobileNumber));
                }
                else if (field.Equals("workNumber", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += Convert.ToInt32(string.IsNullOrEmpty(contact.WorkNumber));
                }
                else if (field.Equals("documentTypeId", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += contact.DocumentTypeId == null ? 1 : 0;

                }
                else if (field.Equals("issuingCountryId", StringComparison.OrdinalIgnoreCase))
                {
                    warnings += contact.IssuingCountryId == null ? 1 : 0;
                }
            }

            return warnings;
        }

        private int ComputeErrors(EntityClasses.CustomField cf, string value)
        {
            int errors = 0;
            switch (cf.CustomFieldType)
            {
                default:
                case 0:  //Text
                    errors = errors + CheckMinMaxValue(cf, value);
                    break;

                case 1: // Number
                    errors = errors + CheckNumber(value);
                    errors = errors + CheckMinMaxValue(cf, value);
                    break;

                case 2: // Date
                        //var date =  ? Convert.ToDateTime(value) : null;
                    if (value.IsNotNullOrEmpty() && cf.MinValue.HasValue)
                    {
                        errors += Convert.ToDateTime(value) < Convert.ToDateTime(cf.MinValue) ? 1 : 0;
                    }
                    if (value.IsNotNullOrEmpty() && cf.MaxValue.HasValue)
                    {
                        errors += Convert.ToDateTime(value) > Convert.ToDateTime(cf.MaxValue) ? 1 : 0;
                    }

                    break;
            }
            return errors;
        }

        private int CheckNumber(string value)
        {
            return int.TryParse(value, out _) ? 1 : 0;
        }

        private int CheckMinMaxValue(EntityClasses.CustomField cf, string value)
        {
            int errors = 0;
            if (value.IsNotNullOrEmpty() && cf.MinValue.HasValue)
            {
                if (value.Length < cf.MinValue)
                {
                    errors++;
                }
            }

            if (value.IsNotNullOrEmpty() && cf.MaxValue.HasValue)
            {
                if (value.Length > cf.MaxValue)
                {
                    errors++;
                }
            }

            return errors;
        }
    }

}
