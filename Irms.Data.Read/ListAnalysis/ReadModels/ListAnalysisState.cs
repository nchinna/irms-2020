﻿using Irms.Domain.Entities;

namespace Irms.Data.Read.ListAnalysis.ReadModels
{
    public class ListAnalysisState
    {
        public ListAnalysisState(ListAnalysisStep? listAnalysisStep, int? listAnalysisPageNo, int? listAnalysisPageSize)
        {
            ListAnalysisStep = listAnalysisStep ?? ListAnalysisStep.ImportantFields;
            ListAnalysisPageNo = listAnalysisPageNo;
            ListAnalysisPageSize = listAnalysisPageSize;
        }

        public ListAnalysisStep ListAnalysisStep { get; set; }
        public int? ListAnalysisPageNo { get; set; }
        public int? ListAnalysisPageSize { get; set; }
    }
}