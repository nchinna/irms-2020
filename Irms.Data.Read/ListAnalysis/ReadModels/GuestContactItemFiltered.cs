﻿using System;
using System.Collections.Generic;

namespace Irms.Data.Read.ListAnalysis.ReadModels
{
    public class ListAnalysisImportantFields
    {
        public IEnumerable<Field> CustomsFields { get; set; }
        public IEnumerable<Field> ReservedFields { get; set; }
    }

    public class Field
    {
        public Field(string value, bool isRequired)
        {
            Value = value;
            IsRequired = isRequired;
        }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
    }
}
