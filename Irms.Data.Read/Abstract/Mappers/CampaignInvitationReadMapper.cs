﻿using Irms.Data.Read.Campaign.ReadModels;
using Irms.Data.Read.CampaignInvitation.ReadModels;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class CampaignInvitationReadMapper : AutoMapper.Profile
    {
        public CampaignInvitationReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.CampaignInvitation, CampaignInvitationRsvp>();
            CreateMap<EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
            CreateMap<EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            CreateMap<EntityClasses.CampaignInvitation, CampaignInvitation.ReadModels.CampaignInvitation>();

            CreateMap<EntityClasses.CampaignInvitation, CampaignInvitationBasicInfo>()
                .ForMember(x => x.InvitationType, x => x.MapFrom(x => (Domain.Entities.InvitationType)x.InvitationTypeId));

            CreateMap<EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>();
        }
    }
}
