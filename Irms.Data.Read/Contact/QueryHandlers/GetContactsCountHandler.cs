﻿using Irms.Data.Read.Contact.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetContactsCountHandler : IRequestHandler<GetContactsCount, long>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetContactsCountHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// get contact count
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<long> Handle(GetContactsCount request, CancellationToken token)
        {
            var total = await _context.Contact
                .Where(t => t.TenantId == _tenant.Id && t.IsGuest == request.IsGuest)
                .LongCountAsync(token);

            return total;
        }
    }
}
