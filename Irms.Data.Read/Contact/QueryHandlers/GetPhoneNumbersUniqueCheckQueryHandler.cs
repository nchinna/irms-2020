﻿using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Data.Read.Contact.ReadModels.PhoneNumbersUniqueListReadModel;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetPhoneNumbersUniqueCheckQueryHandler : IRequestHandler<GetPhoneNumberUniqueCheck, PhoneNumbersUniqueListReadModel>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _repo;
        private readonly IMediator _mediator;

        public GetPhoneNumbersUniqueCheckQueryHandler(
            TenantBasicInfo tenant,
            IContactRepository<Domain.Entities.Contact, Guid> repo,
            IMediator mediator
        )
        {
            _tenant = tenant;
            _repo = repo;
            _mediator = mediator;
        }

        public async Task<PhoneNumbersUniqueListReadModel> Handle(GetPhoneNumberUniqueCheck request, CancellationToken token)
        {
            var data = await _repo.GetExistingPhoneNumbers(request.PhoneNumbers, token);
            var result = new List<PhoneNumberUniqueReadModel>();

            request.PhoneNumbers
                .ToList()
                .ForEach(x =>
                {
                    if (data.Contains(x))
                    {
                        result.Add(new PhoneNumberUniqueReadModel
                        {
                            PhoneNumber = x,
                            IsExists = true
                        });
                    }
                    else
                    {
                        result.Add(new PhoneNumberUniqueReadModel
                        {
                            PhoneNumber = x,
                            IsExists = false
                        });
                    }
                });

            return new PhoneNumbersUniqueListReadModel { Data = result };
        }
    }
}
