﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    [HasSqlQuery(ListsQuery)]
    public class GetEventGuestListsHandler : IRequestHandler<GetEventGuestListsQuery, IEnumerable<EventGuestReadModel>>
    {
        private const string ListsQuery = @"
            SELECT
	            cl.Id,
	            cl.Name AS Value
	            FROM contactlist cl 
	            WHERE cl.EventId=@eventId
                AND cl.TenantId=@tenantId
	            AND cl.Id NOT IN (
		            SELECT
		            cl.Id
		            FROM contactlist cl 
		            INNER JOIN EventCampaign ec on ec.GuestListId=cl.id
		            WHERE cl.EventId=@eventId
		            AND ec.Active=1
                    AND cl.TenantId=@tenantId
	            )";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetEventGuestListsHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _tenant = tenant;
            _connectionString = connectionString;
        }

        /// <summary>
        /// Fetches all event guest lists that are not in use
        /// </summary>
        /// <param name="request">event id</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EventGuestReadModel>> Handle(GetEventGuestListsQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                eventId = request.EventId,
                tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = await connection.QueryAndLog<EventGuestReadModel>(ListsQuery, parameters);
                return data;
            }
        }
    }
}
