﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactItemAndListId
    {
        public ContactItem Item { get; set; }
        public Guid ContactListId { get; set; }
        public ContactItemAndListId()
        {
            Item = new ContactItem();
        }
    }
}
