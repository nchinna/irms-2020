﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactWithCustomFields
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public Gender? Gender { get; set; }

        public string Email { get; set; }
        public string AlternativeEmail { get; set; }

        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }

        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public Guid? IssuingCountryId { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }

        public IEnumerable<ContactCustomField> CustomFields { get; set; }

        public class ContactCustomField
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Value { get; set; }
        }
    }
}
