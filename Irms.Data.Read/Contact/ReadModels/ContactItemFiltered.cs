﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactItemFiltered
    {
        public Guid Id { get; set;}
        public string Name { get; set;}
        public int Count { get; set; }
    }
}
