﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class PhoneNumbersUniqueListReadModel
    {
        public IEnumerable<PhoneNumberUniqueReadModel> Data { get; set; }

        public class PhoneNumberUniqueReadModel
        {
            public string PhoneNumber { get; set; }
            public bool IsExists { get; set; }
        }
    }
}
