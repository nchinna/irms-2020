﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class EventGuestReadModel
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
    }
}
