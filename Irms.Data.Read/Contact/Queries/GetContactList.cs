﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactList : IRequest<IReadOnlyCollection<ContactListItem>>
    {
        public GetContactList(bool isGuest, bool? isGlobal)
        {
            IsGuest = isGuest;
            IsGlobal = isGlobal;
        }

        public bool IsGuest { get; }
        public bool? IsGlobal { get; }
    }
}
