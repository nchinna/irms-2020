﻿using System;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.ReadModels;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactListFiltered : IPageQuery<ContactListItemFiltered>
    {
        public GetContactListFiltered(
         int pageNo,
         int pageSize,
         string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public bool? IsGlobal { get; set; }
        public bool IsGuest { get; set; }
        public Guid? EventId { get; set; }
    }
}
