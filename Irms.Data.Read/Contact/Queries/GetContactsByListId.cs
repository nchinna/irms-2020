﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactsByListId : IRequest<IReadOnlyCollection<ContactItemAndListId>>
    {
        public GetContactsByListId(List<Guid> listId, bool loadUnassigned)
        {
            ListId = listId;
            LoadUnassigned = loadUnassigned;
        }
        public List<Guid> ListId { get; }
        public bool LoadUnassigned { get; }
    }
}
