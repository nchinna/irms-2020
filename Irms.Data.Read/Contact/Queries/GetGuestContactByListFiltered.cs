﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.ReadModels;
using System;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetGuestContactByListFiltered : IPageQuery<GuestContactItemFiltered>
    {
        public GetGuestContactByListFiltered(int pageNo,
         int pageSize,
         string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }
        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public bool IsGuest { get; set; }
        public Guid ListId { get; set; }
        public Guid EventId { get; set; }
    }
}
