﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetEmailsUniqueCheck : IRequest<EmailsUniqueListReadModel>
    {
        public IEnumerable<string> Emails { get; set; }
    }
}
