﻿namespace Irms.Data.Read.Tenant.ReadModels
{
    public class TenantLogo
    {
        public TenantLogo(string logoPath)
        {
            LogoPath = logoPath;
        }

        public string LogoPath { get; }
    }
}
