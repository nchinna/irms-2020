﻿using System;

namespace Irms.Data.Read.Tenant.ReadModels
{
    public class TenantConfigInfo
    {
        public Guid Id { get; set; }
        public bool? HasOwnDomain { get; set; }
        public string ClientUrl { get; set; }
        public string Host { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string EmailFrom { get; set; }
        public string Scheme { get; set; }
        public string SubDomain { get; set; }
    }
}
