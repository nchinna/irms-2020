﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Abstract;
using Irms.Data.Read.Tenant.Queries;
using Microsoft.Data.SqlClient;
using Irms.Application;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    [HasSqlQuery(nameof(Query), nameof(_dummyParams))]
    public class GetTenantByUrlHandler : IRequestHandler<GetTenantByUrl, TenantBasicInfo>
    {
        private const string Query = @"
        SELECT
          t.Id,
          t.Name,
          t.ClientURL,
          t.EmailFrom,
          t.IsBtoC
        FROM
          [Tenant] t
        WHERE t.ClientUrl = @clientUrl
          AND t.IsDeleted = 0
          AND t.IsActive = 1";

        private readonly object _dummyParams = new
        {
            ClientURL = "test.url.net"
        };

        private readonly IConnectionString _connectionString;

        public GetTenantByUrlHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<TenantBasicInfo> Handle(GetTenantByUrl request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                return await db.QueryFirstOrDefaultAndLog<TenantBasicInfo>(Query, new { clientUrl = request.TenantUrl });
            }
        }
    }
}
