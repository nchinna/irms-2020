﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Irms.Data.Read.Dictionary.ReadModels;
using Microsoft.Extensions.Configuration;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    public class GetTenantContactInfoHandler : IRequestHandler<GetTenantContactInfo, TenantContactInfo>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        public GetTenantContactInfoHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// get tenant contact info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TenantContactInfo> Handle(GetTenantContactInfo request, CancellationToken token)
        {
            var info = await _context.TenantContactInfo
                .FirstOrDefaultAsync(x => x.TenantId == request.Id, token);

            var contactInfo = _mapper.Map<TenantContactInfo>(info);
            return contactInfo;
        }
    }
}
