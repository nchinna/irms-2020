﻿using Irms.Application;
using MediatR;

namespace Irms.Data.Read.Tenant.Queries
{
    public class GetTenantByUrl : IRequest<TenantBasicInfo>
    {
        public GetTenantByUrl(string tenantUrl)
        {
            TenantUrl = tenantUrl;
        }

        public string TenantUrl { get; }
    }
}
