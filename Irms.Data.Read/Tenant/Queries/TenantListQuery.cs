﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Tenant.ReadModels;
using Newtonsoft.Json;

namespace Irms.Data.Read.Tenant.Queries
{
    public class TenantListQuery : IPageTimeQuery<TenantListItem>
    {
        [JsonConstructor]
        public TenantListQuery(int pageNo, int pageSize, TimeFilterRange timeFilter, string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            TimeFilter = timeFilter;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public TimeFilterRange TimeFilter { get; }

        public string SearchText { get; }
    }
}
