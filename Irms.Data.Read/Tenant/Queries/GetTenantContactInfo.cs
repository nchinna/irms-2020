﻿using System;
using MediatR;
using Irms.Data.Read.Tenant.ReadModels;

namespace Irms.Data.Read.Tenant.Queries
{
    public class GetTenantContactInfo : IRequest<TenantContactInfo>
    {
        public GetTenantContactInfo(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
