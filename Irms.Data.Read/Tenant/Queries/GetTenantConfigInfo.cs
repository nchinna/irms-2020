﻿using System;
using MediatR;
using Irms.Data.Read.Tenant.ReadModels;

namespace Irms.Data.Read.Tenant.Queries
{
    public class GetTenantConfigInfo : IRequest<TenantConfigInfo>
    {
        public GetTenantConfigInfo(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }

}
