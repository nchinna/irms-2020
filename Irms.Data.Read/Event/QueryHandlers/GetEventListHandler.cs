﻿using Dapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Product.Commands;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Event.Queries;
using Irms.Data.Read.Event.ReadModels;
using Irms.Data.Read.Registration;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetEventListHandler : IPageHandler<GetEventList, EventListItem>
    {
        private const string CountQuery = @"
                SELECT
                  COUNT(e.Id)
                FROM Event e
                INNER JOIN EventLocation el ON e.Id = el.EventId
                WHERE e.IsActive = 1 
                    AND e.TenantId = @tenant
                    AND e.IsDeleted = 0
                {0}";

        private const string PageQuery = @"
                SELECT
                  e.Id,
                  e.Name,
                  e.StartDateTime,
                  e.EndDateTime,
                  @path + e.ImagePath AS ImagePath,
                  e.EventTypeId
                FROM Event e
                INNER JOIN EventLocation el ON e.Id = el.EventId
                WHERE e.IsActive = 1 
                    AND e.TenantId = @tenant
                    AND e.IsDeleted = 0
                {0}
                ORDER BY e.CreatedOn DESC
                OFFSET @skip ROWS
                FETCH NEXT @take ROWS ONLY";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _user;
        private readonly IConfiguration _config;


        private const string SearchClause = @"
            AND e.Name like @searchText
            OR el.City like @searchText";

        public GetEventListHandler(IConnectionString connectionString, 
            TenantBasicInfo tenant,
            ICurrentUser user,
            IConfiguration config)
        {
            _connectionString = connectionString;
            _tenant = tenant;
            _user = user;
            _config = config;
        }

        /// <summary>
        /// this method will return list of events with pagination
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<EventListItem>> Handle(GetEventList request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            if (request.Filters != null && request.Filters.Count > 0)
            {
                clauses.Add(GetFilterQuery(request.Filters));
            }

            if (_tenant.IsBtoC)
            {
                clauses.Add(FullTextHelpers.MakeCreatedByClause("e.CreatedById"));
            }

            var parameters = new
            {
                tenant = _tenant.Id,
                createdBy = _user.Id,
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                skip = request.Skip(),
                take = request.Take(),
                date = DateTime.UtcNow,
                path = _config["AzureStorage:BaseUrl"],
            };

            var conditionStr = string.Join("\r\n", clauses);
            var countQuery = string.Format(CountQuery, conditionStr);
            var pageQuery = string.Format(PageQuery, conditionStr);

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var events = await connection.QueryAndLog<EventListItem>(pageQuery, parameters);
                return new PageResult<EventListItem>(events, total);
            }
        }

        private string GetFilterQuery(List<PeriodCheckBoxRangeFilter> filters)
        {
            if (filters.Count == Enum.GetNames(typeof(PeriodCheckBoxRangeFilter)).Length)
            {
                return string.Empty;
            }

            StringBuilder filterQuery = new StringBuilder();
            filterQuery.Append("AND (");

            for (var i = 0; i < filters.Count; i++)
            {
                if (i > 0)
                {
                    filterQuery.Append(" OR ");
                }

                if (filters[i] == PeriodCheckBoxRangeFilter.Upcoming)
                {
                    filterQuery.Append("e.StartDateTime > @date");
                }
                else if (filters[i] == PeriodCheckBoxRangeFilter.Live)
                {
                    filterQuery.Append("(e.StartDateTime <= @date AND e.EndDateTime >= @date)");
                }
                else if (filters[i] == PeriodCheckBoxRangeFilter.Past)
                {
                    filterQuery.Append("e.EndDateTime < @date");
                }
            }

            filterQuery.Append(")");
            return filterQuery.ToString();
        }
    }
}
