﻿using Irms.Data.Read.Service.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Service.Queries
{
    public class GetAvailableServicesCmd : IRequest<IEnumerable<ServicesList>>
    {
    }
}
