﻿using Irms.Application.Product.Commands;
using Irms.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Irms.Data.Read.Service.ReadModels
{
    public class ServicesList
    {
        public ServicesList() { }
        public ServicesList(Guid id, bool? isActive, string title)
        {
            Id = id;
            IsActive = isActive ?? false;
            Title = title;
        }

        public ServicesList(Guid id, bool? isActive, string title, List<ServiceFeature> serviceFeatures) : this(id, isActive, title)
        {
            ServiceFeatures = serviceFeatures;
        }

        public ServicesList(Guid id, bool? isActive, string title, IEnumerable<ServiceFeature> serviceFeatures) : this(id, isActive, title)
        {
            ServiceFeatures = serviceFeatures.ToList();
        }

        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public List<ServiceFeature> ServiceFeatures { get; set; } = new List<ServiceFeature>();
    }
}
