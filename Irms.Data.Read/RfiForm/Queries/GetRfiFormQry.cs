﻿using MediatR;
using System;

namespace Irms.Data.Read.RfiForm.Queries
{
    public class GetRfiFormQry : IRequest<ReadModels.RfiForm>
    {
        public GetRfiFormQry(Guid id)
        {
            CampaignInvitationId = id;
        }

        public Guid CampaignInvitationId { get; }
    }
}
