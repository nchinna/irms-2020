﻿namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class CampaignCriteriaTypeListItem
    {
        public CampaignCriteriaTypeListItem(byte id, string value)
        {
            Id = id;
            Value = value;
        }

        public byte Id { get; }
        public string Value { get; }
    }
}
