﻿namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class EnumItem
    {
        public EnumItem(int id, string value)
        {
            Id = id;
            Value = value;
        }

        public int Id { get; }
        public string Value { get; }
    }
}
