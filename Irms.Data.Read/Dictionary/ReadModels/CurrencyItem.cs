﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class CurrencyItem
    {
        public CurrencyItem(byte id, string title)
        {
            Id = id;
            Title = title;
        }

        public byte Id { get; }
        public string Title { get; }
    }
}
