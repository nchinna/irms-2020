﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetDocumentTypeHandler : IRequestHandler<GetDocumentType, IEnumerable<DocumentTypeItem>>
    {
        private const string Query = @"
        SELECT
            Id,
            Name
        FROM DocumentType n
            ORDER BY id";

        private readonly IConnectionString _connectionString;

        public GetDocumentTypeHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// get countries list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DocumentTypeItem>> Handle(GetDocumentType request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                return await db.QueryAndLog<DocumentTypeItem>(Query);
            }
        }
    }
}
