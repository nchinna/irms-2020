﻿using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class CampaignCriteriaTypeHandler : IRequestHandler<GetCampaignCriteriaType, IEnumerable<CampaignCriteriaTypeListItem>>
    {
        private const string Query = @"
        SELECT [Id]
              ,[Title] as Value
        FROM [dbo].[CampaignEntryCriteriaType]
        ORDER BY ID";

        private readonly IConnectionString _connectionString;

        public CampaignCriteriaTypeHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<IEnumerable<CampaignCriteriaTypeListItem>> Handle(GetCampaignCriteriaType request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CampaignCriteriaTypeListItem>(Query);
                return list;
            }
        }
    }
}
