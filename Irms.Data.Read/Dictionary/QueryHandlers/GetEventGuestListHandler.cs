﻿using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetEventGuestListHandler : IRequestHandler<GetEventGuestList, IEnumerable<DictionaryItem>>
    {
        private const string Query = @"
        SELECT [Id],
              [Name] as Value
        FROM [dbo].[ContactList]
        WHERE EventId = @eventId
        ORDER BY Name";

        private readonly IConnectionString _connectionString;

        public GetEventGuestListHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<IEnumerable<DictionaryItem>> Handle(GetEventGuestList request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<DictionaryItem>(Query, new { eventId = request.EventId });
                return list;
            }
        }
    }
}
