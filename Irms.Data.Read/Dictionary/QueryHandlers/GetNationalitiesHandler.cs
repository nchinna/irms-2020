﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetNationalitiesHandler : IRequestHandler<GetNationalities, IEnumerable<NationalityItem>>
    {
        private const string Query = @"
        SELECT
            Id,
            Nationality
        FROM Country n
        Where Nationality is not Null
        ORDER BY Nationality";

        private readonly IConnectionString _connectionString;
        private readonly ICurrentLanguage _currentLanguage;

        public GetNationalitiesHandler(IConnectionString connectionString, ICurrentLanguage currentLanguage)
        {
            _connectionString = connectionString;
            _currentLanguage = currentLanguage;
        }

        /// <summary>
        /// get countries list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<NationalityItem>> Handle(GetNationalities request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                culture = _currentLanguage.GetRequestCulture()?.Name ?? "en-US"
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<NationalityItem>(Query, parameters);
                return list;
            }
        }
    }
}
