﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetTenantsHandler : IRequestHandler<GetTenants, IEnumerable<DictionaryItem>>
    {
        private const string Query = @"
                    SELECT Id,
                           Name AS Value
                    FROM Tenant
                    WHERE IsActive=1
                    AND Id <> @tenant ";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetTenantsHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// get tenants list for dropdown
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DictionaryItem>> Handle(GetTenants request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                tenant = _tenant.Id,
            };
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<DictionaryItem>(Query, parameters);
                return list;
            }
        }
    }
}