﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class CampaignInvitationResponseRepositoryTest
    {
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();                    
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var campaignInvitationResponse = new Data.EntityClasses.CampaignInvitationResponse
            {
                Id = Guid.NewGuid(),
                Answer = 1,
                CampaignInvitationId = Guid.NewGuid(),
                ContactId = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow,
                ResponseDate = DateTime.UtcNow,
                ResponseMediaType = 0,
                TenantId = Guid.NewGuid()
            };

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignInvitationResponse.AddRangeAsync(campaignInvitationResponse);
                await db.SaveChangesAsync();
            }

            //act
            CampaignInvitationResponse result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetById(campaignInvitationResponse.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(1, campaignInvitationResponse.Answer);
            Assert.AreEqual(result.CampaignInvitationId, campaignInvitationResponse.CampaignInvitationId);
            Assert.AreEqual(result.ContactId, campaignInvitationResponse.ContactId);
            Assert.AreEqual(result.CreatedOn, campaignInvitationResponse.CreatedOn);
            Assert.AreEqual(result.ResponseDate, campaignInvitationResponse.ResponseDate);
            Assert.AreEqual(0, campaignInvitationResponse.ResponseMediaType);
            Assert.AreEqual(result.TenantId, campaignInvitationResponse.TenantId);
        }

        [TestMethod]
        public async Task CanLoadQuestions()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();
            using(var db = new IrmsDataContext(options))
            {
                db.RfiFormQuestion.Add(new Data.EntityClasses.RfiFormQuestion
                {
                    Id = id
                });
                await db.SaveChangesAsync();
            }
            //act
            IEnumerable<RfiFormQuestion> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.LoadQuestions(new List<Guid> { id }, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.First().Id);
        }

        [TestMethod]
        public async Task CanSaveRfiAnswers()
        { 
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
                cfg.CreateMap<Data.EntityClasses.RfiFormResponse, RfiFormResponse>();
                cfg.CreateMap<RfiFormResponse, Data.EntityClasses.RfiFormResponse>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            //act
            List<Data.EntityClasses.RfiFormResponse> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                await sut.SaveRfiAnswers(new List<RfiFormResponse> { new RfiFormResponse
                {
                    Id = id
                } }, CancellationToken.None);
                await db.SaveChangesAsync();
                result = await db.RfiFormResponse.ToListAsync();
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public async Task CanCreateRange()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
                cfg.CreateMap<Data.EntityClasses.RfiFormResponse, RfiFormResponse>();
                cfg.CreateMap<RfiFormResponse, Data.EntityClasses.RfiFormResponse>();
                cfg.CreateMap<Data.EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();
                cfg.CreateMap<CampaignInvitationResponse, Data.EntityClasses.CampaignInvitationResponse>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();
            //act
            List<Data.EntityClasses.CampaignInvitationResponse> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                await sut.CreateRange(new List<CampaignInvitationResponse> { new CampaignInvitationResponse
                {
                    Id = id
                } }, CancellationToken.None);
                await db.SaveChangesAsync();
                result = await db.CampaignInvitationResponse.ToListAsync();
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public async Task LoadRfiFormIdByResponseId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
                cfg.CreateMap<Data.EntityClasses.RfiFormResponse, RfiFormResponse>();
                cfg.CreateMap<RfiFormResponse, Data.EntityClasses.RfiFormResponse>();
                cfg.CreateMap<Data.EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();
                cfg.CreateMap<CampaignInvitationResponse, Data.EntityClasses.CampaignInvitationResponse>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            using (var db = new IrmsDataContext(options))
            {
                db.CampaignInvitationResponse.Add(new Data.EntityClasses.CampaignInvitationResponse
                {
                    CampaignInvitation = new Data.EntityClasses.CampaignInvitation
                    {
                        RfiForm = new List<Data.EntityClasses.RfiForm>()
                        {
                         new Data.EntityClasses.RfiForm
                         {
                             Id = id
                         }
                        }
                    },
                    Id = id
                });
                await db.SaveChangesAsync();
            }

            //act
            Guid result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.LoadRfiFormIdByResponseId(id, CancellationToken.None);
                await db.SaveChangesAsync();
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(id, result);


        }
        [TestMethod]
        public async Task GetContactListByInvitationId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
                cfg.CreateMap<Data.EntityClasses.RfiFormResponse, RfiFormResponse>();
                cfg.CreateMap<RfiFormResponse, Data.EntityClasses.RfiFormResponse>();
                cfg.CreateMap<Data.EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();
                cfg.CreateMap<Data.EntityClasses.ContactListToContacts, ContactListToContact>();
                cfg.CreateMap<CampaignInvitationResponse, Data.EntityClasses.CampaignInvitationResponse>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            using (var db = new IrmsDataContext(options))
            {
                db.ContactList.Add(new Data.EntityClasses.ContactList
                {
                    Id = id,
                    ContactListToContacts = new List<Data.EntityClasses.ContactListToContacts>
                    {
                        new Data.EntityClasses.ContactListToContacts
                        {
                            Id = id,
                            Contact = new Data.EntityClasses.Contact
                            {
                                Id = id
                            }
                        }
                    }
                });

                db.CampaignInvitation.Add(new Data.EntityClasses.CampaignInvitation
                {
                    Id = id,
                    EventCampaign = new Data.EntityClasses.EventCampaign
                    {
                        Id = id,
                        GuestListId = id
                    }
                });

                await db.SaveChangesAsync();
            }

            //act
            IEnumerable<ContactListToContact> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetContactListsByInvitationId(id, CancellationToken.None);
                await db.SaveChangesAsync();
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public async Task CanUpdate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiFormQuestion, RfiFormQuestion>();
                cfg.CreateMap<Data.EntityClasses.RfiFormResponse, RfiFormResponse>();
                cfg.CreateMap<RfiFormResponse, Data.EntityClasses.RfiFormResponse>();
                cfg.CreateMap<Data.EntityClasses.CampaignInvitationResponse, CampaignInvitationResponse>();
                cfg.CreateMap<CampaignInvitationResponse, Data.EntityClasses.CampaignInvitationResponse>();
            });

            var mapper = config.CreateMapper();

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var data = new Data.EntityClasses.CampaignInvitationResponse
            {
                Id = id,
                Answer = 0
            };

            using (var db = new IrmsDataContext(options))
            {
                db.CampaignInvitationResponse.Add(data);
                await db.SaveChangesAsync();
            }


            //act
            Data.EntityClasses.CampaignInvitationResponse result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationResponseRepository(db, mapper, currentUser.Object, tenant);
                data.Answer = 1;
                await sut.Update(mapper.Map<CampaignInvitationResponse>(data), CancellationToken.None);
                await db.SaveChangesAsync();
                result = await db.CampaignInvitationResponse.FirstOrDefaultAsync(x => x.Id == id);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Answer);
        }
    }

}
