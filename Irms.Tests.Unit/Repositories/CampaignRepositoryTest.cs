﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Tests.Unit.ValidData;
using Irms.Application;
using System.Linq;
using System.Collections.Generic;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class CampaignRepositoryTest
    {
        /// <summary>
        /// can get campaign by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.EventCampaign, Campaign>()
                    .ForMember(x => x.CampaignStatus, x => x.MapFrom(m => (CampaignStatus)m.CampaignStatus));
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var campaign = new CampaignBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            campaign.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.EventCampaign.AddRangeAsync(campaign);
                await db.SaveChangesAsync();
            }

            //act
            Campaign result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetById(campaign.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(result.Name, "Campaign1");
            Assert.AreEqual(result.CampaignStatus, CampaignStatus.Draft);
        }

        /// <summary>
        /// unit test to get tenant admin
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetAdminTenant()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.EventCampaign, Campaign>()
                    .ForMember(x => x.CampaignStatus, x => x.MapFrom(m => (CampaignStatus)m.CampaignStatus));
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test
            var user = new UserInfoBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            user.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                await db.UserInfo.AddRangeAsync(user);
                await db.SaveChangesAsync();
            }

            //act
            (string name, string email) result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetTenantAdmin(CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.name);
            Assert.IsNotNull(result.email);
            Assert.AreEqual(result.name, user.FullName);
            Assert.AreEqual(result.email, user.Email);
        }

        /// <summary>
        /// can create campaign with location
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaign()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Campaign, Data.EntityClasses.EventCampaign>()
                .ForMember(x => x.CampaignStatus, x => x.MapFrom(m => (byte)m.CampaignStatus));
            });

            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var campaignBuilder = new CampaignBuilder();
            var campaignToCreate = campaignBuilder.Build();
            campaignToCreate.Id = id;
            campaignToCreate.TenantId = tenant.Id;

            //act
            Guid resultId;
            Data.EntityClasses.EventCampaign result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.Create(campaignToCreate, CancellationToken.None);
                result = await db.EventCampaign.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            //campaign
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Name, "Campaign1");
            Assert.AreEqual(result.CampaignStatus, 0);
        }

        /// <summary>
        /// can update campaign basic info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaignInfo()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var campaignEntity = new CampaignBuilder().BuildEntity();
            campaignEntity.Id = id;
            campaignEntity.TenantId = tenant.Id;

            var campaignToUpdate = new CampaignBuilder().Build();
            campaignToUpdate.Id = id;
            campaignToUpdate.TenantId = tenant.Id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.EventCampaign.AddRangeAsync(campaignEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.EventCampaign result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Update(campaignToUpdate, CancellationToken.None);
                result = await db.EventCampaign.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "Campaign1");
            Assert.AreEqual(result.CampaignStatus, 0);
            Assert.AreEqual(result.Deleted, false);
        }

        /// <summary>
        /// can delete campaign by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteCampaignById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var campaignEntity = new CampaignBuilder().BuildEntity();

            using (var db = new IrmsDataContext(options))
            {
                await db.EventCampaign.AddRangeAsync(
                    campaignEntity);
                await db.SaveChangesAsync();
            }

            Data.EntityClasses.EventCampaign result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Delete(campaignEntity.Id, CancellationToken.None);
                result = await db.EventCampaign.FirstOrDefaultAsync(p => p.Deleted == true && p.Id == campaignEntity.Id, CancellationToken.None);
            }

            //assert
            Assert.AreEqual(result.Id, campaignEntity.Id);
            Assert.AreEqual(result.Name, "Campaign1");
            Assert.AreEqual(result.CampaignStatus, 0);
            Assert.AreEqual(result.Deleted, true);
        }
    }
}
