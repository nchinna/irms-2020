﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Data;
using Irms.Data.Repositories.Templates;
using Irms.Domain.Entities.Templates;
using Irms.Tests.Unit.DataBuilder.Template;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.Repositories.Templates
{
    [TestClass]
    public class TemplateSelectionRepositoryTest
    {
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateSelectionId = Guid.NewGuid();

            var templateSelectionEntity = new EntitiesValidData().TemplateSelectionEntityValidData();
            templateSelectionEntity.Id = templateSelectionId;
            templateSelectionEntity.TenantId = tenant.Id;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.TemplateSelection.AddRangeAsync(
                    templateSelectionEntity);

                await db.SaveChangesAsync();
            }

            //act
            TemplateSelection result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateSelectionRepository(db, mapper.Object, tenant);
                result = await sut.GetById(templateSelectionId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, templateSelectionId);
            Assert.AreEqual(result.IsEnabled, true);
            Assert.AreEqual(result.Type, TemplateType.ActivationMessage);
            Assert.IsNotNull(result.EmailTemplate);
            Assert.IsNotNull(result.SmsTemplate);
        }

        [TestMethod]
        public async Task CanGetById_Null()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateSelectionId = Guid.NewGuid();

            var templateSelectionEntity = new EntitiesValidData().TemplateSelectionEntityValidData();
            templateSelectionEntity.Id = templateSelectionId;
            templateSelectionEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.TemplateSelection.AddRangeAsync(
                    templateSelectionEntity);

                await db.SaveChangesAsync();
            }

            //act
            TemplateSelection result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateSelectionRepository(db, mapper.Object, tenant);
                result = await sut.GetById(Guid.NewGuid(), CancellationToken.None);
            }

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task CanUpdate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateSelectionId = Guid.NewGuid();

            var templateSelectionEntity = new EntitiesValidData().TemplateSelectionEntityValidData();
            templateSelectionEntity.Id = templateSelectionId;
            templateSelectionEntity.TenantId = tenant.Id;
            var templateSelectionBuilder = new TemplateSelectionBuilder
            {
                Id = templateSelectionId,
                Type = TemplateType.ActivationMessage,
                IsEnabled = false
            };

            var templateSelectionToUpdate = templateSelectionBuilder.Build();

            mapper
                .Setup(x => x.Map<Irms.Data.EntityClasses.TemplateSelection>(It.IsAny<TemplateSelection>()));
            //.Callback((TemplateSelection i, Data.EntityClasses.TemplateSelection e) =>
            //{
            //    e.Id = templateSelectionToUpdate.Id;
            //    e.TypeId = (int)templateSelectionToUpdate.Type;
            //    e.EmailTemplateId = templateSelectionToUpdate.EmailTemplate;
            //    e.SmsTemplateId = templateSelectionToUpdate.SmsTemplate;
            //    e.IsEnabled = templateSelectionToUpdate.IsEnabled;
            //});

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.TemplateSelection.AddRangeAsync(
                    templateSelectionEntity);

                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.TemplateSelection result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateSelectionRepository(db, mapper.Object, tenant);
                await sut.Update(templateSelectionToUpdate, CancellationToken.None);
                result = await db.TemplateSelection.FirstOrDefaultAsync(x => x.Id == templateSelectionId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, templateSelectionId);
            Assert.AreEqual(result.IsEnabled, templateSelectionEntity.IsEnabled);
            Assert.AreEqual(result.TypeId, (int)TemplateType.ActivationMessage);
            Assert.IsNotNull(result.EmailTemplateId);
            Assert.IsNotNull(result.SmsTemplateId);
        }


        [TestMethod]
        public async Task CanCreate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateSelectionId = Guid.NewGuid();

            var templateSelectionEntity = new EntitiesValidData().TemplateSelectionEntityValidData();
            templateSelectionEntity.Id = templateSelectionId;
            templateSelectionEntity.TenantId = tenant.Id;


            var templateSelectionBuilder = new TemplateSelectionBuilder
            {
                Id = templateSelectionId,
                Type = TemplateType.ActivationMessage,
                IsEnabled = false
            };

            var templateSelection = templateSelectionBuilder.Build();

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.TemplateSelection.AddRangeAsync(
                    templateSelectionEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateSelectionRepository(db, mapper.Object, tenant);
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.Create(templateSelection, CancellationToken.None));
            }
        }

        [TestMethod]
        public async Task CanDelete()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateSelectionId = Guid.NewGuid();

            var templateSelectionEntity = new EntitiesValidData().TemplateSelectionEntityValidData();
            templateSelectionEntity.Id = templateSelectionId;
            templateSelectionEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.TemplateSelection.AddRangeAsync(
                    templateSelectionEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateSelectionRepository(db, mapper.Object, tenant);
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.Delete(templateSelectionId, CancellationToken.None));
            }
        }
    }
}
