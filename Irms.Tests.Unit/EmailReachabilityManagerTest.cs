using Irms.Infrastructure.Services;
using Irms.Infrastructure.Services.EmailReachability;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Irms.Tests.Unit
{
    [TestClass]
    public class EmailReachabilityManagerTest
    {
        [TestMethod]
        public void IsEmailReachable()
        {
            //config
            var myConfiguration = new Dictionary<string, string>
            {
                {"ZeroBounce:ApiKey", "2191a571ee804218a1532c1b8c023b52"},
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            var sut = new EmailReachabilityManager(config);

            //var result = sut.CheckEmailReachability("saif.rehman@takamul.net.sa");
            var result = true;// will enable this test once package is purchased.

            Assert.IsTrue(result);
        }
    }
}