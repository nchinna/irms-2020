﻿//using Irms.Application.Abstract.Repositories.Base;
//using Irms.Application.Abstract.Services;
//using Irms.Application.Abstract.Services.Notifications;
//using Irms.Application.Messages.CommandHandlers;
//using Irms.Application.Messages.Commands;
//using Irms.Application.Messages.Events;
//using Irms.Domain.Entities.Guest;
//using Irms.Infrastructure.Services;
//using Irms.Tests.Unit.DataBuilder.Guest;
//using MediatR;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Tests.Unit.CommandsHandlers.Message
//{
//    [TestClass]
//    public class SendMessageHandlerTest
//    {
//        [TestMethod]
//        public async Task CanHandle()
//        {
//            //Arrange
//            var guestRegistrationRepository = new Mock<IReadOnlyBatchRepository<GuestRegistration, Guid>>();
//            var mediator = new Mock<IMediator>();
//            var emailSender = new Mock<IEmailSender>();
//            var smsSender = new Mock<ISmsSender>();
//            var phoneValidator = new Mock<IPhoneNumberValidator>();

//            phoneValidator
//                .Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()))
//                .Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

//            var guestBuilder = new GuestBuilder();
//            Guest guest = guestBuilder;

//            var guestRegistration = new GuestRegistration(guest);

//            var guestBuilder2 = new GuestBuilder
//            {
//                ContactInfo =
//                {
//                    Email = "guest2@mail.net",
//                    PhoneNo = "+380952625245"
//                }
//            };
//            Guest guest2 = guestBuilder2;

//            var guestRegistration2 = new GuestRegistration(guest2);

//            var guestBuilder3 = new GuestBuilder
//            {
//                ContactInfo =
//                {
//                    Email = "guest3@mail.net",
//                    PhoneNo = "+380952725245"
//                }
//            };
//            Guest guest3 = guestBuilder3;

//            var guestRegistration3 = new GuestRegistration(guest3);

//            IEnumerable<GuestRegistration> guestRegistrationList =
//                new List<GuestRegistration>
//                {
//                    guestRegistration,
//                    guestRegistration2,
//                    guestRegistration3
//                };

//            EmailMessage emailMessage = null;
//            SmsMessage smsMessage = null;
//            var emailEvents = new List<EmailSent>();
//            var smsEvents = new List<SmsSent>();

//            emailSender
//                .Setup(x => x.SendEmail(It.IsAny<EmailMessage>(), CancellationToken.None))
//                .Returns((EmailMessage m, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((EmailMessage m, CancellationToken t) => emailMessage = m);

//            smsSender
//                .Setup(x => x.SendSms(It.IsAny<SmsMessage>(), CancellationToken.None))
//                .Returns((SmsMessage s, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((SmsMessage s, CancellationToken t) => smsMessage = s);

//            mediator
//                .Setup(x => x.Publish(It.IsAny<EmailSent>(), CancellationToken.None))
//                .Returns((EmailSent q, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((EmailSent q, CancellationToken t) => emailEvents.Add(q));

//            mediator
//                .Setup(x => x.Publish(It.IsAny<SmsSent>(), CancellationToken.None))
//                .Returns((SmsSent q, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((SmsSent q, CancellationToken t) => smsEvents.Add(q));

//            guestRegistrationRepository
//                  .Setup(x => x.GetByIds(It.IsAny<IEnumerable<Guid>>(), CancellationToken.None))
//                  .Returns((IEnumerable<Guid> i, CancellationToken t) => Task.FromResult(guestRegistrationList));

//            var sut = new SendMessageHandler(guestRegistrationRepository.Object, emailSender.Object, smsSender.Object, mediator.Object, phoneValidator.Object);
//            var cmd = new SendMessageCmd(
//                new[]
//                {
//                    Guid.NewGuid(),
//                    Guid.NewGuid(),
//                    Guid.NewGuid()
//                },
//                true,
//                true,
//                "Subject",
//                "email body",
//                false,
//                new string[]
//                {
//                },
//                new string[]
//                {
//                });

//            //Act
//            await sut.Handle(
//                cmd,
//                CancellationToken.None);

//            //Assert
//            Assert.AreEqual(emailMessage.Recipients.FirstOrDefault().EmailAddress, "guest@mail.com");
//            Assert.AreEqual(emailMessage.Recipients.Count(), 3);
//            Assert.AreEqual(smsMessage.Recipients.FirstOrDefault().Phone, "+380982526252");
//            Assert.AreEqual(smsMessage.Recipients.Count(), 3);
//            Assert.AreEqual(emailEvents.Count, 3);
//            Assert.AreEqual(smsEvents.Count, 3);
//        }

//        [TestMethod]
//        public async Task CanHandle_TestMode()
//        {
//            //Arrange
//            var guestRegistrationRepository = new Mock<IReadOnlyBatchRepository<GuestRegistration, Guid>>();
//            var mediator = new Mock<IMediator>();
//            var emailSender = new Mock<IEmailSender>();
//            var smsSender = new Mock<ISmsSender>();
//            var phoneValidator = new Mock<IPhoneNumberValidator>();

//            phoneValidator
//                .Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()))
//                .Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

//            var guestBuilder = new GuestBuilder();
//            Guest guest = guestBuilder;

//            var guestRegistration = new GuestRegistration(guest);

//            var guestBuilder2 = new GuestBuilder
//            {
//                ContactInfo =
//                {
//                    Email = "guest2@mail.net",
//                    PhoneNo = "+380952625245"
//                }
//            };
//            Guest guest2 = guestBuilder2;

//            var guestRegistration2 = new GuestRegistration(guest2);

//            var guestBuilder3 = new GuestBuilder
//            {
//                ContactInfo =
//                {
//                    Email = "guest3@mail.net",
//                    PhoneNo = "+380952725245"
//                }
//            };
//            Guest guest3 = guestBuilder3;

//            var guestRegistration3 = new GuestRegistration(guest3);

//            IEnumerable<GuestRegistration> guestRegistrationList =
//                new List<GuestRegistration>
//                {
//                    guestRegistration,
//                    guestRegistration2,
//                    guestRegistration3
//                };

//            EmailMessage emailMessage = null;
//            SmsMessage smsMessage = null;
//            var emailEvents = new List<EmailSent>();
//            var smsEvents = new List<SmsSent>();

//            emailSender
//                .Setup(x => x.SendEmail(It.IsAny<EmailMessage>(), CancellationToken.None))
//                .Returns((EmailMessage m, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((EmailMessage m, CancellationToken t) => emailMessage = m);

//            smsSender
//                .Setup(x => x.SendSms(It.IsAny<SmsMessage>(), CancellationToken.None))
//                .Returns((SmsMessage s, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((SmsMessage s, CancellationToken t) => smsMessage = s);

//            mediator
//                .Setup(x => x.Publish(It.IsAny<EmailSent>(), CancellationToken.None))
//                .Returns((EmailSent q, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((EmailSent q, CancellationToken t) => emailEvents.Add(q));

//            mediator
//                .Setup(x => x.Publish(It.IsAny<SmsSent>(), CancellationToken.None))
//                .Returns((SmsSent q, CancellationToken t) => MediatR.Unit.Task)
//                .Callback((SmsSent q, CancellationToken t) => smsEvents.Add(q));

//            guestRegistrationRepository
//                  .Setup(x => x.GetByIds(It.IsAny<IEnumerable<Guid>>(), CancellationToken.None))
//                  .Returns((IEnumerable<Guid> i, CancellationToken t) => Task.FromResult(guestRegistrationList));

//            var sut = new SendMessageHandler(guestRegistrationRepository.Object, emailSender.Object, smsSender.Object, mediator.Object, phoneValidator.Object);
//            var cmd = new SendMessageCmd(
//                new[]
//                {
//                    Guid.NewGuid(),
//                    Guid.NewGuid(),
//                    Guid.NewGuid()
//                },
//                true,
//                true,
//                "Subject",
//                "email body",
//                true,
//                new[]
//                {
//                    "guesttest@mail.net",
//                    "guesttest2@mail.net",
//                    "guesttest3@mail.net"
//                },
//                new[]
//                {
//                    "+380972568685",
//                    "+380972588685",
//                    "+380972578685"
//                });

//            //Act
//            await sut.Handle(
//                cmd,
//                CancellationToken.None);

//            //Assert
//            Assert.AreEqual(emailMessage.Recipients.FirstOrDefault().EmailAddress, "guesttest@mail.net");
//            Assert.AreEqual(emailMessage.Recipients.Count(), 3);
//            Assert.AreEqual(smsMessage.Recipients.FirstOrDefault().Phone, "+380972568685");
//            Assert.AreEqual(smsMessage.Recipients.Count(), 3);
//            Assert.AreEqual(emailEvents.Count, 3);
//            Assert.AreEqual(smsEvents.Count, 3);
//        }
//    }
//}
