﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateOrUpdateCampaignEmailTemplateHandlerTest
    {
        /// <summary>
        /// unit test to create/update campaign email template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateOrUpdateCampaignEmailTemplateHandler()
        {
            var mediator = new Mock<IMediator>();
            var repo = new Mock<ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid>>();
            var mapper = new Mock<IMapper>();
            var config = new Mock<IConfiguration>();

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignEmailTemplateCreatedOrUpdated>(), CancellationToken.None))
                .Returns((CampaignEmailTemplateCreatedOrUpdated q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new CampaignEmailTemplateBuilder().Build();

            mapper
                .Setup(x => x.Map<Domain.Entities.CampaignEmailTemplate>(It.IsAny<CreateOrUpdateCampaignEmailTemplateCmd>()))
                .Returns(expectedResult);

            config
                .SetupGet(x => x[It.Is<string>(s => s == "AzureStorage:BaseUrl")])
                .Returns(string.Empty);

            var prodHandler = new CreateOrUpdateCampaignEmailTemplateHandler(repo.Object, mediator.Object, mapper.Object, config.Object);
            var cmd = new CreateOrUpdateCampaignEmailTemplateCmd()
            {
                Id = Guid.NewGuid(),
                Subject = "TestSubject",
                Preheader = "PreHeader",
                SenderEmail = "sender@email.com",
                Body = "template_body",
                PlainBody = "plain_template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            Assert.AreEqual(id, expectedResult.Id);
        }
    }
}
