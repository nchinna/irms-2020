﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    [TestClass]
    public class DeleteInvitationHandlerTest
    {
        /// <summary>
        /// unit test that checks if it can delete invitation, ensures that result is not null and result id equals input id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteInvitation()
        {
            //Arrange
            var id = Guid.NewGuid();
            var newId = Guid.NewGuid();

            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();

            var mediator = new Mock<IMediator>();

            var campaignInvitation = new CampaignInvitation
            {
                Title = "123",
                Id = id,
                Active = true,
                EventCampaignId = id,
                Interval = 0,
                IntervalType = IntervalType.Days,
                InvitationType = InvitationType.Accepted,
                IsInstant = true,
                SortOrder = 0,
                StartDate = DateTime.UtcNow,
                TenantId = id
            };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>();
            });

            var mapper = config.CreateMapper();

            var smsTemplate = new CampaignSmsTemplateBuilder().BuildEntity();
            var emailTemplate = new CampaignEmailTemplateBuilder().BuildEntity();
            var whatsappTemplate = new CampaignWhatsappTemplateBuilder().BuildEntity();

            mediator
                .Setup(x => x.Publish(It.IsAny<InvitationDeleted>(), CancellationToken.None))
                .Returns((InvitationDeleted id, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(campaignInvitation));

            IEnumerable<CampaignInvitation> listData = new List<CampaignInvitation>()
            {
                campaignInvitation
            };

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(campaignInvitation));

            repo
                .Setup(x => x.LoadInvitationGroup(It.IsAny<Guid>(), It.IsAny<InvitationType>(), CancellationToken.None))
                .Returns((Guid q, InvitationType it, CancellationToken t) => Task.FromResult(listData));

            repo
                .Setup(x => x.Create(It.IsAny<CampaignInvitation>(), CancellationToken.None))
                .Returns((CampaignInvitation q, CancellationToken t) => Task.FromResult(newId));

            repo
                .Setup(x => x.GetCampaignSmsTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(mapper.Map<CampaignSmsTemplate>(smsTemplate)));

            repo
                .Setup(x => x.GetCampaignEmailTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(mapper.Map<CampaignEmailTemplate>(emailTemplate)));

            repo
                .Setup(x => x.GetCampaignWhatsappTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(mapper.Map<CampaignWhatsappTemplate>(whatsappTemplate)));

            repo
                .Setup(x => x.DeleteSmsTemplate(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.DeleteWhatsappTemplate(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.UpdateRange(It.IsAny<IEnumerable<CampaignInvitation>>(), CancellationToken.None))
                .Returns((IEnumerable<CampaignInvitation> q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.DeleteEmailTemplate(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.Delete(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => MediatR.Unit.Task);

            var prodHandler = new DeleteInvitationHandler(repo.Object, mediator.Object);

            var cmd = new DeleteInvitationCmd
            {
                Id = id
            };

            // Act
            var apiResult = await prodHandler.Handle(cmd, CancellationToken.None);

            // Assert
            Assert.IsNotNull(apiResult);
        }
    }
}