﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    [TestClass]
    public class ConvertContactsToGuestsHandlerTest
    {
        /// <summary>
        /// unit test to transform contacts to guests
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanConvertContactsToGuestsHandlerTest()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var mapper = new Mock<IMapper>();
            var repo = new Mock<IContactRepository<Domain.Entities.Contact, Guid>>();
            var transactionManager = new Mock<ITransactionManager>();

            var transaction = new Mock<ITransaction>();
            transaction
                .Setup(x => x.Commit());

            transactionManager
                .Setup(x => x.BeginTransactionAsync(CancellationToken.None))
                .Returns((CancellationToken q) => Task.FromResult(transaction.Object));

            repo
                .Setup(x => x.UpdateContactsToGuests(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid id, CancellationToken token) => MediatR.Unit.Task);

            mediator
                .Setup(x => x.Publish(It.IsAny<ContactsConvertedToGuests>(), CancellationToken.None))
                .Returns((ContactsConvertedToGuests q, CancellationToken t) => MediatR.Unit.Task);

            var id = Guid.NewGuid();

            // Act
            var procHandler = new ConvertContactsToGuestsHandler(repo.Object, mediator.Object, transactionManager.Object);
            var cmd = new ConvertContactsToGuestsCmd() { CampaignId = id };
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, id);
        }
    }
}
