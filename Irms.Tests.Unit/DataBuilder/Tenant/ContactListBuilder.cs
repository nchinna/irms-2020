﻿using System;
using System.Collections.Generic;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class ContactListBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public bool IsGlobal { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }


        public static implicit operator Domain.Entities.ContactList(ContactListBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.ContactList Build()
        {
            return new Domain.Entities.ContactList()
            {
                Name = "ContactList1",
                CreatedOn = DateTime.UtcNow,
                IsGlobal = true
            };
        }
    }
}
