﻿using System;
using System.Collections.Generic;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class EventBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public short? EventTypeId { get; set; }
        public bool IsLimitedAttendees { get; set; }
        public int? MaximumAttendees { get; set; }
        public int? OverflowAttendees { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string TimeZoneUtcOffset { get; set; }
        public string TimeZoneName { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }


        public static implicit operator Domain.Entities.Event(EventBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Event Build()
        {
            return new Domain.Entities.Event
            {
                Id = Guid.NewGuid(),
                Name = "event to create",
                IsLimitedAttendees = true,
                MaximumAttendees = 200,
                OverflowAttendees = 50,
                StartDateTime = DateTime.UtcNow,
                EndDateTime = DateTime.UtcNow.AddDays(30),
                Features = new List<Guid>() { Guid.NewGuid() }
            };
        }
    }
}
