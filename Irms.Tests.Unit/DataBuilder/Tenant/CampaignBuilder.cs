﻿using Irms.Domain.Entities;
using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class CampaignBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public Guid? ContactListId { get; set; }
        public byte? EntryCriteriaTypeId { get; set; }
        public bool? ExitCriteriaType { get; set; }
        public CampaignStatus CampaignStatus { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }


        public static implicit operator Campaign(CampaignBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Campaign Build()
        {
            return new Campaign
            {
                Id = Guid.NewGuid(),
                Name = "Campaign1",
                EventId = Guid.NewGuid(),
                Active = true,
                Deleted = false,
                CampaignStatus = CampaignStatus.Draft,
            };
        }

        public Data.EntityClasses.EventCampaign BuildEntity()
        {
            return new Data.EntityClasses.EventCampaign
            {
                Name = "Campaign1",
                EventId = Guid.NewGuid(),
                Active = true,
                Deleted = false,
                CampaignStatus = 0,
                Id = Guid.NewGuid()
            };
        }
    }
}
