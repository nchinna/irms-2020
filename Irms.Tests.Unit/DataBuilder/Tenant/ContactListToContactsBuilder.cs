﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class ContactListToContactsBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactListId { get; set; }
        public Guid ContactId { get; set; }

        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public static implicit operator ContactListToContact(ContactListToContactsBuilder instance)
        {
            return instance.Build();
        }

        public ContactListToContact Build()
        {
            return new ContactListToContact
            {
                Id = Id == Guid.Empty ? Guid.NewGuid() : Id,
                CreatedOn = DateTime.UtcNow,
                ContactListId = ContactListId,
                ContactId = ContactId
            };
        }
    }
}
