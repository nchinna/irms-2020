﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Irms.Application.CampaignInvitationResponses.Commands;
using Irms.Data.Read.CampaignInvitationResponses.Queries;
using Irms.Data.Read.CampaignInvitationResponses.QueryHandlers;
using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using Irms.WebApi.TenantControllers;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Controller.TenantControllers
{
    [TestClass]
    public class CampaignInvitationResponseControllerTest
    {
        [TestMethod]
        public async Task CanGetResponseForm()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var response = new InvitationResponseReadModel 
            {
                AcceptButtonText = "accepted_button_text",
                AcceptedHtml = "accepted_html",
                AcceptedOrRejected = Domain.Entities.ResponseMediaType.ACCEPTED,
                CampaignInvitationId = id,
                CampaignInvitationResponseId = id,
                Error = "",
                MediaType = Domain.Entities.MediaType.Email,
                ProceedButtonText = "proceed",
                RejectButtonText = "reject",
                RejectedHtml = "rejected_html",
                RsvpHtml = "rsvp_html",
                ThemeObj = "theme_obj",
                WelcomeHtml = "welcome_html"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationResponseQuery>(), CancellationToken.None))
                .Returns((GetCampaignInvitationResponseQuery q, CancellationToken t) => Task.FromResult(response));

            var sut = new CampaignInvitationResponseController(mediator.Object);


            //Act
            var result = await sut.Load(id, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(response.AcceptButtonText, result.AcceptButtonText);
            Assert.AreEqual(response.AcceptedHtml, result.AcceptedHtml);
            Assert.AreEqual(response.AcceptedOrRejected, result.AcceptedOrRejected);
            Assert.AreEqual(response.CampaignInvitationId, result.CampaignInvitationId);
            Assert.AreEqual(response.CampaignInvitationResponseId, result.CampaignInvitationResponseId);
            Assert.AreEqual(response.Error, result.Error);
            Assert.AreEqual(response.MediaType, result.MediaType);
            Assert.AreEqual(response.ProceedButtonText, result.ProceedButtonText);
            Assert.AreEqual(response.RejectButtonText, result.RejectButtonText);
            Assert.AreEqual(response.RejectedHtml, result.RejectedHtml);
            Assert.AreEqual(response.RsvpHtml, result.RsvpHtml);
            Assert.AreEqual(response.ThemeObj, result.ThemeObj);
            Assert.AreEqual(response.WelcomeHtml, result.WelcomeHtml);
        }

        [TestMethod]
        public async Task CanGetRfiResponseForm()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var response = new InvitationResponseRfiModel
            {
                CampaignInvitationResponseId = id,
                Error = "",
                BackgroundImageUrl = "image.jpg",
                FormSettings = "{}",
                FormTheme = "{}",
                Id = id,
                Questions = null,
                Submit = "submit",
                Thanks = "thanks",
                Welcome = "welcome"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationRfiQuery>(), CancellationToken.None))
                .Returns((GetCampaignInvitationRfiQuery q, CancellationToken t) => Task.FromResult(response));

            var sut = new CampaignInvitationResponseController(mediator.Object);


            //Act
            var result = await sut.LoadForRfi(id, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(response.CampaignInvitationResponseId, result.CampaignInvitationResponseId);
            Assert.AreEqual(response.Error, result.Error);
            Assert.AreEqual(response.BackgroundImageUrl, result.BackgroundImageUrl);
            Assert.AreEqual(response.FormSettings, result.FormSettings);
            Assert.AreEqual(response.FormTheme, result.FormTheme);
            Assert.AreEqual(response.Id, result.Id);
            Assert.AreEqual(response.Questions, result.Questions);
            Assert.AreEqual(response.Submit, result.Submit);
            Assert.AreEqual(response.Thanks, result.Thanks);
            Assert.AreEqual(response.Welcome, result.Welcome);
        }

        [TestMethod]
        public async Task SaveResponse()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();


            mediator
                .Setup(x => x.Send(It.IsAny<SaveResponseCmd>(), CancellationToken.None))
                .Returns((SaveResponseCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationResponseController(mediator.Object);


            //Act
            var cmd = new SaveResponseCmd
            {
                Id = id,
                Answer = Domain.Entities.UserAnswer.ACCEPTED,
                MediaType = Domain.Entities.MediaType.Email,
            };

            var result = await sut.SaveResults(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(cmd.Id, result);
        }

        [TestMethod]
        public async Task SaveRfi()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();


            mediator
                .Setup(x => x.Send(It.IsAny<SaveRfiCmd>(), CancellationToken.None))
                .Returns((SaveRfiCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationResponseController(mediator.Object);


            //Act
            var cmd = new SaveRfiCmd
            {
                Id = id,
                Answers = null
            };

            var result = await sut.SaveRfi(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(cmd.Id, result);
        }
    }
}
