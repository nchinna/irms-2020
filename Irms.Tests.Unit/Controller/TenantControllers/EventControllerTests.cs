﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Application.Events.Commands;
using Irms.WebApi.TenantControllers;
using Irms.Data.Read.Event.ReadModels;
using System.Collections.Generic;
using Irms.Data.Read.Event.Queries;
using System.Linq;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Event;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class EventControllerTests
    {
        /// <summary>
        /// can get event list by filters
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetEventList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new EventListItem
                {
                    Id = id,
                    Name = "eventone",
                    StartDateTime = DateTime.UtcNow,
                    EndDateTime = DateTime.UtcNow.AddDays(2),
                    ImagePath = "image/abcd.png"
                },
                new EventListItem
                {
                    Id = Guid.NewGuid(),
                    Name = "eventtwo",
                    StartDateTime = DateTime.UtcNow,
                    EndDateTime = DateTime.UtcNow.AddDays(2),
                    ImagePath = "image/two.png"
                }
            };

            IPageResult<EventListItem> templateListItems = new PageResult<EventListItem>(items, 10);

            mediator
                .Setup(x => x.Send(It.IsAny<GetEventList>(), CancellationToken.None))
                .Returns((GetEventList q, CancellationToken t) => Task.FromResult(templateListItems));

            var sut = new EventController(mediator.Object);

            //Act
            var result = await sut.GetEventList(
                new GetEventList(new List<PeriodCheckBoxRangeFilter>() { PeriodCheckBoxRangeFilter.Upcoming, PeriodCheckBoxRangeFilter.Past }, 1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var @event = result.Items.First();

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(@event.Id, id);
            Assert.AreEqual(@event.Name, "eventone");
            Assert.IsNotNull(@event.StartDateTime);
            Assert.IsNotNull(@event.EndDateTime);
            Assert.AreEqual(@event.ImagePath, "image/abcd.png");
            Assert.AreEqual(result.Items.Skip(1).First().Name, "eventtwo");
        }

        /// <summary>
        /// can get product features list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetProductFeatures()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new ProductFeatureListItem()
                {
                    Id = id,
                    Title = "rsvp",
                    Description = "RSVPDESC",
                    IsPurchased = true
                },
                new ProductFeatureListItem()
                {
                    Id = Guid.NewGuid(),
                    Title = "sms",
                    Description = "SMSFeature",
                    IsPurchased = true
                },
            };

            IEnumerable<ProductFeatureListItem> list = new List<ProductFeatureListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetProductFeatures>(), CancellationToken.None))
                    .Returns((GetProductFeatures q, CancellationToken t) => Task.FromResult(list));

            var sut = new EventController(mediator.Object);

            //Act
            var features = await sut.GetProductFeatures(
                CancellationToken.None);

            //Assert
            //Assert
            Assert.IsNotNull(features);

            var feature = features.First();

            Assert.AreEqual(features.Count(), 2);
            Assert.AreEqual(feature.Id, id);
            Assert.AreEqual(feature.Title, "rsvp");
            Assert.AreEqual(feature.Description, "RSVPDESC");
            Assert.AreEqual(feature.IsPurchased, true);
            Assert.AreEqual(features.Skip(1).First().Title, "sms");

        }

        /// <summary>
        /// can get event info with location
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetEventInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var eventInfo = new EventInfo
            {
                Id = id,
                Name = "Event one",
                StartDateTime = DateTime.UtcNow,
                EndDateTime = DateTime.UtcNow,
                City = "Al khobar",
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetEventInfo>(), CancellationToken.None))
                .Returns((GetEventInfo q, CancellationToken t) => Task.FromResult(eventInfo));

            var sut = new EventController(mediator.Object);

            //Act
            var result = await sut.GetEventInfo(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "Event one");
            Assert.IsNotNull(result.StartDateTime);
            Assert.AreEqual(result.City, "Al khobar");
        }

        /// <summary>
        /// can get event info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetEventFeatures()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new EventFeatureListItem()
                {
                    Id = id,
                    Title = "rsvp",
                    Description = "RSVPDESC",
                    IsPurchased = true,
                    IsChecked = true
                },
                new EventFeatureListItem()
                {
                    Id = Guid.NewGuid(),
                    Title = "sms",
                    Description = "SMSFeature",
                    IsPurchased = true,
                    IsChecked = false,
                },
            };

            IEnumerable<EventFeatureListItem> list = new List<EventFeatureListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetEventFeatures>(), CancellationToken.None))
                    .Returns((GetEventFeatures q, CancellationToken t) => Task.FromResult(list));

            var sut = new EventController(mediator.Object);

            //Act
            var features = await sut.GetEventFeatures(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(features);

            var feature = features.First();

            Assert.AreEqual(features.Count(), 2);
            Assert.AreEqual(feature.Id, id);
            Assert.AreEqual(feature.Title, "rsvp");
            Assert.AreEqual(feature.Description, "RSVPDESC");
            Assert.AreEqual(feature.IsPurchased, true);
            Assert.AreEqual(feature.IsChecked, true);
            Assert.AreEqual(features.Skip(1).First().Title, "sms");

        }
        /// <summary>
        /// can create event with location
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateEvent()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateEventCmd>(), CancellationToken.None))
                .Returns((CreateEventCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new EventController(mediator.Object);

            //Act
            var apiResult = await sut.Create(
                new CreateEventCmd
                {
                    Name = "Event one",
                    StartDateTime = DateTime.UtcNow,
                    EndDateTime = DateTime.UtcNow,
                    City = "Al khobar",
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// can update event info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateEventInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateEventCmd>(), CancellationToken.None))
                .Returns((UpdateEventCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new EventController(mediator.Object);

            //Act
            await sut.Update(
                new UpdateEventCmd
                {
                    Name = "Event one",
                    StartDateTime = DateTime.UtcNow,
                    EndDateTime = DateTime.UtcNow,
                    City = "Al khobar",
                },
                CancellationToken.None);
        }

        /// <summary>
        /// can update event features
        /// </summary>
        /// <returns></returns>
        public async Task CanUpdateEventFeatures()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateEventFeaturesCmd>(), CancellationToken.None))
                .Returns((UpdateEventFeaturesCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new EventController(mediator.Object);

            //Act
            await sut.UpdateEventFeatures(
                new UpdateEventFeaturesCmd
                {
                    Id = Guid.NewGuid(),
                    Features = new List<Guid>() { Guid.NewGuid(), Guid.NewGuid() },
                },
                CancellationToken.None); ;
        }

        /// <summary>
        /// can delete event by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteEventById()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new DeleteEventCmd();
            request.Id = Guid.NewGuid();

            var response = new MediatR.Unit();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteEventCmd>(), CancellationToken.None))
                .Returns((DeleteEventCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new EventController(mediator.Object);
            //Act
            var apiResult = await sut.Delete(
                request,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

    }
}
