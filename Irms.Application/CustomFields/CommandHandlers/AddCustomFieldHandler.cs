﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CustomFields.Commands;
using Irms.Application.CustomFields.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CustomFields.CommandHandlers
{
    public class AddCustomFieldHandler : IRequestHandler<AddCustomFieldCmd, Guid>
    {
        public readonly IMediator _mediator;
        public readonly IMapper _mapper;
        public readonly ICustomFieldRepository<CustomField, Guid> _repo;


        private static readonly string[] ForbiddenNames = new string[] {"Title", "FullName", "PrefferedName", "Gender", "Email", "AlternateEmail", "MobileNumber", "SecondaryMobileNumber", "Nationality", "Position"};
        public AddCustomFieldHandler(
            IMediator mediator,
            IMapper mapper,
            ICustomFieldRepository<CustomField, Guid> repo
            )
        {
            _mediator = mediator;
            _mapper = mapper;
            _repo = repo;
        }

        public async Task<Guid> Handle(AddCustomFieldCmd request, CancellationToken token)
        {
            var alreadyExists = await _repo.HasCustomFieldWithThisName(request.Name, token);
            if(alreadyExists)
            {
                throw new IncorrectRequestException("Field with this name already exists.");
            }

            if (ForbiddenNames.Contains(request.Name))
            {
                throw new IncorrectRequestException("Name is forbidden");
            }

            var data = new CustomField
            {
                CustomFieldType = request.CustomFieldType,
                FieldName = request.Name,
                MinValue = request.MinValue,
                MaxValue = request.MaxValue
            };

            await _mediator.Publish(new CustomFieldAdded(data.Id), token);
            return await _repo.Create(data, token);
        }
    }
}
