﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CustomFields.Commands;
using Irms.Application.CustomFields.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CustomFields.CommandHandlers
{
    public class UpdateCustomFieldHandler : IRequestHandler<UpdateCustomFieldCmd, Guid>
    {
        public readonly IMediator _mediator;
        public readonly IMapper _mapper;
        public readonly ICustomFieldRepository<CustomField, Guid> _repo;
        public UpdateCustomFieldHandler(
            IMediator mediator,
            IMapper mapper,
            ICustomFieldRepository<CustomField, Guid> repo
            )
        {
            _mediator = mediator;
            _mapper = mapper;
            _repo = repo;
        }

        public async Task<Guid> Handle(UpdateCustomFieldCmd request, CancellationToken token)
        {
            var data = await _repo.GetById(request.Id, token);

            _mapper.Map(request, data);
            await _repo.Update(data, token);

            await _mediator.Publish(new CustomFieldUpdated(data.Id), token);

            return request.Id;
        }
    }
}
