﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CustomFields.Commands
{
    public class AddCustomFieldCmd : IRequest<Guid>
    {
        public string Name { get; set; }
        public CustomFieldType CustomFieldType { get; set; }
        public long? MinValue { get; set; }
        public long? MaxValue { get; set; }
    }
}
