﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class UpdateContactListImportantFieldsCmd : IRequest<Unit>
    {
        public Guid ListId { get; set; }
        public IEnumerable<string> FIeldNames { get; set; }
        public IEnumerable<ImportantField> CustomFields { get; set; }

        public class ImportantField
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}
