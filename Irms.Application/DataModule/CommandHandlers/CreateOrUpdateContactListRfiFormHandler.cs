﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class CreateOrUpdateContactListRfiFormHandler : IRequestHandler<CreateOrUpdateContactListRfiFormCmd, Unit>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IRfiFormRepository<RfiForm, Guid> _rfiFormRepository;
        private readonly IMediator _mediator;
        private readonly TenantBasicInfo _tenant;

        public CreateOrUpdateContactListRfiFormHandler(TenantBasicInfo tenant,
            IRfiFormRepository<RfiForm, Guid> rfiFormRepository,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _tenant = tenant;
            _contactListRepository = contactListRepository;
            _rfiFormRepository = rfiFormRepository;
        }

        /// <summary>
        /// creates or updates rfi for contact list
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(CreateOrUpdateContactListRfiFormCmd request, CancellationToken token)
        {
            var rfi = await _rfiFormRepository.GetByContactListId(request.ContactListId, token);
            var customFieldsToCreate = await _contactListRepository.GetContactListImportantFields(request.ContactListId, token);

            var rfiFormQuestions = new List<RfiFormQuestion>();

            Guid rfiId = Guid.Empty;

            if (rfi == null)
            {
                var rfiForm = new RfiForm
                {
                    Id = Guid.NewGuid(),
                    CampaignInvitationId = null,
                    FormSettings = "{\"title\":\"hello form\",\"defaultLanguage\":\"en\",\"languageOption\":false}",
                    FormTheme = "{\"buttons\":{\"background\":\"#576268\",\"text\":\"#fff\"},\"questions\":{\"color\":\"#576268\"},\"answers\":{\"color\":\"#576268\"},\"background\":{\"color\":\"#fff\",\"style\":\"\"}}",
                    SubmitHtml = "Everything seems good,\nYou can submit now!",
                    ThanksHtml = "<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">Thank you for your response!</span></p>\n      <p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We will see you at the&nbsp;</span></p>\n      <h2 style=\"text-align: center;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\n      <p style=\"text-align: center;\">&nbsp;</p>",
                    WelcomeHtml = "<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif;\">You are invited to</span></p>\n      <h2 style=\"text-align: left;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\n      <p style=\"text-align: left;\"><span style=\"color: #000000; font-family: tahoma, arial, helvetica, sans-serif;\">on 23rd April 2020, 6:00 PM</span></p>\n      <h5 style=\"text-align: left;\"><span style=\"color: #34495e;\"><em><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">\n      Please let us know if you will be joining us</span></em></span></h5>",
                };

                int counter = 0;
                foreach(var customField in customFieldsToCreate)
                {
                    var id = Guid.NewGuid();
                    var rfiFormQuestion = new RfiFormQuestion
                    {
                        Id = id,
                        Mapped = true,
                        MappedField = customField.FieldName,
                        SortOrder = counter++,
                        Question = "{" + $"\"type\":\"singleLineText\",\"id\":\"{id}\",\"headlineArabic\":\"\",\"headline\":\"Mapping to {customField.FieldName}\",\"isDescription\":false,\"description\":\"Description for mapping to {customField.FieldName}\",\"descriptionArabic\":null,\"required\":true,\"multiline\":false,\"minLength\":3,\"maxLength\":20,\"pattern\":\"\",\"mapping\":true,\"mappingField\":\"{customField.FieldName}\",\"jumps\":null" + "}"
                    };

                    rfiFormQuestions.Add(rfiFormQuestion);
                }

                if(rfiFormQuestions.Count > 0)
                {
                    rfiForm.RfiFormQuestions = rfiFormQuestions.ToImmutableList();
                    rfiId = await _rfiFormRepository.Create(rfiForm, token);
                }
            }
            else
            {
                rfiId = rfi.Id;
                var existingRfiQuestions = await _rfiFormRepository.GetQuestions(rfi.Id, token);
                var customNames = existingRfiQuestions.Select(x => x.MappedField);

                var rfiQuestionsToAdd = customFieldsToCreate
                    .Where(x => !customNames.Contains(x.FieldName))
                    .ToList();

                int counter = 0;
                foreach (var customField in rfiQuestionsToAdd)
                {
                    var id = Guid.NewGuid();
                    var rfiFormQuestion = new RfiFormQuestion
                    {
                        Id = id,
                        RfiFormId = rfi.Id,
                        Mapped = true,
                        MappedField = customField.FieldName,
                        SortOrder = counter++,
                        Question = "{" + $"\"type\":\"singleLineText\",\"id\":\"{id}\",\"headlineArabic\":\"\",\"headline\":\"Mapping to {customField.FieldName}\",\"isDescription\":false,\"description\":\"Description for mapping to {customField.FieldName}\",\"descriptionArabic\":null,\"required\":true,\"multiline\":false,\"minLength\":3,\"maxLength\":20,\"pattern\":\"\",\"mapping\":true,\"mappingField\":\"{customField.FieldName}\",\"jumps\":null" + "}"
                    };

                    rfiFormQuestions.Add(rfiFormQuestion);
                }

                if(rfiFormQuestions.Count > 0)
                {
                    rfi.RfiFormQuestions = rfiFormQuestions.ToImmutableList();
                    await _rfiFormRepository.Update(rfi, token);
                }
            }

            await _mediator.Publish(new RfiUpdated(rfiId), token);

            return Unit.Value;
        }
    }
}
