﻿using System;
using MediatR;

namespace Irms.Application.Files.Commands
{
    public class UploadFileCmd : IRequest<Unit>
    {
        public UploadFileCmd(
            string name,
            string data,
            params FileType[] fileType)

        {
            Name = name;
            Data = data;
            FileType = fileType;
        }

        public string Name { get; }
        public string Data { get; }
        public FileType[] FileType { get; }
    }
}
