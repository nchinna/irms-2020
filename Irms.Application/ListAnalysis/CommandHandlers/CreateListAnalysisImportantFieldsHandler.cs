﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract.Repositories;
using Irms.Application.ListAnalysis.Commands;
using Irms.Application.ListAnalysis.Events;

namespace Irms.Application.ListAnalysis.CommandHandlers
{
    public class CreateListAnalysisImportantFieldsHandler : IRequestHandler<CreateListAnalysisImportantFieldsCmd, Unit>
    {
        private readonly IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> _repo;
        private readonly IMediator _mediator;

        public CreateListAnalysisImportantFieldsHandler(
            IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(CreateListAnalysisImportantFieldsCmd message, CancellationToken token)
        {
            await _repo.AddImportantFieldsToGuestList(message, token);

            await _mediator.Publish(new ImportantFieldsToGuestListAdded(message.ListId), token);
            return Unit.Value;
        }
    }
}