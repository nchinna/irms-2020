﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Webhooks.Events;
using Irms.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class TwilioWebhookSmsHandler : IRequestHandler<TwilioWebhookSmsCmd, Unit>
    {
        private readonly IBackgroundServiceRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        public TwilioWebhookSmsHandler(IBackgroundServiceRepository<CampaignInvitation, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(TwilioWebhookSmsCmd request, CancellationToken token)
        {
            Thread.Sleep(TimeSpan.FromSeconds(10));
            var log = await _repo.GetMessageById(request.MessageSid, token);
            var status = (InvitationMessageStatus)Enum.Parse(typeof(InvitationMessageStatus), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(request.MessageStatus.ToLower()), true);

            var response = new ProviderResponse
            {
                Id = Guid.NewGuid(),
                TenantId = log.TenantId,
                CampaignInvitationMessageLogId = log.Id,
                ContactId = log.ContactId,
                ProviderType = (int)ProviderType.Twilio,
                MessageStatus = (int)status,
                ResponseDate = DateTime.UtcNow,
                ResponseJson = JsonConvert.SerializeObject(request)
            };

            await _repo.LogProviderResponse(response, token);
            await _mediator.Publish(new TwilioWebhookSmsStatusLogged(response.Id, log.TenantId), token);

            return Unit.Value;
        }
    }
}
