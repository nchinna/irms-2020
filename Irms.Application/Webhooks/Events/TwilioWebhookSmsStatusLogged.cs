﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Webhooks.Events
{
    public class TwilioWebhookSmsStatusLogged : ITenantEvent
    {
        public TwilioWebhookSmsStatusLogged(Guid logId, Guid tenantId)
        {
            LogId = logId;
            TenantId = tenantId;
        }

        public Guid LogId { get; }
        public string Name { get; }
        public Guid TenantId { get; }

        public Guid ObjectId => LogId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x) => $"The twilio webook message {x(LogId)} status has been logged";
    }


    public class TwilioWhatsappBotResponseLogged : ITenantEvent
    {
        public TwilioWhatsappBotResponseLogged(Guid logId, Guid tenantId)
        {
            LogId = logId;
            TenantId = tenantId;
        }

        public Guid LogId { get; }
        public string Name { get; }
        public Guid TenantId { get; }

        public Guid ObjectId => LogId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x) => $"The twilio whatsapp BOT of contact {x(LogId)} has been logged";
    }
}
