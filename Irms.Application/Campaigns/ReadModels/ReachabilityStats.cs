﻿using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Campaigns.ReadModels
{
    public class ReachabilityStats
    {
        public long EmailCount { get; set; }
        public long SmsCount { get; set; }
        public long WhatsAppCount { get; set; }
        public long NotReachableCount { get; set; }
    }
}
