﻿using Irms.Application.Campaigns.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Campaigns.Queries
{
    public class GetGuestsByListId : IRequest<(bool reachabilitySubscribed, IEnumerable<GuestInfoListItem> guests)>
    {
        public Guid ListId { get; }

        public GetGuestsByListId(Guid listId)
        {
            ListId = listId;
        }
    }
}
