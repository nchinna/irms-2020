﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Product.Events
{
    class ProductDeleted : IEvent
    {
        public ProductDeleted(Guid productId)
        {
            ProductId = productId;
        }

        public Guid ProductId { get; }
        public Guid ObjectId => ProductId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has successfully deleted the product with ID {x(ProductId)}";
    }
}
