﻿
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Product.Commands;
using Irms.Application.Product.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Product.CommandHandlers
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCmd, Guid>
    {
        private readonly IMediator _mediator;
        private readonly IProductRepository<Irms.Domain.Entities.Product, Guid> _repository;
        private readonly IRepository<Irms.Domain.Entities.LicensePeriod, byte> _licenseRepository;
        private readonly IMapper _mapper;

        public UpdateProductHandler(IMediator mediator, IProductRepository<Irms.Domain.Entities.Product, Guid> repository, IRepository<Irms.Domain.Entities.LicensePeriod, byte> licenseRepository, IMapper mapper)
        {
            _mediator = mediator;
            _repository = repository;
            _mapper = mapper;
            _licenseRepository = licenseRepository;
        }
        /// <summary>
        /// this method handle the request for updating the <see cref="EntityClasses.Product"/> 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(UpdateProductCmd request, CancellationToken cancellationToken)
        {
            var serviceCatalog = _mapper.Map<List<Domain.Entities.ServiceCatalog>>(request.Services);

            var license = new Domain.Entities.LicensePeriod(); 
            license.Days = request.LicenseDays;
            license.Title = request.LicenseName;

            var updateProductRequest = new Domain.Entities.Product(
                request.Id,
                request.ProductName,
                request.Price,
                request.DisountPercentage,
                false,
                request.CurrencyId,
                license,
                serviceCatalog);

            await _repository.Update(updateProductRequest, cancellationToken);

                
            await _mediator.Publish(new ProductUpdated(request.Id, request.ProductName));
            return request.Id;
        }
    }
}
