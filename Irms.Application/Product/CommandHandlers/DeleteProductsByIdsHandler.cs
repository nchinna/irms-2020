﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Product.Commands;
using Irms.Application.Product.Events;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Product.CommandHandlers
{
    public class DeleteProductsByIdsHandler : IRequestHandler<DeleteProductsByIdsCmd>
    {
        private readonly IMediator _mediator;
        private readonly IProductRepository<Irms.Domain.Entities.Product, Guid> _repository;
        private readonly ITransactionManager _transactionManager;

        public DeleteProductsByIdsHandler(IMediator mediator, IProductRepository<Irms.Domain.Entities.Product, Guid> repository, ITransactionManager transactionManager)
        {
            _mediator = mediator;
            _repository = repository;
            _transactionManager = transactionManager;
        }

        /// <summary>
        /// this method handle the request for marking the list of <see cref="Product"/> as deleted by ids using Transaction Manager
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteProductsByIdsCmd request, CancellationToken cancellationToken)
        {
            using (var tr = await _transactionManager.BeginTransactionAsync(cancellationToken))
            {
                await _repository.DeleteList(request.Ids, cancellationToken);
                tr.Commit();
            }
            var tasks = new Task[request.Ids.Count()];
            int i = 0;
            foreach (var id in request.Ids)
            {
                tasks[i] =_mediator.Publish(new ProductDeleted(id));
                i++;
            }
            await Task.WhenAll(tasks);
            return Unit.Value;
        }
    }
}
