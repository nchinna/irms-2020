﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Product.Commands
{
    public class GetProductsByIdsCmd : IRequest<IEnumerable<Domain.Entities.Product>>
    {
        public GetProductsByIdsCmd(IEnumerable<Guid> ids)
        {
            Ids = ids;
        }

        public IEnumerable<Guid> Ids { get; }
    }
}
