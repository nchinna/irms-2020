﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace Irms.Application.Product.Commands
{
    public class UpdateProductCmd : CreateProductCmd
    {
        public UpdateProductCmd(): base() {}

        public UpdateProductCmd(string productName, decimal price, byte currencyId) : base(productName, price, currencyId)
        {
        }

        public UpdateProductCmd(string productName, decimal price, byte currencyId, List<ProductService> services, decimal? disountPercentage) : base(productName, price, currencyId, services, disountPercentage)
        {
        }
        [BindRequired]
        public new Guid Id { get; set; }
    }
}
