﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.BackgroundService.Commands;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract.Services.Notifications.Notifiers;

namespace Irms.Application.BackgroundService.CommandHandlers
{
    public class SendMessageHandler :
        IRequestHandler<SendEmailCmd, (bool sent, EmailMessage msg)>,
        IRequestHandler<SendSmsCmd, (bool sent, SmsMessage msg)>
    {
        private readonly ICampaignInvitationNotifier _notifier;
        public SendMessageHandler(ICampaignInvitationNotifier notifier)
        {
            _notifier = notifier;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<(bool sent, EmailMessage msg)> Handle(SendEmailCmd request, CancellationToken token)
        {
            var emsg = await _notifier.NotifyByEmail(request.Msg, token);
            return emsg;
        }

        public async Task<(bool sent, SmsMessage msg)> Handle(SendSmsCmd request, CancellationToken token)
        {
            //sms message
            var response = await _notifier.NotifyBySms(request.Msg, token);
            return response;
        }
    }
}