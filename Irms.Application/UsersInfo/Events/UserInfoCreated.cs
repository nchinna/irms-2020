﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.UsersInfo.Events
{
    public class UserInfoCreated : IEvent
    {
        public UserInfoCreated(Guid employeeId)
        {
            UserInfoId = employeeId;
        }

        public Guid UserInfoId { get; }

        public Guid ObjectId => UserInfoId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The employee {x(UserInfoId)} has been created by {u}";
    }
}
