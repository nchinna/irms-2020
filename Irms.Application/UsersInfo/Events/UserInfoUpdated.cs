﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.UsersInfo.Events
{
    public class UserInfoUpdated : IEvent
    {
        public UserInfoUpdated(Guid userId)
        {
            UserInfoId = userId;
        }

        public Guid UserInfoId { get; }

        public Guid ObjectId => UserInfoId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The user {x(UserInfoId)} has been updated by {u}";
    }
}
