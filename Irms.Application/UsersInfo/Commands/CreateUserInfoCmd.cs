﻿using Irms.Domain.Entities;
using MediatR;
using System;

namespace Irms.Application.UsersInfo.Commands
{
    public class CreateUserInfoCmd : IRequest<Guid>
    {
        public CreateUserInfoCmd(
            string email,
            string phone,
            string password,
            string passwordConfirmation,
            RoleType role,
            string fullName,
            Gender gender,
            DateTime birthDate,
            string messagingHandle,
            string nationalId)
        {
            Email = email;
            Phone = phone;
            Password = password;
            PasswordConfirmation = passwordConfirmation;
            Role = role;
            FullName = fullName;
            Gender = gender;
            BirthDate = birthDate;
            MessagingHandle = messagingHandle;
            NationalId = nationalId;
        }

        //asp net user
        public string Email { get; }
        public string Phone { get; }
        public string NationalId { get; }
        public string Password { get; }
        public string PasswordConfirmation { get; }

        //employee
        public RoleType Role { get; }
        public string FullName { get; }
        public Gender Gender { get; }
        public DateTime BirthDate { get; }
        public string MessagingHandle { get; }
    }
}
