﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Events.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;

namespace Irms.Application.Events.CommandHandlers
{
    public class DeleteEventHandler : IRequestHandler<DeleteEventCmd, Unit>
    {
        private readonly IEventRepository<Event, Guid> _repository;
        private readonly IMediator _mediator;

        public DeleteEventHandler(
            IEventRepository<Event, Guid> repository, IMediator mediator
            )
        {
            _repository = repository;
            _mediator = mediator;
        }

        /// <summary>
        /// del event handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteEventCmd request, CancellationToken token)
        {
            await _repository.Delete(request.Id, token);

            var e = new EventDeleted(request.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
