﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Events.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;

namespace Irms.Application.Events.CommandHandlers
{
    public class UpdateEventFeaturesHandler : IRequestHandler<UpdateEventFeaturesCmd, Unit>
    {
        private readonly IEventRepository<Event, Guid> _repository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public UpdateEventFeaturesHandler(
            IEventRepository<Event, Guid> repository,
            IMediator mediator,
            IMapper mapper,
            TenantBasicInfo tenant
            )
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateEventFeaturesCmd request, CancellationToken token)
        {
            var @event = _mapper.Map<Event>(request);

            await _repository.UpdateFeatures(@event, token);

            var e = new EventFeaturesUpdated(@event.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
