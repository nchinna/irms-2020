﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Events.Events
{
    public class EventDeleted : IEvent
    {
        public EventDeleted(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; }

        public Guid ObjectId => EventId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The event {x(EventId)} has been deleted by {u}";
    }
}
