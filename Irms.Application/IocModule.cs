﻿using Autofac;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Messages.Services;

namespace Irms.Application
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            //builder.RegisterType<CampaignInvitationNotifier>().As<ICampaignInvitationNotifier>();
            builder.RegisterType<BaseMediumNotifier>().As<IBaseMediumNotifier>();

            builder
                .RegisterAssemblyTypes(ThisAssembly)
                .AssignableTo<INotifier>()
                .AsImplementedInterfaces();

            builder.RegisterType<NotifierProvider>().As<INotifierProvider>();
            builder.RegisterType<TemplateTagProvider>().As<ITemplateTagProvider>();
        }
    }
}
