﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.Templates.Queries;
using Irms.Domain.Entities.Templates;
using Irms.Domain;

namespace Irms.Application.Messages.Services
{
    /// <summary>
    /// Implementation for password recovery notification <see cref="IForgotPasswordNotifier"/> resolved by MediatR
    /// </summary>
    public class ForgotPasswordNotifier : IForgotPasswordNotifier
    {
        private readonly IMediator _mediator;
        private readonly ISmsSender _smsSender;
        private readonly IEmailSender _emailSender;
        private readonly ICurrentLanguage _language;
        private readonly IPhoneNumberValidator _phoneValidator;

        public ForgotPasswordNotifier(IMediator mediator, ISmsSender smsSender, IEmailSender emailSender, ICurrentLanguage language, IPhoneNumberValidator phoneValidator)
        {
            _mediator = mediator;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _language = language;
            _phoneValidator = phoneValidator;
        }

        public IEnumerable<string> Placeholders => new[]
        {
            "Link",
            "SmsCode"
        }
        .Select(PlaceholderFormatHelper.GetPlaceholder);

        public async Task NotifyByEmail(string email, string passwordResetLink, CancellationToken cancellationToken)
        {
            var template = await _mediator.Send(new GetActiveEmailTemplate(TemplateType.ForgotPassword), cancellationToken);
            if (template == null)
            {
                return;
            }

            template.Translate(await _language.GetRequestLanguageId());

            var link = new EmailMessage.TemplateVariable("{{Link}}", $"<a href=\"{passwordResetLink}\">{template.EmailSubject}</a>");
            var recipient = new EmailMessage.Recipient(email, template.EmailSubject, link.Enumerate());

            var msg = new EmailMessage(template.EmailBody, new[] { recipient });
            await _emailSender.SendEmail(msg, cancellationToken);
        }

        public async Task NotifyBySms(string phone, string passwordResetCode, CancellationToken cancellationToken)
        {
            var formattedPhone = _phoneValidator.Validate(phone, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException("Phone number format is incorrect");
            }

            var template = await _mediator.Send(new GetActiveSmsTemplate(TemplateType.ForgotPassword), cancellationToken);
            if (template == null)
            {
                return;
            }

            template.Translate(await _language.GetRequestLanguageId());

            var code = new SmsMessage.TemplateVariable("{{SmsCode}}", passwordResetCode);
            var recipient = new SmsMessage.Recipient(formattedPhone, code.Enumerate());

            var sms = new SmsMessage(template.SmsText, new[] { recipient });
            await _smsSender.SendSms(sms, cancellationToken);
        }
    }
}
