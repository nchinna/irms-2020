﻿using MediatR;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Domain;
using Irms.Domain.Entities.Templates;

namespace Irms.Application.Messages.Services
{
    public class NotifierProvider : INotifierProvider
    {
        private readonly ISmsSender _smsSender;
        private readonly IEmailSender _emailSender;
        private readonly IMediator _mediator;
        private readonly IPhoneNumberValidator _phoneValidator;
        private readonly ICurrentLanguage _language;
        private readonly IMessageTranslator _translator;

        public NotifierProvider(
            ISmsSender smsSender,
            IEmailSender emailSender,
            IMediator mediator,
            ICurrentLanguage language,
            IMessageTranslator translator,
            IPhoneNumberValidator phoneValidator)
        {
            _smsSender = smsSender;
            _emailSender = emailSender;
            _mediator = mediator;
            _language = language;
            _translator = translator;
            _phoneValidator = phoneValidator;
        }

        public INotifier GetNotifier(TemplateType templateType)
        {
            switch (templateType)
            {
                case TemplateType.ForgotPassword: return new ForgotPasswordNotifier(_mediator, _smsSender, _emailSender, _language, _phoneValidator);
                case TemplateType.SuccessfulActivation: throw new IncorrectRequestException("template type is not supported");
                case TemplateType.OTPCode: return new OTPCodeNotifier(_mediator, _smsSender, _language, _phoneValidator);

                default: throw new IncorrectRequestException("Template type is invalid");
            }
        }
    }
}
