﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class UpdateCustomFieldCmd : IRequest<Unit>
    {
        public UpdateCustomFieldCmd(Guid id, CustomFieldModel data)
        {
            Id = id;
            Data = data;
        }

        public Guid Id { get; set; }
        public CustomFieldModel Data { get; set; }

        public class CustomFieldModel
        {
            public CustomFieldModel(IEnumerable<CustomFieldEntry> fields)
            {
                Fields = fields;
            }

            public IEnumerable<CustomFieldEntry> Fields { get; set; }
            
            public class CustomFieldEntry
            {
                public Guid Id { get; set; }
                public string Title { get; set; }
                public string Value { get; set; }
                public int CustomFieldType { get; set; }
                public long? MinValue { get; set; }
                public long? MaxValue { get; set; }
            }
        }
    }
}
