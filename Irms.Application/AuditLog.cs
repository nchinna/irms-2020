﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories.Base;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application
{
    public class AuditLog : INotificationHandler<IEvent>
    {
        private readonly IEventLogRecorder _recorder;

        public AuditLog(IEventLogRecorder recorder)
        {
            _recorder = recorder;
        }

        public Task Handle(IEvent notification, CancellationToken cancellationToken) => _recorder.AppendEventToLog(notification, cancellationToken);
    }
}
