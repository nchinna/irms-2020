﻿using System;
using MediatR;

namespace Irms.Application.Subscriptions.Commands
{
    public class CreateSubscriptionCmd : IRequest<Guid>
    {
        public Guid TenantId { get; set; }
        public Guid ProductId { get; set; }
    }
}
