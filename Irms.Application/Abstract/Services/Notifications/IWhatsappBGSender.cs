﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications
{
    public interface IWhatsappBGSender
    {
        Task<bool> SendWhatsappMessage(Guid tenantId, WhatsappMessage message, CancellationToken token);
        Task<bool> SendWhatsappWebhookMessage(Guid tenantId, WhatsappMessage message, CancellationToken token);
    }
}
