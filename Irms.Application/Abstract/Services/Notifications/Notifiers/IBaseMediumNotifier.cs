﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications.Notifiers
{
    public interface IBaseMediumNotifier
    {
        Task<bool> NotifyByEmail(string subject, string body, string recipient, Guid tenantId, CancellationToken token);

        Task<bool> NotifyBySms(Guid contactId, string body, string recipientPhone, Guid tenantId, CancellationToken token);
    }
}
