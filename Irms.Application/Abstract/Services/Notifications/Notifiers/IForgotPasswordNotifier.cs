﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications.Notifiers
{
    /// <summary>
    /// Password recovery notification interface
    /// </summary>
    public interface IForgotPasswordNotifier : INotifier
    {
        Task NotifyByEmail(
            string email,
            string passwordResetLink,
            CancellationToken cancellationToken);

        Task NotifyBySms(
            string phone,
            string passwordResetCode,
            CancellationToken cancellationToken);
    }
}
