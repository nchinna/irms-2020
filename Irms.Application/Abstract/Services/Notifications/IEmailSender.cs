﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications
{
    public interface IEmailSender
    {
        Task<bool> SendEmail(EmailMessage message, CancellationToken token);
    }
}
