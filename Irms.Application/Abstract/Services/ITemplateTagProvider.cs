﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services
{
    public interface ITemplateTagProvider
    {
        Task<Dictionary<string, string>> LoadTagsForContactAndEvent(Guid contactId, Guid eventId, Guid campaignInvitationId, CancellationToken token);
        string ReplaceTagsForTemplate(string template, Dictionary<string, string> tags);
    }
}
