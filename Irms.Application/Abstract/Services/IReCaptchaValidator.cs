﻿using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services
{
    public interface IReCaptchaValidator
    {
        Task<bool> Validate(string captchaResponse);
    }
}
