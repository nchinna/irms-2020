﻿namespace Irms.Application.Abstract.Services
{
    public interface IFileTypeValidator
    {
        bool CheckFileType(byte[] data, string declaredExtension);
        bool IsPicture(byte[] data);
        bool IsDocument(byte[] data);
        bool IsPdf(byte[] data);
    }
}
