﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface IDatabaseQueryExecutor
    {
        Task<int> ExecuteSqlInterpolatedAsync(FormattableString sql, CancellationToken token);
        Task<int> ExecuteSqlRawAsync(string sql, CancellationToken token);     
    }
}
