﻿using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface IDNSRecordsManager
    {
        Task AddSubDomain(string subDomain);
        Task UpdateSubDomain(string oldSubDomain, string newSubDomain);
        Task DeleteSubDomain(string clientUrl);
    }
}
