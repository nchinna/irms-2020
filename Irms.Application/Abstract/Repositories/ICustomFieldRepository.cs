﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.CustomFields.ReadModels;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ICustomFieldRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity: IEntity<TKey>
    {
        Task<bool> HasCustomFieldWithThisName(string name, CancellationToken token);
        Task<IEnumerable<CustomField>> Load(CancellationToken token);
        Task<IEnumerable<Domain.Entities.ContactCustomField>> LoadCustomFieldValues(IEnumerable<string> emails, IEnumerable<string> phones, CancellationToken token);
        Task<IEnumerable<Domain.Entities.ContactCustomField>> LoadCustomFieldValues(Guid contactId, CancellationToken token);
        Task<IEnumerable<Domain.Entities.Contact>> LoadContacts(CancellationToken token);
        Task<IEnumerable<KeyValuePair<string, int>>> LoadDocumentTypes(CancellationToken token);
        Task<IEnumerable<KeyValuePair<string, Guid>>> LoadNationalities(CancellationToken token);
        Task<IEnumerable<KeyValuePair<string, Guid>>> LoadCountries(CancellationToken token);
        Task UpdateContactCustomField(Guid contactId, string customFieldName, string value, CancellationToken token);
        Task CreateCustomFieldValueRange(IEnumerable<ContactCustomField> entities, CancellationToken token);
        Task UpdateCustomFieldValueRange(IEnumerable<ContactCustomField> entities, CancellationToken token);
        Task DeleteContactCustomField(Guid id, CancellationToken token);
    }
}
