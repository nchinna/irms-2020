﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories.Base
{
    public interface IEventLogRecorder
    {
        Task AppendEventToLog(IEvent @event, CancellationToken token);
        Task AppendEventToTenantLog(ITenantEvent @event, CancellationToken cancellationToken);
    }
}
