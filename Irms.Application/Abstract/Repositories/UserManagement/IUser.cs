﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Repositories.UserManagement
{
    public interface IUser
    {
        Guid Id { get; set; }
        Guid TenantId { get; set; }
        bool IsEnabled { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
        bool EmailConfirmed { get; set; }

        string PhoneNumber { get; set; }
        bool PhoneNumberConfirmed { get; set; }
    }
}
