﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Repositories.UserManagement
{
    public interface IUserFactory
    {
        IUser CreateUser(
            Guid id,
            string userName,
            string email,
            string phoneNumber,
            bool isEnabled = true,
            bool emailConfirmed = false,
            bool phoneNumberConfirmed = false);

        IUser CreateUserForTenant(
            Guid id,
            Guid tenantId,
            string userName,
            string email,
            string phoneNumber,
            bool isEnabled = true,
            bool emailConfirmed = false,
            bool phoneNumberConfirmed = false);
    }
}
