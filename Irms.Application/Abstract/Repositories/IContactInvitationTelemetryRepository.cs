﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Abstract.Repositories
{
    public interface IContactInvitationTelemetryRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {

    }
}
