﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface ICurrentLanguage
    {
        CultureInfo GetRequestCulture();
        Task<Guid?> GetRequestLanguageId();
    }
}
