﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class UpsertCampaignInvitationRejectedHandler : IRequestHandler<UpsertCampaignInvitationRejectedCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpsertCampaignInvitationRejectedHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(UpsertCampaignInvitationRejectedCmd message, CancellationToken token)
        {
            var ci = _mapper.Map<CampaignInvitation>(message);

            var inv = await _repo.GetById(message.Id, token);
            if (inv == null)
            {
                ci.CreateInvitationRejected();
                await _repo.Create(ci, token);
            }
            else
            {
                ci.UpdateInvitationRejected(inv);
                await _repo.Update(ci, token);
            }

            await _mediator.Publish(new CampaignInvitationRejectedUpserted(ci.Id), token);
            return ci.Id;
        }
    }
}
