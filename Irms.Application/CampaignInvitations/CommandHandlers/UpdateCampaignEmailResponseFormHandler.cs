﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using Irms.Application.Files.Commands;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class UpdateCampaignEmailResponseFormHandler : IRequestHandler<UpdateCampaignEmailResponseFormCmd, Unit>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public UpdateCampaignEmailResponseFormHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateCampaignEmailResponseFormCmd message, CancellationToken token)
        {
            var inv = await _repo.GetCampaignEmailTemplateById(message.Id, token);

            message.BackgroundImage = await UploadExistingImage(message.BackgroundImage, inv.BackgroundImagePath, token);

            message.AcceptHtml = await UploadExistingBodyImages(message.AcceptHtml, inv.AcceptHtml, token);
            message.RejectHtml = await UploadExistingBodyImages(message.RejectHtml, inv.RejectHtml, token);

            message.ThemeJson = DeleteImageIfExist(message.ThemeJson);

            await _repo.UpdateCampaignEmailResponseForm(message, token);

            await _mediator.Publish(new CampaignEmailResponseUpdated(message.Id), token);
            return Unit.Value;
        }

        private string DeleteImageIfExist(string body)
        {
            var images = GetImagesInHTMLString(body, true);
            foreach (var img in images)
            {
                body = body.Replace(img, string.Empty);
            }

            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            return newBody;
        }
        private List<string> GetImagesInHTMLString(string htmlString, bool isUri = false)
        {
            List<string> images = new List<string>();
            string pattern = isUri ? @"(data:image\/[^;]+;base64[^""]+)" : @" < img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath))
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {
                    var path = string.IsNullOrEmpty(existingImagePath) ? $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid().ToString()}_image.{res}" : existingImagePath;
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);
                    return path;
                }
                return newImagePath;
            }

            return string.Empty;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }
    }
}