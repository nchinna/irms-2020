﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Application.Files.Commands;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class CreateOrUpdateWhatsappTemplateHandler : IRequestHandler<CreateOrUpdateWhatsappTemplateCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public CreateOrUpdateWhatsappTemplateHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        public async Task<Guid> Handle(CreateOrUpdateWhatsappTemplateCmd request, CancellationToken token)
        {
            var ci = _mapper.Map<CampaignWhatsappTemplate>(request);

            var inv = await _repo.GetCampaignWhatsappTemplateByInvitationId(request.CampaignInvitationId, token);
            if (inv == null)
            {
                ci.Create();
                ci.AcceptButtonText = string.Empty;
                ci.ProceedButtonText = string.Empty;
                ci.RejectButtonText = string.Empty;
                ci.CampaignInvitationId = request.CampaignInvitationId;
                ci.Body = request.Body ?? string.Empty;
                ci.SenderName = request.SenderName ?? string.Empty;
                ci.TemplateId = request.TemplateId;
                await _repo.CreateCampaignWhatsappTemplate(ci, token);
            }
            else
            {
                if (inv.IsBot.HasValue && inv.IsBot.Value)
                {
                    throw new IncorrectRequestException("Can't update existing BOT whatsapp template");
                }

                ci.Body = request.Body ?? string.Empty;
                ci.SenderName = request.SenderName ?? string.Empty;
                ci.TemplateId = request.TemplateId;
                ci.Id = await _repo.UpdateCampaignWhatsappTemplate(ci, token);
            }

            await _mediator.Publish(new CampaignInvitationCreatedOrUpdated(ci.Id), token);
            return ci.Id;
        }


        #region extract image from body and save to azure
        private async Task<string> UploadBodyImages(string body, CancellationToken token)
        {
            var images = GetImagesInHTMLString(body);
            foreach (var img in images)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                body = body.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }
            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            return newBody;
        }

        private List<string> GetImagesInHTMLString(string htmlString)
        {
            List<string> images = new List<string>();
            string pattern = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        #endregion

        /// <summary>
        /// Upload new image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadNewImage(string backgroundImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(backgroundImagePath))
            {
                string res = CutExtension(backgroundImagePath);

                var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.{res}";
                await _mediator.Send(new UploadFileCmd(path, backgroundImagePath, FileType.Picture), token);

                return path;
            }

            return string.Empty;
        }


        /// <summary>
        /// Override image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath))
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {
                    var path = string.IsNullOrEmpty(existingImagePath) ? $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.{res}" : existingImagePath;
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);
                    return path;
                }
                return "";
            }

            return string.Empty;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }
    }
}
