﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using Irms.Application.Files.Commands;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class CopyInvitationTemplateHandler : IRequestHandler<CopyInvitationTemplateCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly ICurrentUser _user;
        private readonly IFileUploadService _fileUploadService;

        public CopyInvitationTemplateHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            ICurrentUser user,
            IFileUploadService fileUploadService
            )
        {
            _repo = repo;
            _mediator = mediator;
            _user = user;
            _fileUploadService = fileUploadService;
        }

        public async Task<Guid> Handle(CopyInvitationTemplateCmd request, CancellationToken token)
        {
            if (request.CopyTemplateType == CopyTemplateType.SmsToWhatsapp)
            {
                var id = await CopySmsTemplateToWhatsapp(request.CampaignInvitationId, token);
                await _mediator.Publish(new InvitationTemplateCopied(id), token);
                return id;
            }
            else if (request.CopyTemplateType == CopyTemplateType.WhatsappToSms)
            {
                var id = await CopyWhatsappTemplateToSms(request.CampaignInvitationId, token);
                await _mediator.Publish(new InvitationTemplateCopied(id), token);
                return id;
            }

            return Guid.Empty;
        }

        private async Task<Guid> CopySmsTemplateToWhatsapp(Guid campaignInvitationId, CancellationToken token)
        {
            var sms = await _repo.GetCampaignSmsTemplateByInvitationId(campaignInvitationId, token);
            if (sms == null)
            {
                throw new IncorrectRequestException("Sms template not found for invitation.");
            }

            var existingWhatsapp = await _repo.GetCampaignWhatsappTemplateByInvitationId(campaignInvitationId, token);
            if (existingWhatsapp == null)
            {
                throw new IncorrectRequestException("WhatsApp template not found for invitation.");
            }

            existingWhatsapp.WelcomeHtml = sms.WelcomeHtml;
            existingWhatsapp.ProceedButtonText = sms.ProceedButtonText;
            existingWhatsapp.Rsvphtml = sms.RSVPHtml;
            existingWhatsapp.AcceptButtonText = sms.AcceptButtonText;
            existingWhatsapp.RejectButtonText = sms.RejectButtonText;
            existingWhatsapp.AcceptHtml = sms.AcceptHtml;
            existingWhatsapp.RejectHtml = sms.RejectHtml;
            existingWhatsapp.ThemeJson = sms.ThemeJson;
            existingWhatsapp.BackgroundImagePath = await CloneImage(sms.BackgroundImagePath, token);
            existingWhatsapp.ModifiedOn = DateTime.UtcNow;
            existingWhatsapp.ModifiedById = _user.Id;

            var id = await _repo.CopyCampaignWhatsappTemplate(existingWhatsapp, token);
            return id;
        }

        private async Task<Guid> CopyWhatsappTemplateToSms(Guid campaignInvitationId, CancellationToken token)
        {
            var whatsapp = await _repo.GetCampaignWhatsappTemplateByInvitationId(campaignInvitationId, token);
            if (whatsapp == null)
            {
                throw new IncorrectRequestException("Whatsapp template not found for invitation.");
            }

            var existingsms = await _repo.GetCampaignSmsTemplateByInvitationId(campaignInvitationId, token);
            if (existingsms == null)
            {
                throw new IncorrectRequestException("Sms template not found for invitation.");
            }

            existingsms.WelcomeHtml = whatsapp.WelcomeHtml;
            existingsms.ProceedButtonText = whatsapp.ProceedButtonText;
            existingsms.RSVPHtml = whatsapp.Rsvphtml;
            existingsms.AcceptButtonText = whatsapp.AcceptButtonText;
            existingsms.RejectButtonText = whatsapp.RejectButtonText;
            existingsms.AcceptHtml = whatsapp.AcceptHtml;
            existingsms.RejectHtml = whatsapp.RejectHtml;
            existingsms.ThemeJson = whatsapp.ThemeJson;
            existingsms.BackgroundImagePath = await CloneImage(whatsapp.BackgroundImagePath, token);

            await _repo.UpdateCampaignSmsTemplate(existingsms, token);
            return existingsms.Id;
        }

        private async Task<string> CloneImage(string backgroundImagePath, CancellationToken token)
        {

            if (!string.IsNullOrEmpty(backgroundImagePath))
            {
                var file = await _fileUploadService.LoadFile(backgroundImagePath);
                string res = Path.GetExtension(backgroundImagePath);

                var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignWhatsapp)}{Guid.NewGuid()}_image.{res}";
                await _mediator.Send(new UploadFileCmd(path, Convert.ToBase64String(file), FileType.Picture), token);

                return path;
            }

            return string.Empty;
        }
    }
}
