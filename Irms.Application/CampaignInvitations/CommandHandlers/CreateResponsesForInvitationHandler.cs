﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class CreateResponsesForInvitationHandler : IRequestHandler<CreateResponsesForInvitationCmd, Guid>
    {
        private readonly ICampaignInvitationResponseRepository<CampaignInvitationResponse, Guid> _repo;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _campaignInvitation;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;
        private readonly ITransactionManager _transaction;
        private readonly IUniqueUrlGenerator _urlGenerator;
        public CreateResponsesForInvitationHandler(
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository,
            ICampaignInvitationRepository<CampaignInvitation, Guid> campaignInvitation,
            ICampaignInvitationResponseRepository<CampaignInvitationResponse, Guid> repo,
            IMediator mediator,
            ITransactionManager transaction,
            IUniqueUrlGenerator urlGenerator
        )
        {
            _repo = repo;
            _mediator = mediator;
            _transaction = transaction;
            _contactRepository = contactRepository;
            _campaignInvitation = campaignInvitation;
            _urlGenerator = urlGenerator;
        }

        /// <summary>
        /// Creates responses for invitation
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateResponsesForInvitationCmd request, CancellationToken token)
        {
            var contacts = await _repo.GetContactListsByInvitationId(request.InvitationId, token);

            using (var tr = await _transaction.BeginTransactionAsync(token))
            {
                var responses = contacts.Select(x =>
                {
                    return new CampaignInvitationResponse()
                    {
                        CampaignInvitationId = request.InvitationId,
                        ContactId = x.ContactId,
                        TenantId = x.TenantId,
                    };
                }).ToList();

                await _repo.CreateRange(responses, token);
                tr.Commit();
            }

            await _mediator.Publish(new CampaignResponsesCreated(request.InvitationId), token);
            return request.InvitationId;
        }
    }
}
