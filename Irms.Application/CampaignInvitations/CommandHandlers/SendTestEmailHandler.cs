﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CampaignInvitations;
using System.Linq;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using System;
using Irms.Domain;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class SendTestEmailHandler : IRequestHandler<SendTestEmailCmd, bool>
    {
        private readonly IMediator _mediator;
        private readonly ICampaignInvitationTestNotifier _notifier;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;

        public SendTestEmailHandler(ICampaignInvitationRepository<CampaignInvitation,
            Guid> repo,
            IMediator mediator,
            ICampaignInvitationTestNotifier notifier
            )
        {
            _repo = repo;
            _mediator = mediator;
            _notifier = notifier;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> Handle(SendTestEmailCmd message, CancellationToken token)
        {
            var template = await _repo.GetCampaignEmailTemplateById(message.Id, token);

            if (template == null)
            {
                throw new IncorrectRequestException("Template was not found");
            }

            var msg = new InvitationMessage
            {
                Sender = template.SenderEmail,
                Subject = template.Subject,
                Body = template.Body.ReplaceDefaultValuePlaceholders(),
                Recipients = message.Recipients.Select(x => new EventGuest
                {
                    Email = x
                }).ToList()
            };

            _ = await _notifier.NotifyByEmail(msg, token);
            await _mediator.Publish(new TestEmailSent(message.Id), token);
            return true;
        }
    }
}