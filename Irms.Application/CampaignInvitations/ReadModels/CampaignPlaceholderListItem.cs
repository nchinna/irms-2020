﻿namespace Irms.Application.Campaigns.ReadModels
{
    public class CampaignPlaceholderListItem
    {
        public CampaignPlaceholderListItem(int id,
            string title,
            string placeholder)
        {
            Id = id;
            Title = title;
            Placeholder = placeholder;
        }
        public int Id { get; }
        public string Title { get; }
        public string Placeholder { get; }
    }
}
