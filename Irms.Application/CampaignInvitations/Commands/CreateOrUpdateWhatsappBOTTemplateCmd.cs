﻿using MediatR;
using System;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class CreateOrUpdateWhatsappBOTTemplateCmd : IRequest<Guid>
    {
        public Guid CampaignInvitationId { get; set; }
        public string Body { get; set; }
        public string AcceptedResponse { get; set; }
        public string RejectedResponse { get; set; }
        public string DefaultResponse { get; set; }
        public Guid? TemplateId { get; set; }
        public string SenderName { get; set; }
    }
}
