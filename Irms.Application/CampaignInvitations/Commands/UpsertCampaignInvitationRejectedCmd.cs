﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class UpsertCampaignInvitationRejectedCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public Guid EventCampaignId { get; set; }
        public string Title { get; set; }
        public bool IsInstant { get; set; }
        public int? Interval { get; set; }
        public int SortOrder { get; set; }
        public IntervalType? IntervalType { get; set; }
    }
}
