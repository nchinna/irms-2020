﻿using System;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class UpdateCampaignEmailResponseFormCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string FormSettings { get; set; }
        public string BackgroundImage { get; set; }
    }
}
