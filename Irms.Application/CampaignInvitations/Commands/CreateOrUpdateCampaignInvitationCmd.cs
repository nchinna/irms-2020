﻿using System;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class CreateOrUpdateCampaignInvitationCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public Guid CampaignId { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
    }
}
