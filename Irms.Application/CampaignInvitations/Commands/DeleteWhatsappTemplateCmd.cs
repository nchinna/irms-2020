﻿using MediatR;
using System;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class DeleteWhatsappTemplateCmd : IRequest<Unit>
    {
        public Guid InvitationId { get; set; }
    }
}