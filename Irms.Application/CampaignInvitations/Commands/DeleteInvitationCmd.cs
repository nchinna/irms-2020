﻿using MediatR;
using System;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class DeleteInvitationCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
    }
}
