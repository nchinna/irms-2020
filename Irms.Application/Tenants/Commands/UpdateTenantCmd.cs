﻿using System;
using MediatR;

namespace Irms.Application.Tenants.Commands
{
    public class UpdateTenantCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
        public Guid CountryId { get; set; }
        public string City { get; set; }
        public bool IsActive { get; set; }
    }
}
