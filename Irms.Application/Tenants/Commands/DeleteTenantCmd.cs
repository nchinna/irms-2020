﻿using System;
using System.Collections.Generic;
using MediatR;
using Newtonsoft.Json;

namespace Irms.Application.Tenants.Commands
{
    public class DeleteTenantCmd : IRequest<Unit>
    {
        [JsonConstructor]
        public DeleteTenantCmd(List<Guid> tenantIds)
        {
            TenantIds = tenantIds;
        }

        public List<Guid> TenantIds { get; }
    }
}
