﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Tenants.Commands;
using Irms.Application.Tenants.Events;
using Irms.Domain.Entities.Tenant;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using AutoMapper;
using Irms.Domain;
using Irms.Application.Files.Commands;

namespace Irms.Application.Tenants.CommandHandlers
{
    public class UpdateTenantHandler : IRequestHandler<UpdateTenantCmd, Unit>
    {
        private readonly ITenantRepository<Tenant, Guid> _repository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpdateTenantHandler(
            ITenantRepository<Tenant, Guid> repository,
            IMediator mediator,
            IMapper mapper)
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// update tenant basic info handler
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateTenantCmd message, CancellationToken token)
        {
            var tenant = await _repository.GetById(message.Id, token);
            _mapper.Map(message, tenant);
            tenant.CanActivate();

            //upload logo to azure storage
            string fileName = string.Empty;
            if (message.Logo != null)
            {

                //delete file from azure
                if (tenant.LogoPath.IsNotNullOrEmpty())
                {
                    await _mediator.Send(new DeleteFileCmd(tenant.LogoPath), token);
                }

                //add new logo
                fileName = $"{FilePath.GetFileDirectory(SystemModule.Customers)}{Guid.NewGuid()}_logo.png";
                await _mediator.Send(new UploadFileCmd(fileName, message.Logo, FileType.Picture), token);

                tenant.SetLogoPath(fileName);
            }

            await _repository.Update(tenant, token);

            var e = new TenantUpdated(tenant.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
