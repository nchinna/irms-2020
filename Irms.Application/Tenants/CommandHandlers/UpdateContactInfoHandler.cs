﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Tenants.Commands;
using Irms.Application.Tenants.Events;
using Irms.Domain.Entities.Tenant;
using Irms.Application.Abstract.Repositories;
using AutoMapper;

namespace Irms.Application.Tenants.CommandHandlers
{
    public class UpdateContactInfoHandler : IRequestHandler<UpdateContactInfoCmd, Unit>
    {
        private readonly ITenantRepository<Tenant, Guid> _repository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpdateContactInfoHandler(
            ITenantRepository<Tenant, Guid> repository,
            IMediator mediator,
            IMapper mapper)
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// update tenant basic info handler
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateContactInfoCmd message, CancellationToken token)
        {
            var contactInfo = _mapper.Map<TenantContactInfo>(message);
            await _repository.UpdateContactInfo(contactInfo, token);

            var e = new TenantContactInfoUpdated(contactInfo.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
