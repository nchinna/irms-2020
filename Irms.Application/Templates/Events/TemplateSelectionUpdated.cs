﻿using Irms.Application.Abstract;
using Irms.Domain.Entities.Templates;
using System;

namespace Irms.Application.Templates.Events
{
    public class TemplateSelectionUpdated : IEvent
    {
        public TemplateSelectionUpdated(TemplateType templateType, Guid? emailTemplateId, Guid? smsTemplateId)
        {
            TemplateType = templateType;
            EmailTemplateId = emailTemplateId;
            SmsTemplateId = smsTemplateId;
        }

        public TemplateType TemplateType { get; }
        public Guid? EmailTemplateId { get; }
        public Guid? SmsTemplateId { get; }

        public Guid ObjectId => EmailTemplateId ?? Guid.Empty;
        public Guid? ObjectId2 => SmsTemplateId;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) =>
            $"The template settings for type {TemplateType.ToString()} has been changed by {u}. "
            + $"Email template: {x(EmailTemplateId ?? default)}, Sms template: {x(SmsTemplateId ?? default)}";
    }
}
