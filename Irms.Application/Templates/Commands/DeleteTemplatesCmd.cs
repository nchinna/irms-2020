﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Templates.Commands
{
    /// <summary>
    /// Request object based on which an SMS or Email template will delated
    /// </summary>
    public class DeleteTemplatesCmd : IRequest<Unit>
    {
        public DeleteTemplatesCmd(IEnumerable<Guid> ids)
        {
            Ids = ids;
        }

        public IEnumerable<Guid> Ids { get; }
    }
}
