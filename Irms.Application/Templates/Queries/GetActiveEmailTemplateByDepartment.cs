﻿using Irms.Domain.Entities.Templates;
using MediatR;
using System;

namespace Irms.Application.Templates.Queries
{
    /// <summary>
    /// Request object based on which handler return active Email template based on DepartmentId
    /// </summary>
    public class GetActiveEmailTemplateByDepartment : IRequest<Template>
    {
        public GetActiveEmailTemplateByDepartment(TemplateType templateType, Guid tenantId)
        {
            TemplateType = templateType;
            TenantId = tenantId;
        }

        public TemplateType TemplateType { get; }
        public Guid TenantId { get; }
    }
}
