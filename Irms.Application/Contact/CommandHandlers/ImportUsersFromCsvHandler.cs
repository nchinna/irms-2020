﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.CustomFields.ReadModels;
using Irms.Application.Contact.Commands;
using Irms.Application.Tenants.Commands;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;

namespace Irms.Application.Contact.CommandHandlers
{
    public class ImportUsersFromCsvHandler : IRequestHandler<ImportUsersFromCsvCmd, Unit>
    {
        private readonly string[] _defaultFields = new string[] { "Title", "FullName", "PrefferedName", "Gender", "Email", "AlternativeEmail", "MobileNumber", "SecondaryMobileNumber", "Nationality", "WorkNumber", "DocumentType", "DocumentNumber", "IssuingCountry", "ExpirationDate", "Organization", "Position" };

        private readonly TenantBasicInfo _tenant;
        private readonly IMediator _mediator;
        private readonly ICustomFieldRepository<CustomField, Guid> _customFieldsRepo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepo;

        private KeyValuePair<string, Guid>[] _nationalities;
        private KeyValuePair<string, Guid>[] _countries;
        private KeyValuePair<string, int>[] _documents;
        private List<Domain.Entities.Contact> _contacts = new List<Domain.Entities.Contact>();
        public ImportUsersFromCsvHandler(
            TenantBasicInfo tenant,
            IMediator mediator,
            ICustomFieldRepository<CustomField, Guid> customFieldsRepo,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepo
        )
        {
            _tenant = tenant;
            _mediator = mediator;
            _customFieldsRepo = customFieldsRepo;
            _contactRepo = contactRepo;
        }

        public async Task<Unit> Handle(ImportUsersFromCsvCmd request, CancellationToken token)
        {
            // load custom fields
            var customFields = await _customFieldsRepo.Load(token);

            _nationalities = (await _customFieldsRepo.LoadNationalities(token))
                .ToArray();

            _documents = (await _customFieldsRepo.LoadDocumentTypes(token))
                .ToArray();

            _countries = (await _customFieldsRepo.LoadCountries(token))
                .ToArray();

            var contactEmails = request.Contacts.Select(x => x["Email"]).ToList();
            var contactMobileNumbers = request.Contacts.Select(x => x["MobileNumber"]).ToList();

            var existingCustomFieldValues = await _customFieldsRepo.LoadCustomFieldValues(contactEmails, contactMobileNumbers, token);

            var contacts = await _customFieldsRepo.LoadContacts(token);

            var contactsToAdd = new List<Domain.Entities.Contact>();
            var contactsToUpdate = new List<Domain.Entities.Contact>();
            var customFieldsToAdd = new List<ContactCustomField>();
            var customFieldsToUpdate = new List<ContactCustomField>();

            // create contacts
            var rawContacts = request.Contacts;
            foreach (var rawContact in rawContacts)
            {
                var contact = contacts.FirstOrDefault(x => x.Email == rawContact["Email"] || x.MobileNumber == rawContact["MobileNumber"]);
                if (contact != null)
                {
                    // update contact
                    UpdateContact(contact, rawContact, customFields, customFieldsToAdd, customFieldsToUpdate, existingCustomFieldValues);
                    contactsToUpdate.Add(contact);
                }
                else
                {
                    // add contact
                    contactsToAdd.Add(AddContact(rawContact, customFields, customFieldsToAdd));
                }
            }

            if (contactsToAdd.Count > 0)
                await _contactRepo.CreateRange(contactsToAdd, token);

            if (contactsToUpdate.Count > 0)
                await _contactRepo.UpdateRange(contactsToUpdate, token);

            if (customFieldsToAdd.Count > 0)
                await _customFieldsRepo.CreateCustomFieldValueRange(customFieldsToAdd, token);

            if (customFieldsToUpdate.Count > 0)
                await _customFieldsRepo.UpdateCustomFieldValueRange(customFieldsToUpdate, token);

            //ADD THIS FEATURE
            // assign to list if needed to, with ContactList or ContactListToContacts entities creation
            _contacts.AddRange(contactsToAdd);
            _contacts.AddRange(contactsToUpdate);
            var kvp = new KeyValuePair<string, string>(request.ListInfo.ToArray().First().Value, request.ListInfo.ToArray().Last().Value);
            await FillList(kvp, token);
            return Unit.Value;
        }

        private async Task FillList(KeyValuePair<string, string> data, CancellationToken token)
        {
            switch (data.Key)
            {
                case "new":
                    var createCmd = new CreateGuestListCmd(data.Value, null);
                    var id = await _mediator.Send(createCmd, token);
                    await _mediator.Send(new AddGuestsInGuestListCmd
                    {
                        ListId = id.Value,
                        GuestIds = _contacts.Select(x => x.Id).ToList()
                    }, token);
                    break;

                case "existing":
                case "eventGuest":
                    var listId = data.Value;
                    await _mediator.Send(new AddGuestsInGuestListCmd
                    {
                        ListId = Guid.Parse(listId),
                        GuestIds = _contacts.Select(x => x.Id).ToList()
                    }, token);
                    break;

                case "add":
                default:
                    break;
            }
        }

        private Domain.Entities.Contact AddContact(Dictionary<string, string> rawContact, IEnumerable<CustomField> customFields, List<ContactCustomField> customFieldsToAdd)
        {
            var contact = new Domain.Entities.Contact();
            contact.Create(_tenant.Id);
            contact.CreatedOn = DateTime.UtcNow;

            var fields = rawContact.ToArray();
            foreach (var field in fields)
            {
                if (_defaultFields.Contains(field.Key))
                {
                    // default field
                    FillContact(contact, field.Key, field.Value);
                }
                else
                {
                    // custom field
                    var customFieldValue = new ContactCustomField();
                    customFieldValue.Create();
                    customFieldValue.ContactId = contact.Id;
                    customFieldValue.CustomFieldId = customFields.FirstOrDefault(x => x.FieldName == field.Key).Id;
                    customFieldValue.Value = field.Value;
                    customFieldsToAdd.Add(customFieldValue);
                }

            }

            return contact;
        }

        private void UpdateContact(Domain.Entities.Contact contact, Dictionary<string, string> rawContact, IEnumerable<CustomField> customFields, List<ContactCustomField> customFieldsToAdd, List<ContactCustomField> customFieldsToUpdate, IEnumerable<ContactCustomField> existingCustomFields)
        {
            var fields = rawContact.ToArray();
            foreach (var field in fields)
            {
                if (_defaultFields.Contains(field.Key))
                {
                    // default field
                    FillContact(contact, field.Key, field.Value);
                }
                else
                {
                    // custom field
                    var customFieldId = customFields.FirstOrDefault(x => x.FieldName.Equals(field.Key, StringComparison.OrdinalIgnoreCase)).Id;
                    var existingCustomField = existingCustomFields.FirstOrDefault(x => x.ContactId == contact.Id && x.CustomFieldId == customFieldId);

                    if (existingCustomField == null)
                    {
                        // Add custom field
                        var customFieldValue = new ContactCustomField();
                        customFieldValue.Create();
                        customFieldValue.CustomFieldId = customFields.FirstOrDefault(x => x.FieldName.Equals(field.Key, StringComparison.OrdinalIgnoreCase)).Id;
                        customFieldValue.Value = field.Value;
                        customFieldValue.ContactId = contact.Id;
                        customFieldsToAdd.Add(customFieldValue);
                    }
                    else
                    {
                        // Update custom field
                        existingCustomField.Value = field.Value;
                        customFieldsToUpdate.Add(existingCustomField);
                    }
                }
            }
        }

        private void FillContact(Domain.Entities.Contact contact, string field, string value = "")
        {
            switch (field)
            {
                case "Title":
                    contact.Title = value;
                    break;

                case "FullName":
                    contact.FullName = value;
                    break;

                case "PrefferedName":
                    contact.PreferredName = value;
                    break;

                case "Gender":
                    if (value == "Male")
                        contact.GenderId = 0;
                    else
                        contact.GenderId = 1;
                    break;

                case "Email":
                    contact.Email = value;
                    break;

                case "AlternativeEmail":
                    contact.AlternativeEmail = value;
                    break;

                case "MobileNumber":
                    contact.MobileNumber = value;
                    break;

                case "WorkNumber":
                    contact.WorkNumber = value;
                    break;

                case "SecondaryMobileNumber":
                    contact.SecondaryMobileNumber = value;
                    break;

                case "DocumentNumber":
                    contact.DocumentNumber = value;
                    break;

                case "Organization":
                    contact.Organization = value;
                    break;

                case "Position":
                    contact.Position = value;
                    break;

                case "DocumentType":
                    var type = value?.ToLower() ?? string.Empty;
                    var selectedDocument = _documents.Where(x => x.Key.Contains(type)).ToList();
                    if (selectedDocument.Count() > 0 && type.IsNotNullOrEmpty())
                    {
                        contact.DocumentTypeId = selectedDocument.First().Value;
                    }
                    break;

                case "ExpirationDate":
                    if (value.IsNotNullOrEmpty())
                    {
                        contact.ExpirationDate = DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture); //Convert.ToDateTime(value);
                    }
                    break;

                case "IssuingCountry":
                    var country = value?.ToLower() ?? string.Empty;
                    var selectedCountries = _countries.Where(x => x.Key.Contains(country)).ToList();
                    if (selectedCountries.Count() > 0 && country.IsNotNullOrEmpty())
                    {
                        contact.IssuingCountryId = selectedCountries.First().Value;
                    }
                    break;

                case "Nationality":
                    var nationality = value?.ToLower() ?? string.Empty;
                    var selectedNationality = _nationalities.Where(x => x.Key.Contains(nationality)).ToList();
                    if (selectedNationality.Count() > 0 && nationality.IsNotNullOrEmpty())
                    {
                        contact.NationalityId = selectedNationality.First().Value;
                    }
                    break;

                default:
                    return;
            }
        }
    }
}
