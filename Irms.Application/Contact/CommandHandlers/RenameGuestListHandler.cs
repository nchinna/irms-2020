﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class RenameGuestListHandler : IRequestHandler<RenameGuestListCmd, Guid>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        public RenameGuestListHandler(TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator,
            IMapper mapper)
        {
            _contactListRepository = contactListRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// renames guest list
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(RenameGuestListCmd request, CancellationToken cancellationToken)
        {
            var contactList = await _contactListRepository.GetById(request.Id, cancellationToken);

            contactList.Name = request.Name;

            await _contactListRepository.Update(contactList, cancellationToken);

            var guestListRenamed = new GuestListRenamed(request.Id, request.Name);

            await _mediator.Publish(guestListRenamed, cancellationToken);
            return contactList.Id;
        }
    }
}
