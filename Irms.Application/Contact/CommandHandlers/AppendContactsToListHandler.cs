﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Domain.Entities.Tenant;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class AppendContactsToListHandler : IRequestHandler<AppendContactsToListCmd, Unit>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _repo;
        private readonly IContactListToContactsRepository<ContactListToContact, Guid> _contactListToContactsRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        public AppendContactsToListHandler(
            IContactRepository<Domain.Entities.Contact, Guid> repo,
            IContactListToContactsRepository<ContactListToContact, Guid> contactListToContactsRepository,
            IMediator mediator,
            IMapper mapper,
            TenantBasicInfo tenant)
        {
            _repo = repo;
            _contactListToContactsRepository = contactListToContactsRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// Appends contacts to a new list if they are not already in it
        /// </summary>
        /// <param name="request">list id and contact ids</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(AppendContactsToListCmd request, CancellationToken token)
        {
            var contacts = await _repo.GetByIds(request.ContactIds, token);
            var contactListToContacts = await _contactListToContactsRepository.GetAllForContactList(request.ListId, token);

            var newContactLists = new List<ContactListToContact>();

            contacts
                .ToList()
                .ForEach(x =>
                {
                    var existingEntity = contactListToContacts.FirstOrDefault(y => y.ContactId == x.Id);
                    if (existingEntity == null)
                    {
                        newContactLists.Add(new ContactListToContact
                        {
                            Id = Guid.NewGuid(),
                            ContactId = x.Id,
                            ContactListId = request.ListId,
                            CreatedOn = DateTime.UtcNow,
                            TenantId = _tenant.Id,
                            CreatedById = _tenant.Id
                        });
                    }
                });

            if (newContactLists.Count == 0)
            {
                throw new IncorrectRequestException("All contacts are already is on the list.");
            }

            await _contactListToContactsRepository.CreateRange(newContactLists, token);
            await _mediator.Publish(new ContactsAppendedToList(request.ListId, request.ContactIds), token);

            return Unit.Value;
        }
    }
}
