﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Events
{
    public class RfiQuestionsAsked : IEvent
    {
        public RfiQuestionsAsked(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Rfi for contact list with id {x(Id)} was created {u}";
    }
}
