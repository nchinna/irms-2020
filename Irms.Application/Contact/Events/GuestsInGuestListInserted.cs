﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Contact.Events
{
    public class GuestsInGuestListInserted : IEvent
    {
        public GuestsInGuestListInserted(Guid guestListId)
        {
            GuestListIdListId = guestListId;
        }

        public Guid GuestListIdListId { get; }

        public Guid ObjectId => GuestListIdListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact list  {x(GuestListIdListId)} has been updated by {u}";
    }
}
