﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;

namespace Irms.Application.Contact.Events
{
    public class ContactsDeleted : IEvent
    {
        public ContactsDeleted(List<Guid> ids)
        {
            Ids = ids;
        }
        public List<Guid> Ids { get; }

        public Guid ObjectId => Guid.Empty;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contacts  {string.Join(",", Ids.ToArray())} has been deleted by {u}";
    }
}
