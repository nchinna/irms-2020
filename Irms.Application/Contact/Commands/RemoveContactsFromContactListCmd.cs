﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Contact.Commands
{
    public class RemoveContactsFromContactListCmd : IRequest<Unit>
    {
        public IEnumerable<Guid> ContactIds { get; set; }
        public Guid ContactListId { get; set; }
    }
}
