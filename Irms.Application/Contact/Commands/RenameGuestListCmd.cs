﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Commands
{
    public class RenameGuestListCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
