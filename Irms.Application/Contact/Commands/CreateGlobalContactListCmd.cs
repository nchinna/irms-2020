﻿using System;
using MediatR;

namespace Irms.Application.Contact.Commands
{
    public class CreateGlobalContactListCmd : IRequest<Guid?>
    {
        public string Name { get; set; }
    }
}
