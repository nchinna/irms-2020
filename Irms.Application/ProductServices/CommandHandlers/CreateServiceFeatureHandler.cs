﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.ProductServices.Commands;
using Irms.Application.ProductServices.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.ProductServices.CommandHandlers
{
    public class CreateServiceFeatureHandler : IRequestHandler<CreateServiceFeatureCmd, Guid>
    {
        private readonly IMediator _mediator;
        private readonly IServiceRepository<ServiceCatalog, Guid> _repository;
        public CreateServiceFeatureHandler(IMediator mediator, IServiceRepository<ServiceCatalog, Guid> repository)
        {
            _mediator = mediator;
            _repository = repository;
        }
        public async Task<Guid> Handle(CreateServiceFeatureCmd request, CancellationToken cancellationToken)
        {
            var serviceCatalog = await _repository.GetById(request.ServiceId, cancellationToken);
            if (serviceCatalog == null)
            {
                throw new IncorrectRequestException($"ServiceCatalog not found by id: {request.ServiceId}");
            }
            serviceCatalog.ServiceCatalogFeatures = new List<ServiceCatalogFeature>();
            serviceCatalog.ServiceCatalogFeatures.Add(new ServiceCatalogFeature(
                request.ServiceFeature.FeatureId, request.ServiceFeature.FeatureTitle,
                request.ServiceFeature.IsFeatureEnabled, request.ServiceFeature.IsCustom));

            Guid id = await _repository.CreateServiceFeature(serviceCatalog, cancellationToken);

            await _mediator.Publish(new ServiceFeatureCreated(request.ServiceId), cancellationToken);
            return id;
        }
    }
}
