﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Subscriptions.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Subscription.Queries;
using Irms.Data.Read.Subscription.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Irms.WebApi.AdminControllers
{
    [AuthorizeRoles(RoleType.SuperAdmin)]
    [Route("api/[controller]")]
    public class SubscriptionController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SubscriptionController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// get subscriptions list by filters, pagination
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public async Task<IPageResult<SubscriptionListItem>> GetSubscriptionList([FromBody]GetSubscriptionListQuery query, CancellationToken token)
        {
            var subscriptions = await _mediator.Send(query, token);
            return subscriptions;
        }

        /// <summary>
        /// create subscription
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> Create([FromBody]CreateSubscriptionCmd cmd, CancellationToken token)
        {
            var id = await _mediator.Send(cmd, token);
            return id;
        }
    }
}
