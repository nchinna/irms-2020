﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.Campaign.Commands
{
    public class CreateCampaignCmdValidator : AbstractValidator<CreateCampaignCmd>
    {
        public CreateCampaignCmdValidator()
        {
            RuleFor(x => x.EventId)
                .NotEmpty();
        }
    }
}
