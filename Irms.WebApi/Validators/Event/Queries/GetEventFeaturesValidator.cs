﻿using FluentValidation;
using Irms.Data.Read.Event.Queries;

namespace Irms.WebApi.Validators.Event.Queries
{
    public class GetEventFeaturesValidator : AbstractValidator<GetEventFeatures>
    {
        public GetEventFeaturesValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
