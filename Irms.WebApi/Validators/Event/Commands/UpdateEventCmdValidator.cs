﻿using System;
using FluentValidation;
using Irms.Application.Events.Commands;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class UpdateEventCmdValidator : AbstractValidator<UpdateEventCmd>
    {
        public UpdateEventCmdValidator()
        {
            //basic info
            RuleFor(e => e.Id)
                .NotEmpty();

            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(200);

            //RuleFor(e => e.TimeZoneUtcOffset)
            // .NotEmpty()
            // .MaximumLength(10);

            //RuleFor(e => e.TimeZoneName)
            //    .NotEmpty()
            //    .MaximumLength(100);

            RuleFor(e => e.StartDateTime)
                .NotEmpty();

            RuleFor(e => e.EndDateTime)
                .NotEmpty();

            //RuleFor(x => x.Image)
            //     .Must(IsBase64Encoded).WithMessage("Logo icon is not a valid Base64");

            //location info
            RuleFor(e => e.LocationName)
                .NotEmpty()
                .MaximumLength(500);

            RuleFor(e => e.City)
                .MaximumLength(200);

            RuleFor(e => e.Region)
                .MaximumLength(200);

            RuleFor(e => e.ZipCode)
                .MaximumLength(20);

            RuleFor(e => e.Latitude)
                .NotEmpty();

            RuleFor(e => e.Longitude)
                .NotEmpty();

            RuleFor(e => e.Country)
                .MaximumLength(3);
        }

        private bool IsBase64Encoded(string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str))
                {
                    return true;
                }

                // If no exception is caught, then it is possibly a base64 encoded string
                byte[] data = Convert.FromBase64String(str.Split(',')[1]);
                // The part that checks if the string was properly padded to the
                // correct length was borrowed from d@anish's solution
                return (str.Split(',')[1].Replace(" ", "").Length % 4 == 0);
            }
            catch
            {
                // If exception is caught, then it is not a base64 encoded string
                return false;
            }
        }
    }
}
