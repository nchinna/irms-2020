﻿using FluentValidation;
using Irms.Application.Events.Commands;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class UpdateEventFeaturesCmdValidator : AbstractValidator<UpdateEventFeaturesCmd>
    {
        public UpdateEventFeaturesCmdValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty();

            RuleForEach(e => e.Features)
                .NotEmpty();
        }
    }
}
