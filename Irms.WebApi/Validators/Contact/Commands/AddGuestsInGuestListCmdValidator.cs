﻿using FluentValidation;
using Irms.Application.Contact.Commands;

namespace Irms.WebApi.Validators.Contact.Commands
{
    public class AddGuestsInGuestListCmdValidator : AbstractValidator<AddGuestsInGuestListCmd>
    {
        public AddGuestsInGuestListCmdValidator()
        {
            RuleFor(x => x.ListId)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.GuestIds)
                .NotNull()
                .NotEmpty();                
        }
    }
}
