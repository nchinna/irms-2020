﻿using FluentValidation;
using Irms.Data.Read.Contact.Queries;

namespace Irms.WebApi.Validators.Contact.Queries
{
    public class IsEmailUniqueValidator : AbstractValidator<IsEmailUnique>
    {
        public IsEmailUniqueValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty();
        }
    }
}
