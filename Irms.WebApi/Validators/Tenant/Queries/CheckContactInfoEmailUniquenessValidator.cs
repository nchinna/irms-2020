﻿using FluentValidation;
using Irms.Data.Read.Tenant.Queries;

namespace Irms.WebApi.Validators.Tenant.Queries
{
    public class CheckContactInfoEmailUniquenessValidator : AbstractValidator<CheckContactInfoEmailUniqueness>
    {
        public CheckContactInfoEmailUniquenessValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty();
        }
    }
}
