﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers.UserManagement
{
    public static class UserExtensions
    {
        public static IIdentityResult ToResult(this IdentityResult result)
        {
            return new IdResult(result);
        }
    }
}
