﻿using Irms.Application.Abstract;
using Irms.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers
{
    public class LanguageIds : ILanguageIds
    {
        private readonly ILanguageCache _cache;

        public LanguageIds(ILanguageCache cache)
        {
            _cache = cache;
        }

        public string EnCulture => "en-US";
        public string ArCulture => "ar-SA";

        public async Task<Guid> En()
        {
            var languages = await _cache.GetSystemLanguages();
            return languages.FirstOrDefault(x => x.Culture.EqualsIgnoreCase(EnCulture))?.Id
                   ?? throw new IncorrectRequestException("English language is absent in DB");
        }

        public async Task<Guid> Ar()
        {
            var languages = await _cache.GetSystemLanguages();
            return languages.FirstOrDefault(x => x.Culture.EqualsIgnoreCase(ArCulture))?.Id
                   ?? throw new IncorrectRequestException("Arabic language is absent in DB");
        }
    }
}
