﻿using Irms.Application.Files.Commands;

namespace Irms.WebApi.Helpers
{
    public class ImageSize
    {
        public ImageSize(int imgSize, int imgQuality, int tnSize, int tnQuality)
        {
            ImgSize = imgSize;
            ImgQuality = imgQuality;
            TnSize = tnSize;
            TnQuality = tnQuality;
        }

        //TODO: tweak the image size depending on the landing page / public portal design
        public static ImageSize Blog { get; } = new ImageSize(600, 90, 600, 75);
        public static ImageSize Gallery { get; } = new ImageSize(1800, 90, 300, 75);
        public static ImageSize Slideshow { get; } = new ImageSize(1400, 90, 1400, 75);
        public static ImageSize Event { get; } = new ImageSize(600, 90, 600, 75);

        public int ImgSize { get; }
        public int ImgQuality { get; }

        public int TnSize { get; }
        public int TnQuality { get; }

        //public UploadImageCmd ToUploadImageCmd(string name, string mimeType, byte[] data)
        //{
        //    return new UploadImageCmd(name, mimeType, data, ImgSize, ImgQuality, TnSize, TnQuality);
        //}
    }
}
