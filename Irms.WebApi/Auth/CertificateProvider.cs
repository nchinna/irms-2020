﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Irms.WebApi.Auth
{
    public static class CertificateProvider
    {
        public static IIdentityServerBuilder AddSigningCertificate(
            this IIdentityServerBuilder builder,
            IConfiguration cfg,
            IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                var thumbprint = cfg["IdentityServer:CertificateThumbprint"];
                if (string.IsNullOrWhiteSpace(thumbprint))
                {
                    throw new Exception("The thumbprint of the Identity Server signing certificate is not configured");
                }

                using (var certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser))
                {
                    certStore.Open(OpenFlags.ReadOnly);
                    var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
                    if (certCollection.Count == 0)
                    {
                        throw new Exception("Identity Server signing certificate was not found in the registry");
                    }

                    builder.AddSigningCredential(certCollection[0]);
                }
            }

            return builder;
        }
    }
}
