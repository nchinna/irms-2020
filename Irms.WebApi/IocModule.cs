﻿using Autofac;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Data.Abstract;
using Irms.WebApi.Helpers;
using Irms.WebApi.Helpers.UserManagement;
using Irms.WebApi.LocalizationTransform;
using Irms.WebApi.Resources;

namespace Irms.WebApi
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<CurrentUser>().As<ICurrentUser>();
            builder.RegisterType<Serializer>().As<ISerializer>();
            builder.RegisterType<UserManagerDecorator>().As<IUserManager>();
            builder.RegisterType<DefaultConnectionString>().As<IConnectionString>();
            builder.RegisterType<CurrentLanguage>().As<ICurrentLanguage>();

            //translators
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AssignableTo<ITranslator>()
                .AsImplementedInterfaces();


            //builder.RegisterType<ReCaptchaConfigurationProvider>().As<IReCaptchaConfigurationProvider>();
            builder.RegisterType<MessageTranslator>().As<IMessageTranslator>();

            builder.RegisterType<LanguageDetector>().Named<ILanguageDetector>("original");
            builder.RegisterType<LanguageDetectorWithRemap>().Named<ILanguageDetector>("decorator");
            builder.RegisterDecorator<ILanguageDetector>((c, inner) => c.ResolveNamed<ILanguageDetector>("decorator", TypedParameter.From(inner)), "original").As<ILanguageDetector>();


            //builder.RegisterType<BackgroundHostedService>().SingleInstance();
            //builder.Register(ctx => ctx.Resolve<BackgroundHostedService>()).As<IBackgroundHostedService>().SingleInstance();
        }
    }
}
