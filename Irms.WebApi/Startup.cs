using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Dapper;
using Irms.Data;
using Irms.WebApi.Helpers;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.AspNetCore.Identity;
using Irms.Data.IdentityClasses;
using Irms.WebApi.Auth;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Irms.WebApi.Resources;
using Irms.Application.Abstract.Services;
using Serilog;
using Irms.Application.Abstract;
using Autofac;
using Microsoft.IdentityModel.Logging;
using Irms.Application;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Hangfire;
using Hangfire.SqlServer;
using System.Threading;
using Irms.Background;

namespace Irms.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(
            IConfiguration configuration,
            IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conStr = _configuration.GetConnectionString("DefaultConnection");
            var jwtAuthority = _configuration["JwtAuthority:WebSite"];

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            //services.AddScoped<IMediator, Mediator>();
            services.AddLogging(o => o.AddConsole().AddDebug().SetMinimumLevel(LogLevel.Trace));

            services.AddMultitenancy<TenantBasicInfo, CachingAppTenantResolver>();

            services.AddDbContext<IrmsDataContext>(options => options.UseSqlServer(conStr), ServiceLifetime.Scoped);
            services.AddDbContext<IrmsTenantDataContext>(options => options.UseSqlServer(conStr), ServiceLifetime.Scoped);

            //// for background singleton jobs
            services.AddDbContext<IrmsDataBackgroundJobContext>(options => options.UseSqlServer(conStr), ServiceLifetime.Transient);

            services.AddScoped<ILanguageCache, LanguageCache>(); //one cache per http request
            services.AddScoped<ILanguageIds, LanguageIds>(); //one cache per http request

            SqlMapper.AddTypeHandler(new DateTimeHandler()); //UTC dates for Dapper

            void IdentityOptions(IdentityOptions o)
            {
                //password options
                o.Password.RequireDigit = true;
                o.Password.RequireLowercase = true;
                o.Password.RequireUppercase = true;
                o.Password.RequireNonAlphanumeric = true;
                o.Password.RequiredLength = 8;
                o.Password.RequiredUniqueChars = 1;
            }

            services
                .AddIdentity<ApplicationUser, ApplicationRole>(IdentityOptions)
                .AddEntityFrameworkStores<IrmsDataContext>()
                .AddDefaultTokenProviders();

            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromMinutes(15);
            });

            //TODO DELETE THIS ON PROD:
            IdentityModelEventSource.ShowPII = true;

            services
                .AddIdentityServer()
                .AddSigningCertificate(_configuration, _environment)
                .AddAspNetIdentity<ApplicationUser>()
                .AddOperationalStore(x => x.ConfigureDbContext = db => db.UseSqlServer(conStr))
                .AddInMemoryIdentityResources(IdentityServerConfig.GetIdentityResources())
                .AddInMemoryApiResources(IdentityServerConfig.GetApiResources())
                .AddInMemoryClients(IdentityServerConfig.GetClients());

            services.AddTransient<IResourceOwnerPasswordValidator, MultiTenantPasswordValidator>();

            services
                .AddAuthorization(x =>
                {
                    x.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser()
                        .Build();
                })
                .AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, jwt =>
                {
                    //TODO DELETE THIS ON PROD:
#if DEBUG
                    //jwt.BackchannelHttpHandler = new HttpClientHandler { ServerCertificateCustomValidationCallback = delegate { return true; } };
#endif
                    jwt.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerValidator = (issuer, token, parameters) =>
                        {
                            //TODO FIX THIS:
                            var provider = services.BuildServiceProvider();
                            var acc = provider.GetService<IHttpContextAccessor>();
                            var apiTenant = acc.HttpContext.Request.Host.Value;
                            var authTenant = new Uri(issuer).Host;
                            if (apiTenant != authTenant)
                            {
                                //throw new SecurityTokenInvalidIssuerException();
                            }
                            return issuer;
                        },
                        AudienceValidator = (audiences, token, parameters) => true
                    };
                    jwt.Audience = $"{jwtAuthority}/resources";
                    jwt.Authority = jwtAuthority;
                    jwt.RequireHttpsMetadata = false;
                });


            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });

            var validateAttribute = new ValidateModelAttribute(new LanguageDetector(), new MessageTranslator());

            services
                .AddMvcCore(x => x.Filters.Add(validateAttribute)) //validate models of all actions
                .AddAuthorization()
                .AddNewtonsoftJson(
                    t => t.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'"
                )
                .AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<IMarker>())
                .AddApiExplorer()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Takamul IRMS", Version = "v1", });

                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Password = new OpenApiOAuthFlow
                        {
                            TokenUrl = new Uri("/connect/token", UriKind.Relative),
                            Scopes = new Dictionary<string, string>
                            {
                                { "api", "SPA and Mobile API" }
                            }
                        }
                    }
                });
                //TODO Chack this:
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference{ Type = ReferenceType.SecurityScheme, Id = "oauth2"}
                        },
                        new [] { "api" }
                    }
                });

                c.SchemaFilter<SwaggerFluentValidationProvider>();
                c.SchemaFilter<SwaggerSampleModelProvider>();
                c.OperationFilter<SwaggerAuthDefinitionProvider>();
                c.OperationFilter<SwaggerLanguageHeaderProvider>();
            });

            services.AddMemoryCache();

            services.AddHttpClient();

            services.AddScoped<ILanguageDetector, LanguageDetector>();
            services.AddScoped<IMessageTranslator, MessageTranslator>();

            services.AddHangfire(configuration =>
            {
                configuration.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                             .UseSimpleAssemblyNameTypeSerializer()
                             .UseRecommendedSerializerSettings()
                             .UseSqlServerStorage(conStr, new SqlServerStorageOptions
                             {
                                 CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                                 SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                                 QueuePollInterval = TimeSpan.Zero,
                                 UseRecommendedIsolationLevel = true,
                                 UsePageLocksOnDequeue = true,
                                 DisableGlobalLocks = true
                             });
            });

            services.AddHttpClient();

            services.AddHangfireServer();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(Assemblies.Get());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostEnvironment env,
            ILoggerFactory loggerFactory,
            ILanguageDetector detector,
            IMessageTranslator translator,
            IRecurringJobManager backgroundJobs)
        {

            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.  
            app.UseHsts();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            if (env.IsDevelopment() || env.IsStaging())
            {
                // Swagger
                app.UseSwagger();

                // Hangfire
                app.UseHangfireDashboard();
            }
            else
            {
                //protect swagger
                bool SwaggerIpFilter(HttpContext http)
                {
                    return http.Request.Path.Equals("/swagger/v1/swagger.json")
                           && http.Connection.RemoteIpAddress.ToString()
                               .Equals(_configuration["Swagger:Ip"], StringComparison.Ordinal);
                }

                app.MapWhen(SwaggerIpFilter, builder => builder.UseSwagger());
                //TODO add Hangfire only for used IP

                app.UseHangfireDashboard();
            }

            //backgroundJobs.AddOrUpdate<AcceptedRejectedJob>("accepted", x => x.DoActions(InvitationType.Accepted, CancellationToken.None), "*/1 * * * *");
            //backgroundJobs.AddOrUpdate<AcceptedRejectedJob>("rejected", x => x.DoActions(InvitationType.Rejected, CancellationToken.None), "*/1 * * * *");
            //backgroundJobs.AddOrUpdate<UnifonicWebhookService>("unifonicWebhook", x => x.StartUnifonicWebhook(CancellationToken.None), "*/1 * * * *");
            //backgroundJobs.RemoveIfExists("unifonicWebhook");

            app.UseMiddleware<Helpers.TempMiddleware.RequestLoggingMiddleware>();
            var policyCollection = new HeaderPolicyCollection()
                .AddFrameOptionsDeny()
                .AddXssProtectionBlock()
                .AddContentTypeOptionsNoSniff()
                .AddStrictTransportSecurityMaxAgeIncludeSubDomains(7776000) //minimum recommended value
                .AddReferrerPolicyStrictOriginWhenCrossOrigin()
                .RemoveServerHeader()
                .RemoveCustomHeader("X-Powered-By")
                .AddContentSecurityPolicy(builder =>
                {
                    builder.AddObjectSrc().None();
                    builder.AddFormAction().Self();
                    builder.AddFrameAncestors().None();
                });

            app.UseSecurityHeaders(policyCollection);

            app.UseCors(MyAllowSpecificOrigins);

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Takamul IRMS V1");
                c.OAuthClientId("client-spa");
                c.OAuthClientSecret("secret");
                c.OAuthScopeSeparator(" ");
                //c.OAuthAdditionalQueryStringParams(new Dictionary<string, string> { { "api", "SPA and Mobile API" } });//TODO test this line
                c.OAuthUseBasicAuthenticationWithAccessCodeGrant();

                c.DocExpansion(env.IsDevelopment() ? DocExpansion.None : DocExpansion.List);
            });

            //app.UseHttpsRedirection();

            app.CorrectStatusCode(loggerFactory, detector, translator); //set the http code 400 when it's needed

            loggerFactory.AddSerilog();

            app.UseMultitenancy<TenantBasicInfo>();

            app.UseIdentityServer(); // includes a call to UseAuthentication

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
