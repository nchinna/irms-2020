﻿namespace Irms.WebApi.ViewModels
{
    public class TimeZoneVm
    {
        public TimeZoneVm(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; }
        public string Name { get; }
    }
}
