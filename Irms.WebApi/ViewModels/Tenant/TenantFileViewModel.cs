﻿using System;
using Microsoft.AspNetCore.Http;

namespace Irms.WebApi.ViewModels.Tenant
{
    public class TenantFileViewModel
    {
        public IFormFile File { get; set; }
        public Guid TenantId { get; set; }
    }
}
