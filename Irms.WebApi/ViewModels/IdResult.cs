﻿using System;

namespace Irms.WebApi.ViewModels
{
    public class IdResult
    {
        public IdResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
