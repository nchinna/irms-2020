﻿namespace Irms.WebApi.ViewModels.Account
{
    public class GetTokenByCodeViewModel
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
    }
}
