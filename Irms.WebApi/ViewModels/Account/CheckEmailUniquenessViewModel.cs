﻿namespace Irms.WebApi.ViewModels.Account
{
    public class CheckEmailUniquenessViewModel
    {
        public CheckEmailUniquenessViewModel(string email)
        {
            Email = email;
        }

        public string Email { get; }
    }
}
