﻿namespace Irms.WebApi.ViewModels.Account
{
    public class UniquenessViewModel
    {
        public UniquenessViewModel(bool isUnique, bool isUnavailable)
        {
            IsUnique = isUnique;
            IsUnavailable = isUnavailable;
        }

        public bool IsUnique { get; }
        public bool IsUnavailable { get; }
    }
}
