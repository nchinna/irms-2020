﻿namespace Irms.WebApi.ViewModels.Account
{
    public class CheckNationalIdUniquenessViewModel
    {
        public CheckNationalIdUniquenessViewModel(string nationalId)
        {
            NationalId = nationalId;
        }

        public string NationalId { get; }
    }
}
