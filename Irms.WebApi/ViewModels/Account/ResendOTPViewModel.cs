﻿namespace Irms.WebApi.ViewModels.Account
{
    public class ResendOTPViewModel
    {
        public string UserName { get; set; }
        public string Token { get; set; }
    }
}
