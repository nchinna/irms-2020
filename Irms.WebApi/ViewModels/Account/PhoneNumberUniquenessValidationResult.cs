﻿namespace Irms.WebApi.ViewModels.Account
{
    public class PhoneNumberUniquenessValidationResult
    {
        public PhoneNumberUniquenessValidationResult(bool isValid, bool isUnique, string formattedPhoneNo, bool isUnavailable)
        {
            IsValid = isValid;
            FormattedPhoneNo = formattedPhoneNo;
            IsUnique = isUnique;
            IsUnavailable = isUnavailable;
        }

        public bool IsValid { get; }
        public bool IsUnique { get; }
        public string FormattedPhoneNo { get; }
        public bool IsUnavailable { get; }
    }
}
