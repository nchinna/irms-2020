﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.ListAnalysis.Commands;
using Irms.Data.Read.ListAnalysis.Queries;
using Irms.Data.Read.ListAnalysis.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class ListAnalysisController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ListAnalysisController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("important-fields")]
        public async Task<Unit> CreateListAnalysisImportantFields([FromBody]CreateListAnalysisImportantFieldsCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpGet("important-fields/{id}")]
        public async Task<ListAnalysisImportantFields> GetListAnalysisImportantFields(Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetListAnalysisImportantFieldsQuery(id), token);
        }

        /// <summary>
        /// Get contacts with custom fields endpoint
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("validations-count")]
        public async Task<ValidationsCount> GetValidationsCount([FromBody]GetValidationsCountQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// delete list analysis components
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task<Guid> DeleteListAnalysis([FromBody]DeleteListAnalysisCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        //list analysis state maintaince
        [HttpPost("get-list-state")]
        public async Task<ListAnalysisState> GetListAnalysisState([FromBody]GetListAnalysisStateQuery cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        //set list state
        [HttpPost("set-list-state")]
        public async Task<Unit> SetListAnalysisState([FromBody]SetListAnalysisStateCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }
    }
}