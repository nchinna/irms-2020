﻿using Irms.Application.Contact.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    //[AuthorizeRoles(RoleType.TenantAdmin)]
    [ApiController]
    [Route("api/[controller]")]
    public class WebhookController : ControllerBase
    {
        private readonly IMediator _mediator;
        public WebhookController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// twilio sms webhook
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("twilio")]
        public async Task<Unit> TwilioEvent([FromForm]TwilioWebhookSmsCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpPost("whatsapp-bot")]
        public async Task<Unit> TwilioWhatsappBotResponse([FromForm]TwilioWhatsappBotResponseCmd request, CancellationToken token)
        {
            return await _mediator.Send(request, token);
        }

        #region sendgrid
        [HttpPost("sendgrid")]
        public async Task<Unit> SendgridEvent(WebhookSendgridModel[] model, CancellationToken token)
        {
            //var modddel = new[]
            //{
            //    new WebhookSendgridModel
            //    {
            //    email = "saif.rehman@takamul.net.sa",
            //    @event = "testeve",
            //    ip = "192.168.1.1",
            //    response = "testress",
            //    sg_event_id = "vis3284932k",
            //    sg_message_id = "eEKDAYcOT5mjJ5LLdIOZsA",
            //    smtpid = "testst",
            //    timestamp = 1524042,
            //    tls = 0
            //    }
            //};

            var cmd = new SendgridWebhookEmailCmd
            {
                Responses = model
            };
            return await _mediator.Send(cmd, token);
        }
        #endregion
    }
}
