﻿using Irms.Application.Events.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Event.Queries;
using Irms.Data.Read.Event.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class EventController : ControllerBase
    {
        private readonly IMediator _mediator;
        public EventController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// get events list by filters
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public async Task<IPageResult<EventListItem>> GetEventList([FromBody]GetEventList query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }

        /// <summary>
        /// get info basic details
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/info")]
        public async Task<EventInfo> GetEventInfo(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetEventInfo(id), token);
            return result;
        }

        /// <summary>
        /// get event features
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/event-features")]
        public async Task<IEnumerable<EventFeatureListItem>> GetEventFeatures(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetEventFeatures(id), token);
            return result;
        }

        /// <summary>
        /// get product features...
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("features")]
        public async Task<IEnumerable<ProductFeatureListItem>> GetProductFeatures(CancellationToken token)
        {
            var result = await _mediator.Send(new GetProductFeatures(), token);
            return result;
        }

        /// <summary>
        /// create event
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> Create([FromForm]CreateEventCmd cmd, CancellationToken token)
        {
            var id = await _mediator.Send(cmd, token);
            return id;
        }

        /// <summary>
        /// this method will update event.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromForm]UpdateEventCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// update features of specific event.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("event-features")]
        public async Task UpdateEventFeatures([FromBody]UpdateEventFeaturesCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// delete event by id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<Unit> Delete([FromBody]DeleteEventCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
    }
}
