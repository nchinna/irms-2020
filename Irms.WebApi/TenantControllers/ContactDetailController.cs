﻿using AutoMapper.Mappers;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.ReadModels;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class ContactDetailController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ContactDetailController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("personal-details")]
        public async Task<PersonalDetailsReadModel> GetPersonalDetails([FromBody] GetPersonalDetailsQuery query, CancellationToken token)
        {
            return await _mediator.Send(query, token);
        }

        [HttpGet("organization-info/{id}")]
        public async Task<OrganizationReadModel> GetOrganizationInfo([FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetOrganizationDetailsQuery(id), token);
        }

        [HttpPost("organization-info")]
        public async Task UpdateOrganizationInfo([FromBody] UpdateOrganizationInfoCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        [HttpGet("personal-info/{id}")]
        public async Task<PersonalInfoReadModel> GetPersonalInfo([FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetPersonalInfoQuery(id), token);
        }

        [HttpPost("personal-info")]
        public async Task<PersonalInfoModel> UpdatePersonalInfo([FromBody] UpdatePersonalInfoCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// update global contact personal info
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("global-contact-personal-info")]
        public async Task<PersonalInfoModel> UpdateGlobalContactPersonalInfo([FromBody] UpdateGlobalContactPersonalInfoCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpGet("associated-lists/{id}")]
        public async Task<IEnumerable<AssociatedListReadModel>> GetAssociatedLists([FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetAssociatedListQuery(id), token);
        }

        [HttpGet("event-admission/{listId}/{id}")]
        public async Task<EventAdmissionReadModel> GetEventAdmission([FromRoute] Guid listId, [FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetEventAdmissionQuery(listId, id), token);
        }

        [HttpPost("event-admission")]
        public async Task UpdateEventAdmission([FromBody] UpdateEventAdmissionCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }

        [HttpGet("custom-fields/{id}")]
        public async Task<CustomFieldReadModel> GetCustomFields([FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetCustomFieldsQuery(id), token);
        }

        [HttpPost("custom-fields/{id}")]
        public async Task UpdateCustomFields([FromRoute]Guid id, [FromBody]UpdateCustomFieldCmd.CustomFieldModel model, CancellationToken token)
        {
            await _mediator.Send(new UpdateCustomFieldCmd(id, model), token);
        }
    }
}
