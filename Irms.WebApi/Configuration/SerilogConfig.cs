﻿using Microsoft.Extensions.Configuration;
using Serilog;

namespace Irms.WebApi.Configuration
{
    public class SerilogConfig
    {        public static void InitGlobalLogger(IConfiguration cfg)
        {
            //The logging is required when IoC is not initialized yet
            //Otherwise it would be yet another IoC Module

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(cfg) //load from configuration
                .Enrich.FromLogContext()
                .CreateLogger();
        }
    }
}
